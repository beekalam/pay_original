<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Validator;

class NewsController extends Controller
{
    public function index()
    {
        $vm["news"] = News::all();
        // dd($vm);
        return view('admin.news.index', $vm);
    }

    public function add()
    {
        return view('admin.news.add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required|max:255',
            "text"  => 'required',
            "slug"  => 'required|unique:pages'
        ], $this->messages("pages"));

        if ($validator->fails()) {
            return redirect('/page/add')->withInput()->withErrors($validator);
        }
        $n = new News();
        $n->title  = $request->input("title");
        $n->text   = $request->input('text');
        $n->slug   = $request->input('slug');
        $n->active = 0;
        if (isset($_POST['active'])) {
            $n->active = 1;
        }
        $n->save();
        return view('admin.news.add');
    }
}
