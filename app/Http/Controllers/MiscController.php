<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;

class MiscController extends Controller
{
    public function stateCities(Request $request)
    {
        if (isset($_GET['state_id'])) {
            $cities = DB::table('cities')->where("state_id", $request->input('state_id'))->pluck('id', 'name');
            return json_encode($cities);
        }

        return json_encode([]);
    }
}
