<?php

namespace App\Http\Controllers;

use App\Setting;
use Validator;

class FrontController extends Controller
{
    private function settings()
    {
        return Setting::all()->pluck('value', 'name');
    }

    public function index()
    {
        $vm['settings'] = $this->settings();
        // dd($vm);
        return view('front.index', $vm);
    }

    public function register()
    {
        $vm['settings'] = $this->settings();
        return view('front.register', $vm);
    }

    public function register_store(){
        
    }

    public function faq(){
        $vm['settings'] = $this->settings();
        return view('front.faq',$vm);
    }
    public function aboutus(){
        $vm['settings'] = $this->settings();
        return view('front.aboutus',$vm);
    }

    public function contactus(){
        $vm['settings'] = $this->settings();
        return view('front.contactus',$vm);
    }
}
