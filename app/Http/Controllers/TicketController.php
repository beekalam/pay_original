<?php

namespace App\Http\Controllers;

use App\Department;
use App\Ticket;
use App\TicketPost;
use Auth;
use Illuminate\Http\Request;
use Validator;
use DB;
class TicketController extends Controller
{
    public function index()
    {
        $vm['tickets'] = Ticket::where('user_id', Auth::user()->id)->get();
        return view('front.ticket.index', $vm);
    }

    public function create()
    {
        $vm['departments'] = Department::all();
        return view('front.ticket.create', $vm);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required|max:255',
            "department" => 'required',
            "text" => "required",
        ], $this->messages('ticket.create'));

        if ($validator->fails()) {
            return redirect('/ticket/create')->withInput()->withErrors($validator);
        }

        try {
            DB::beginTransaction();
            $last_post_by = 1;
            if(Auth::user()->isAdmin())
                $last_post_by = 2;
            
            $ticket = new Ticket();
            $ticket->title = $request->input('title');
            $ticket->department_id = $request->input('department');
            $ticket->user_id = Auth::user()->id;
            $ticket->last_post_by = $last_post_by;
            $ticket->save();

            $ticketPost = new TicketPost();
            $ticketPost->ticket_id = $ticket->id;
            $ticketPost->text = $request->input('text');
            $ticketPost->by = $last_post_by;
            $ticketPost->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('/ticket')->with('message','error|خطا در ارسال تیکت');
        }

        return redirect('/ticket')->with('message','info|تیکت با موفقیت ارسال شد.');
    }
}
