<?php

namespace App\Http\Controllers;

use App\Repositories\SettingsRespository;
use DB;
use Illuminate\Http\Request;
use App\Gateway;
use App\Setting;
class GatewayController extends Controller
{
    public function index()
    {
        $title    = 'لیست درگاه های بانکی';
        $query    = '
SELECT gateways.*,bank_modules.title AS module_title
FROM gateways
LEFT JOIN bank_modules ON bank_modules.id = module_id
WHERE gateways.status != 2';
        $gateways = $this->to_array(DB::select(DB::raw($query)));
        return view('admin.gateway.index', compact("title", "gateways"));
    }

    public function add()
    {
        $title        = "افزودن درگاه بانکی جدید";
        $bank_modules = $this->to_array(DB::select(DB::raw("select id,title from bank_modules")));
        $step         = 1;
        return view('admin.gateway.add', compact("title", "bank_modules", "step"));
    }

    public function add_gateway(Request $request)
    {
        $title        = "افزودن درگاه بانکی جدید";
        $step         = $request->input('step');
        $module_id    = $request->input('module_id');
        $module       = $this->to_array(DB::select(DB::raw("select * from bank_modules where  id='$module_id'")));
        $requirements = explode(',', $module[0]["requirements"]);
        if ($step == 1) {
            $step = 2;
            $name = $request->input('title');
            return view("admin.gateway.add", compact("step", "title", "requirements", "module_id", "name"));
        } else if ($step == 2) {
            $name   = $request->input('name');
            $values = [];
            foreach ($requirements as $r) {
                $values[$r] = $request->input($r);
            }
            $values = serialize(json_encode($values));
            DB::table('gateways')->insert([
                "title"     => $name,
                "module_id" => $module_id,
                "data"      => $values,
                "status"    => 1
            ]);
            return redirect(route("gateway.index"));
        }
    }

    public function delete_gateway(Request $request)
    {
        if (isset($_POST['id'])) {
            DB::table('gateways')->where('id', $request->input('id'))->update(['status' => 2]);
        }
        return redirect(route('gateway.index'));
    }

    public function edit($id)
    {
        $query           = "
SELECT gateways.*,bank_modules.title AS module_title
FROM gateways
LEFT JOIN bank_modules ON bank_modules.id = module_id
WHERE (gateways.status != 2 and gateways.id = '$id')";
        $gateway         = $this->to_array(DB::select(DB::raw($query)));
        $gateway         = $gateway[0];
        $gateway['data'] = $this->to_array(json_decode(unserialize($gateway['data'])));

        return view("admin.gateway.edit", compact("gateway"));
    }

    public function editGateway(Request $request)
    {
        if (isset($_POST['id'])) {
            $id      = $request->input('id');
            $gateway = $this->to_array(DB::select(DB::raw("select * from gateways where id='$id'")));
            if (isset($gateway[0])) {
                $gateway      = $gateway[0];
                $module_id    = $gateway['module_id'];
                $requirements = $this->to_array(DB::select(DB::raw("select * from bank_modules where id='$module_id'")));
                $requirements = explode(',', $requirements[0]["requirements"]);
                $updates      = ["title" => $request->input('title')];
                foreach ($requirements as $r) {
                    $updates['data'][$r] = $request->input($r);
                }
                $updates['data'] = serialize(json_encode($updates['data']));
                DB::table('gateways')->where('id', $id)->update($updates);
            }
        }
        return redirect(route('gateway.index'));
    }

    public function setting()
    {
        $vm["title"]                = 'تنظیمات درگاه پرداخت';
        $settingsRepository         = new SettingsRespository();
        $vm["default_gateway_id"]   = $settingsRepository->get("default_gateway_id");
        $vm["default_gateway_id_2"] = $settingsRepository->get("default_gateway_id_2");
        $vm["gateway1"]             = Gateway::where("id", $vm["default_gateway_id"])->first()->toArray();
        $vm["gateway2"]             = Gateway::where('id', $vm["default_gateway_id_2"])->first()->toArray();
        $vm["gateways"]             = Gateway::where("status", "<>", "2")->get()->toArray();

        return view('admin.gateway.setting', $vm);
    }

    public function update_setting(Request $request)
    {
        if (isset($_POST['gateway_id']) && isset($_POST['gateway_id_2'])) {
             Setting::where("name", "default_gateway_id")->update(["value" => $request->input("gateway_id")]);
             Setting::where("name","default_gateway_id_2")->update(["value" => $request->input("gateway_id_2")]);
        }

        return redirect(route("gateway.setting"));
    }
}
