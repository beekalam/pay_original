<?php

namespace App\Http\Controllers;

use http\Env\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;
use DB;
use Route;
use Auth;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        isset($_SERVER['remOTE_ADDR']) && Log::debug("ip: " . $_SERVER['REMOTE_ADDR'] );
        isset($_SERVER['HTTP_USER_AGENT']) &&  Log::debug("ua: " . $_SERVER["HTTP_USER_AGENT"]);
        Log::debug(print_r(Route::currentRouteAction(),true));
        if($_POST)
            Log::debug("post: " . print_r($_POST, true) );
        
        // parent::__construct();
        // DB::setFetchMode(\PDO::FETCH_ASSOC);
    }

    public function to_array($input)
    {
        return json_decode(json_encode($input), true);
    }

    public function messages($key)
    {
        $messages = ["forms" => [
            'title.required'       => 'عنوان کالا وارد نشده است.',
            'amount.required'      => 'مبلغ کالا وارد نشده است.',
            'terminal_id.required' => 'ترمینال انتخاب نشده است.'
        ], 
        "pages"           => [
            "title.required" => "عنوان صفحه وارد نشده است.",
            "text.required"  => "متن صفحه وارد نشده است.",
            "slug.required"  => "عنوان انگلیسی وارد نشده است.",
            "slug.unique"    => "عنوان انگلیسی  یکتا نیست."
        ], 
        "notifications"   => [

        ],
        "register" => [
            "name.required" => "نام وارد نشده است.",
            "email.required" => "ایمیل وارد نشده است.",
            "mobile.required" => "موبایل وارد نشده است.",
            "password.required" => "پسورد وارد نشده است.",
            "phone.required" => "شماره تلفن وارد نشده است.",
            "tos.required" => "قوانین مورد تایید قرار نگرفته.",
        ],
        "ticket.create" =>[
            "title.required" => "عنوان وارد نشده است",
            "department.required" => 'بخش انتخاب نشده است.',
            'text' => 'متن تیکت وارد نشده.',
        ]
        ];
        return $messages[$key];
    }

    public function isAdmin(){
        return Auth::user()->isAdmin();
    }

    protected function build_gateway_handler($gateway)
    {
        $g = null;
        if ($gateway->gateway_name == 'SAMAN') {
            require_once base_path("app/ThirdParty/gateway/saman/saman.php");
            $gateway_credentials = json_decode(unserialize($gateway->data));
            $g = new \saman($gateway_credentials->SAMAN_MERCHANT_ID);
        }
        return $g;
    }
}
