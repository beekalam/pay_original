<?php

namespace App\Http\Controllers;

use App\FormData;
use App\Invoice;
use App\Forms;
use App\Terminal;
use Auth;
use Config;
use function App\generate_hash;
use Illuminate\Http\Request;
use Validator;

class FormsController extends Controller
{
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            $vm['forms_data'] = FormData::all();
        } else {
            $vm['forms_data'] = FormData::all();
        }

        return view("admin.forms.index", $vm);
    }

    public function viewdata($id)
    {
        $vm['data'] = FormData::find($id)->toArray();
        $vm['data']['data'] = unserialize($vm['data']['data']);
        $vm['fields'] = array('name' => 'نام', 'mobile' => 'موبایل', 'zipcode' => 'کد پستی');
        return view('admin.forms.viewdata', $vm);
    }

    public function forms()
    {
        if (Auth::user()->isAdmin()) {
            $vm['forms'] = Forms::all();
        } else {
            $vm['forms'] = Forms::where('user_id', Auth::user()->id)->get();
        }

        return view('admin.forms.forms', $vm);
    }

    public function delete($id)
    {
        $form = Forms::where('id', $id)->firstOrFail();
        $form->delete();
        return redirect(route("forms.forms"))->with("message", "success| فرم با موفقیت حذف شد.");
    }

    private function make_invoice()
    {

    }

    public function pay($key, Request $request)
    {
        //todo return on invlid key
        list($id, $hash) = explode("_", $key);
        $form = Forms::where("id", $id)->where("hash", $hash)->where("status", "=", 1)->firstOrFail();
        $terminal = $form->terminal()->firstOrFail();
        $gateway = $terminal->gateway()->firstOrFail();

        // $g = $this->build_gateway_handler($gateway);
        // echo $g->toBank();
        // dump($gateway->title);
        // dump($gateway->gateway_name);
        // exit;
        $vm["form"] = Forms::where("id", $id)->where("hash", $hash)->where("status", "=", 1)->firstOrFail();
        $vm["pay_fields"] = Config::get('form_keys');
        $vm['form_key'] = $key;
        return view('layouts.pay', $vm);
    }

    private function build_validator($form, $request)
    {
        $val = [];
        $form_keys = Config::get('form_keys');

        foreach ($form->fields_array() as $k) {
            if(isset($form_keys[$k]['validation']))
                $val[$k] = $form_keys[$k]['validation'];
            else {
                $val[$k] = '';
            }
        }
        $messages = ['required' => ':attribute وارد نشده است'];
        return Validator::make($request->all(), $val,$messages);
    }

    public function pay_final($key, Request $request)
    {
        //todo validate based on form selected values.
        list($id, $hash) = explode("_", $key);
        $form = Forms::where("id", $id)->where("hash", $hash)->where("status", "=", 1)->firstOrFail();
        $terminal = $form->terminal()->firstOrFail();
        $gateway = $terminal->gateway()->firstOrFail();

        // validate
        $validator = $this->build_validator($form, $request);
        if ($validator->fails()) {
            return redirect('/forms/pay/' . $key)->withInput()->withErrors($validator);
        }

        // save form data
        $data = [];
        foreach($form->fields_array() as $field){    
            $data[$field] = $request->input($field);
        }
        $data = serialize(json_encode($data));
        $formdata = new FormData();
        $formdata->form_id = $id;
        $formdata->data = $data;
        $formdata->status = 0;
        //todo check for karmozd and update amount
        $formdata->amount = $form->amount;
        $formdata->save();

        //save invoice 
        $invoice = new Invoice();
        $invoice->user_id = Auth::user()->id;
        $invoice->type = Config::get('constants.INVOICE_PAY_TYPE.FORM');
        $invoice->terminal_id = $terminal->id;
        $invoice->amount = $form->amount;
        $invoice->entity_id = 1; //todo replace with real value
        $invoice->karmozd = $terminal->karmozd;
        $invoice->karmozd_type = $terminal->karmozd_type;
        $invoice->hash = generate_hash();
        $invoice->status = Config::get('constants.INVOICE_STATUS.PENDING');
        $invoice->return_url = $form->callback_url_success;
        $invoice->save();

        // redirect
        $g = $this->build_gateway_handler($gateway);
        $vm["html_form"] = $g->toBank($invoice);
        return view('layouts.to_bank',$vm);
    }
 
    public function paid()
    {

    }

    public function add()
    {
        $vm['terminals'] = Terminal::all();
        return view('admin.forms.add', $vm);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required|max:255',
            "amount" => 'required',
            "callback_url_success" => 'nullable|url',
            "callback_url_fail" => 'nullable|url',
            "terminal_id" => "required",
        ], $this->messages("forms"));

        if ($validator->fails()) {
            return redirect('/forms/add')->withInput()->withErrors($validator);
        }

        $form = new Forms();
        $form->user_id = Auth::user()->id;
        $form->title = $request->input('title');
        $form->amount = $request->input("amount");
        $form->terminal_id = $request->input('terminal_id');
        $form->status = 1;
        $form->callback_url_success = $request->input('callback_url_success');
        $form->callback_url_success = is_null($form->callback_url_success) ? "" : $form->callback_url_success;
        $form->callback_url_fail = $request->input('callback_url_fail');
        $form->callback_url_fail = is_null($form->callback_url_fail) ? "" : $form->callback_url_fail;
        $form->hash = generate_hash();
        if (!empty($_POST['fields'])) {
            $form->fields = implode(',', $request->input('fields'));
        }
        $form->save();
        return redirect("/forms/forms")->with("message", "success|فرم با موفقیت ثبت شد.");
    }

    //todo@: add edit
}
