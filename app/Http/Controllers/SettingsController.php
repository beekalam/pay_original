<?php

namespace App\Http\Controllers;

use App\Repositories\SettingsRespository;
use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller
{
    private $system_settings          = ["terminals_auto_approve", "max_upload_size", "photo_shenasname", "domain_lock", "show_invoice_errors_usercp"];
    private $system_settings_checkbox = ["terminals_auto_approve", "photo_shenasname", "domain_lock", "show_invoice_errors_usercp"];

    private $withdraw_settings          = ["withdraw_active", "withdraw_min", "tasvie_limit_hours"];
    private $withdraw_settings_checkbox = ["withdraw_active"];

    private $karmozd_settings = ["karmozd", "karmozd_max", "karmozd_guestpay", "karmozd_withdraw_shetab", "karmozd_withdraw_paya"];

    public function edit()
    {
        $s = new SettingsRespository();

        $vm["site_page_title"]    = $s->get("site_page_title");
        $vm["site_contact_email"] = $s->get('site_contact_email');
        $vm["is_public"]          = $s->get("is_public");
        return view('admin.settings.index', $vm);
    }

    public function update_site_settings(Request $request)
    {
        foreach (["site_page_title", "site_contact_email"] as $k) {
            if (isset($_POST[$k])) {
                Setting::where("name", $k)->update(["value" => $request->input($k)]);
            }
        }
        if (!isset($_POST['is_public'])) {
            Setting::where("name", "is_public")->update(["value" => 0]);
        }

        return redirect(route("settings.edit"));
    }

    public function system()
    {
        $s = new SettingsRespository();
        foreach ($this->system_settings as $k) {
            $vm[$k] = $s->get($k);
            if (is_null($vm[$k])) {
                $vm[$k] = 0;
            }
        }

        return view("admin.settings.system", $vm);
    }

    public function systemSettingsUpdate(Request $request)
    {
        foreach ($this->system_settings_checkbox as $k) {
            if (isset($_POST[$k])) {
                Setting::where("name", $k)->update(["value" => $request->input($k)]);
            } else {
                Setting::where("name", $k)->update(["value" => 0]);
            }
        }
        Setting::where('name', 'max_upload_size')->update(["value" => $request->input('max_upload_size')]);
        return redirect(route('settings.system'));
    }

    public function withdraw()
    {
        $s = new SettingsRespository();
        foreach ($this->withdraw_settings as $k) {
            $vm[$k] = $s->get($k);
        }
        return view("admin.settings.withdraw", $vm);
    }

    public function withdrawSettingsUpdate(Request $request)
    {
        foreach ($this->withdraw_settings_checkbox as $k) {
            if (isset($_POST[$k])) {
                Setting::where('name', $k)->update(['value' => $request->input($k)]);
            } else {
                Setting::where('name', $k)->updaet(['value' => 0]);
            }
        }
        foreach ($this->withdraw_settings as $k) {
            Setting::where('name', $k)->update(['value' => $request->input($k)]);
        }

        return redirect(route('settings.withdraw'));
    }

    public function karmozd()
    {
        $s = new SettingsRespository();
        foreach ($this->karmozd_settings as $k) {
            $vm[$k] = $s->get($k);
        }
        return view("admin.settings.karmozd", $vm);
    }

    public function karmozdSettingsUpdate(Request $request)
    {
        foreach ($this->karmozd_settings as $k) {
            Setting::where("name", $k)->update(["value" => $request->input($k)]);
        }
        return redirect(route("settings.karmozd"));
    }


}
