<?php

namespace App\Http\Controllers;

use App\Terminal;
use Auth;
use function App\ThirdParty\convert_gregorian_iso_to_jalali_iso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TerminalController extends Controller
{
    public function index()
    {
        Log::debug("Listing terminals");
        if ($this->isAdmin()) {
            $vm['terminals'] = Terminal::get()->toArray();
        } else {
            $vm['terminals'] = Terminal::where('user_id', Auth::user()->id)->get()->toArray();
        }

        foreach ($vm['terminals'] as &$item) {
            $item['fa_created_at'] = convert_gregorian_iso_to_jalali_iso($item['created_at'], '-', true);
        }

        return view('admin.terminals.index', $vm);
    }

    public function add()
    {
        //todo check permission for user
        //todo check user completed pofile here.
        return view("admin.terminals.add");
    }

    public function addTerminal(Request $request)
    {
        $inputs = [
            "domain",
            "title",
            "karmozd_type",
            "karmozd",
            "gateway_id",
        ];
        // "no_domain_lock",
        $all_set = array_reduce($inputs, function ($prev, $item) {
            return $prev && isset($_POST[$item]);
        }, true);
        if ($all_set) {

            $terminal = new Terminal();
            $terminal->user_id = Auth::user()->id;
            $terminal->title = $request->input('title');
            $terminal->karmozd_type = $request->input('karmozd_type');
            $terminal->karmozd = $request->input('karmozd');
            $terminal->gateway_id = $request->input('gateway_id');
            $terminal->domain = $request->input('domain');
            $terminal->api_key = md5(rand(10000000, 999999999) . time());
            $terminal->status = 0;
            $terminal->no_domain_lock = isset($_POST['no_domain_lock']) ? $request->input("no_domain_lock") : 0;
            $terminal->save();
        }

        return redirect(route('terminals.index'))->with("message","success|ترمینال با موفقیت ثبت شد.");
    }

}
