<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use Validator;
use Auth;
class NotificationsController extends Controller
{
    public function index()
    {
        $vm['notifications'] = Notification::all();
        return view('admin.notifications.index', $vm);
    }

    public function add()
    {
        return view('admin.notifications.add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => "required",
            "text"  => "required"
        ], $this->messages("notifications"));

        if ($validator->fails()) {
            return redirect(route('notifications.add'))->withInput()->withErrors($validator);
        }
        $n = new Notification();
        //@todo change  to appropriate id
        $n->user_id = Auth::user()->id;
        $n->title   = $request->input("title");
        $n->text    = $request->input("text");
        $n->save();

        return redirect(route('notifications.index'));
    }
}
