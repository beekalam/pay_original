<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Validator;

class PagesController extends Controller
{
    public function index()
    {
        $vm["pages"] = Page::all();
        // dd($vm);
        return view('admin.pages.index', $vm);
    }

    public function add()
    {
        return view('admin.pages.add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => 'required|max:255',
            "text"  => 'required',
            "slug"  => 'required|unique:pages'
        ], $this->messages("pages"));

        if ($validator->fails()) {
            return redirect('/page/add')->withInput()->withErrors($validator);
        }
        $p         = new Page();
        $p->title  = $request->input("title");
        $p->text   = $request->input('text');
        $p->slug   = $request->input('slug');
        $p->active = 0;
        if (isset($_POST['active'])) {
            $p->active = 1;
        }
        $p->save();
        return view('admin.pages.add');
    }
}
