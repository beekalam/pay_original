<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use App\Terminal;
use Validator;
use function App\generate_hash;
use Auth;

class LinksController extends Controller
{
    public function index()
    {
        if(Auth::user()->isAdmin()){
            $vm['links'] = Link::where('status', '1')->get();
        }else{
            $vm['links'] = Link::where('user_id',Auth::user()->id)->get();
        }
        return view("admin.links.index", $vm);
    }

    public function add()
    {
        $vm['terminals'] = Terminal::all();
        return view('admin.links.add', $vm);
    }

    private function build_validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title"                => 'required|max:255',
            "amount"               => 'required',
            "callback_url_success" => 'nullable|url',
            "callback_url_fail"    => 'nullable|url',
            "terminal_id"          => "required"
        ], $this->messages("forms"));
        return $validator;
    }

    public function store(Request $request)
    {
        $validator = $this->build_validator($request);
        if ($validator->fails()) {
            return redirect('/links/add')->withInput()->withErrors($validator);
        }

        $link                       = new Link();
        $link->user_id              = Auth::user()->id;
        $link->title                = $request->input('title');
        $link->amount               = $request->input("amount");
        $link->terminal_id          = $request->input('terminal_id');
        $link->status               = 1;
        $link->callback_url_success = $request->input('callback_url_success');
        $link->callback_url_success = is_null($link->callback_url_success) ? "" : $link->callback_url_success;
        $link->callback_url_fail    = $request->input('callback_url_fail');
        $link->callback_url_fail    = is_null($link->callback_url_fail) ? "" : $link->callback_url_fail;
        $link->hash                 = generate_hash();
        $link->save();

        return redirect('/links/index')->with("message","success|لینک با موفقیت ثبت شد.");
    }
    //@todo add delete
    //@todo add edit
}
