<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        // if(Auth::user()->isAdmin()) die('yes');
        // else die('no');

        $days = 100;
        $res = DB::table('invoices')->where('status', '1')
            ->whereRaw('date > (CURDATE() - INTERVAL ' . $days . ' DAY)')
            // ->select('amount', DB::raw('sum(amount)'))
            ->select('fdate', DB::raw('DATE_FORMAT(date,"%Y-%m-%d")'))
            ->select("invoices.*")
            // ->groupBy()
            ->get();
        
        $title="title";
        if(Auth::user()->isAdmin())
            return view('admin.dashboard',compact("title"));
        else
            return view('admin.dashboard',compact('title'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
