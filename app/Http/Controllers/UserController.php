<?php

namespace App\Http\Controllers;

use App\State;
use App\User;
use Auth;
use Config;
use DB;
use Hash;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function profile()
    {
        $vm["user"] = Auth::user();
        $vm['states'] = State::all();
        $vm['cities'] = [];

        if (isset($vm['user']->state_id)) {
            $vm['cities'] = DB::table('cities')->where("state_id", $vm['user']->state_id)->pluck('name', 'id');
        }

        // foreach($vm['cities'] as $k=>$v){
        //     dump($k . " " . $v);
        // }
        // exit;
        // dd($vm['user']->city_id);
        return view('user.profile', $vm);
    }

    public function updateProfile(Request $request)
    {
        $keys = ['name', 'father', 'birthday', 'sh_melli', 'state_id', 'city_id', 'address', 'zipcode'];
        $user = Auth::user();

        foreach ($keys as $k) {
            if (isset($_POST[$k]) && !empty($_POST[$k])) {
                $user->$k = $request->input($k);
            }
        }

        // save melli_scan
        if ($request->hasFile('melli_scan')) {
            $file_name = $this->make_filename('melli_scan', $request);
            $request->file('melli_scan')
                ->storeAs(Config::get('constants.options.user_document_folder'), $file_name);
            $user->sh_scan_file = $file_name;
        }
        
        // save shenasname_scan
        if ($request->hasFile('shenasname_scan')) {
            $file_name = $this->make_filename('shenasname_scan', $request);
            $request->file('shenasname_scan')
                    ->storeAs(Config::get('constants.options.user_document_folder'), $file_name);
            $user->melli_scan_file = $file_name;
        }

        $user->save();
        return redirect()->back()->with("message", "success| مشخصات شما با موفقیت ثبت شد.");
    }
    private function make_filename($field, $request)
    {
        $prefix = $field == 'melli_scan' ? 'ms' : 'sh';
        $hash = md5_file($request->file($field)->getPathName());
        $uid = Auth::user()->id;
        $time = time();
        $ext = $request->file($field)->getClientOriginalExtension();
        return "{$prefix}_{$uid}_{$hash}_{$time}.{$ext}";
    }
    public function showChangePasswordForm()
    {
        return view('user.change_password');
    }

    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            return redirect()->back()->with("message", "error|پسوردی که وارد کرده اید با پسورد فعلی شما مطابقت ندارد.");
        }
        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            return redirect()->back()->with('message', 'error|پسورد قدیمی و جدید نمی توانند یکسان باشند.');
        }
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => Config::get('constants.options.password_validation_requirements'),
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect()->back()->with("message", "success|پسورد شما با موفقیت تغییر یافت.");
    }

}
