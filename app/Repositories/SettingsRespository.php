<?php
namespace App\Repositories;
use DB;

class SettingsRespository
{
    public function get($name){
       $res =  DB::table('settings')->where("name",$name)->get()->toArray();
       if(count($res) == 1){
           return $res[0]->value;
       }
       return null;
    }
}