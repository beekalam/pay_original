<?php
class irankish extends Gateway
{
    private $irankish_merchant_id;
    private $irankish_sha1key;
    private $client;
    
    public function __construct()
    {
        $this->irankish_merchant_id = IRANKISH_MERCHANT_ID;
        $this->irankish_sha1key = IRANKISH_SHA1KEY;
        require_once(CORE_PATH.'nusoap/nusoap.php');
    }
    
    public function getClient($url)
    {
        $client = new nusoap_client($url,true,false,false,false,false,0,180);
        $client->soap_defencoding = 'UTF-8';
        $this->client = $client;
    }
    
    public function request($value,$tid)
    {
        $this->tid = $tid;
        $params = array(
            'amount' => $value,
            'merchantId' => $this->irankish_merchant_id,
            'invoiceNo' => $tid,
            'paymentId' => $tid,
            'revertURL' => (isset($GLOBALS['callback_url'])?$GLOBALS['callback_url']:(SITE_URL.'invoice/processTransaction/'.$GLOBALS['invoice_key'].'/')),
        );
        $this->getClient('https://ikc.shaparak.ir/XToken/Tokens.xml');
        $result = $this->client->call("MakeToken", array($params));
        
        if($result['MakeTokenResult']['result'] == 'true') {
            DB::update('invoices',array('bank_ref'=>$result['MakeTokenResult']['token']),'id='.$tid);
            return true;
        } else {
            $this->errorCode = $result['MakeTokenResult']['message'];
            return false;
        }
    }
    
    public function toBank($invoice)
    {
        $form = '<form id="kicapeyment" action="https://ikc.shaparak.ir/tpayment/payment/Index" method="POST" >
<input type="hidden" name="token" value="'.$invoice['bank_ref'].'">
<input type="hidden" name="merchantId" value="'.$this->irankish_merchant_id.'">
</form>';
        $this->autoForm($form);
    }
    
    public function process($invoice)
    {
        $this->tid = $invoice['id'];
        
        $token = trim(@$_POST['token']); // همان توکنی که در مرحله رزرو ساخته شد
        $resultCode = trim(@$_POST['resultCode']); // کد برگشت که برای تراکنش موفق عدد 100 میباشد
        $paymentId = trim(@$_POST['paymentId']); // همان شناسه خرید که در مرحله ساخت توکن استفاده کردیم
        $referenceId = trim(@$_POST['referenceId']); // شناسه مرجع که بانک میسازه و قابل پیگیری هست
        $cardNo = trim(@$_POST['cardNo']); // شماره کارت پرداخت کننده

        if($paymentId == $invoice['id']) {
            if($resultCode == '100'){
                $inv = DB::getRow('invoices','bank_code=? AND id!='.$invoice['id'],$referenceId);
                if(!$inv) {
                    $this->getClient('https://ikc.shaparak.ir/XVerify/Verify.xml');
                    $params['token'] = $token;
                    $params['merchantId'] = $this->irankish_merchant_id; // مرچند کد
                    $params['referenceNumber'] = $referenceId;
                    $params['sha1Key'] = $this->irankish_sha1key; //sha1Key که از بانک باید گرفته شود
                    for($i=1; $i<6; $i++) {
                        $result = $this->client->call("KicccPaymentsVerification", array($params));
                        if($result !== false) {
                            break;
                        } else {
                            sleep(2);
                        }
                    }
                    if($result !== false) {
                        if(@$result['KicccPaymentsVerificationResult'] == $invoice['amount'] || @$result['KicccPaymentsVerificationResult'] == -90) {
                            $this->responseOK($referenceId, $cardNo);
                            return true;
                        } else {
                            if(!empty($referenceId) && empty($invoice['bank_ref'])) {
                                DB::update('invoices',array('bank_code'=>$referenceId,'card_number'=>$cardNo),'id='.$invoice['id']);
                            }
                            $this->return_error(@$result['KicccPaymentsVerificationResult']);
                        }
                    } else {
                        $this->responseError($this->tid,'خطای تایید تراکنش ('.var_export($result,true).')');
                    }
                } else {
                    $this->responseError($tid,'شناسه مرجع تکراریست - ش.ت:'.$inv['id']);
                }
            } else {
                $this->return_error($resultCode);
            }
        } else {
            $this->responseError($this->tid, 'شماره تراکنش ارسالی از بانک تطابق ندارد');
        }
        return false;
    }
    
    private function return_error($result)
    {
        $arr = array(
            '110' => 'انصراف از پرداخت',
            '120' => 'موجودی حساب کافی نیست',
            '130' => 'اطلاعات کارت اشتباه است',
            '131' => 'رمز کارت اشتباه است',
            '132' => 'کارت مسدود شده است',
            '133' => 'کارت منقضی شده است',
            '140' => 'زمان پرداخت به پایان رسیده است',
            '150' => 'خطای داخلی بانک',
            '160' => 'خطا در اطلاعات CVV2 یا تاریخ انقضا',
            '166' => 'بانک صادر کننده کارت مجوز انجام تراکنش را صادر نکرده است',
            '200' => 'مبلغ تراکنش بیش از سقف مجاز هر تراکنش میباشد',
            '201' => 'مبلغ تراکنش بیش از سقف مجاز در روز میباشد',
            '202' => 'مبلغ تراکنش بیش از سقف مجاز در ماه میباشد',
            '-20' => 'وجود کاراکترهای غیرمجاز در درخواست',
            '-30' => 'تراکنش قبلا برگشت خورده است',
            '-50' => 'طول رشته درخواست غیر مجاز است',
            '-51' => 'خطا در درخواست',
            '-80' => 'تراکنش مورد نظر یافت نشد',
            '-81' => 'خطای داخلی بانک',
            '-90' => 'تراکنش قبلا تایید شده است',
        );
        if(isset($arr[$result]))
            $this->responseError($this->tid, @$arr[$result]);
        else
            $this->responseError($this->tid, 'خطای نامشخص : '.@$result);
    }
}
?>