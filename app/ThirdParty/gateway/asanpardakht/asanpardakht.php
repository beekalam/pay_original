<?php
class asanpardakht extends Gateway
{
    private $ap_merchant_id;
    private $ap_username;
    private $ap_pass;
    private $ap_key;
    private $ap_iv;
    private $client;
    
    public function __construct()
    {
        if(!extension_loaded('mcrypt')) {
            Err::view(0, 'Mcrypt extension required!');
            return;
        }
        if(!extension_loaded('soap')) {
            Err::view(0,'Soap Extension Required',true);
            return;
        }
        $this->ap_merchant_id = ASANPARDAKHT_MERCHANT_ID;
        $this->ap_username = ASANPARDAKHT_USERNAME;
        $this->ap_pass = ASANPARDAKHT_PASSWORD;
        $this->ap_key = ASANPARDAKHT_KEY;
        $this->ap_iv = ASANPARDAKHT_IV;
        //require_once(CORE_PATH.'nusoap/nusoap.php');
        try {
            $opts = array('ssl' => array('verify_peer'=>false, 'verify_peer_name'=>false));
            $params = array('stream_context' => stream_context_create($opts));
            $this->client = new SoapClient('https://services.asanpardakht.net/paygate/merchantservices.asmx?WSDL',$params);
            //$this->client = new nusoap_client('https://services.asanpardakht.net/paygate/merchantservices.asmx?wsdl',true);
        } catch (SoapFault $e) {
            Err::view(0,'Can not connect to Asanpardakht servers',true);
        }
        // Check for an error
        /*$err = $this->client->getError();
        if($err) {
            Err::view(0,'Can not connect to Asanpardakht servers',true);
            die();
        }*/
    }
    
    public function request($value,$tid)
    {
        load('date');
        $data = array(
            1,
            $this->ap_username,
            $this->ap_pass,
            $tid,
            $value,
            date('Ymd His'),
            '',
            $GLOBALS['callback_url'],
            0
        );
        $data = implode(',',$data);
        $data = $this->encrypt($data);
        $params = array(
            'merchantConfigurationID' => $this->ap_merchant_id,
            'encryptedRequest' => $data,
        );

        // Call the SOAP method
        $result = $this->client->RequestOperation($params);
        
        // Check for a fault
        if($result === false) {
            $this->errorCode = 'error';
            $this->debugData = $result;
            return false;
        }
        $result = $result->RequestOperationResult;
        //$result = $this->decrypt($result);
        $res = explode(',',$result);
        if($res[0] == 0) {
            DB::update('invoices',array('bank_ref'=>$res[1]),'id='.$tid);
            return true;
        } else {
            $this->errorCode = $res[0];
            $this->debugData = $result;
            return false;
        }
    }
    
    public function toBank($invoice)
    {
        $form = '<form action="https://asan.shaparak.ir" method="POST">
    <input type="hidden" name="RefId" value="'.$invoice['bank_ref'].'" />
    <input type="submit" value="ادامه" style="display:none;" />
</form>';
        $this->autoForm($form);
    }
    
    public function process($invoice)
    {
        $this->tid = $tid = $invoice['id'];
        if(@$_POST['ReturningParams']!='') {
            $res = $this->decrypt($_POST['ReturningParams']);
            $res = explode(',',$res);
            /*
            1: amount (rial)
            2: inv id
            3: refId
            4: Res Code
            5: Message Text (consist of first 6 digit of payer card number)
            6: rrn
            7: last 4 digits of card number
            */
            if($res[3] == '0' || $res[3] == '00') {
                $payGateTranId = $res[5];
                $data = $this->encrypt($this->ap_username.','.$this->ap_pass);
                $params = array(
                    'merchantConfigurationID' => $this->ap_merchant_id,
                    'encryptedCredentials' => $data,
                    'payGateTranID' => $payGateTranId,
                );
                $result = $this->client->RequestVerification($params);
                $result = $result->RequestVerificationResult;
                
                #file_put_contents('params.txt',var_export($_POST,true)."\n\n\n".var_export($res,true)."\n\n\n".var_export($result,true));
                if($result == '500') {
                    $result2 = $this->client->RequestReconciliation($params);
                    $result2 = $result2->RequestReconciliationResult;
                    if($result2 == '600') {
                        $tmp = explode(':',$res[4]);
                        if(($tmp[1] == (int)$tmp[1]) && strlen($tmp[1]) == 6) {
                            $pan = $tmp[1];
                        } else {
                            $pan = '';
                        }
                        $pan .= '******'.$res[7];
                        $this->responseOK($res[6],$pan);
                        return true;
                    } else {
                        $this->return_error($result2);
                    }
                } else {
                    $this->return_error($result);
                }
            } else {
                $this->return_error($res[3]);
            }
        } else
            $this->return_error(@$_POST['State']);
        return false;
    }
    
    private function return_error($result)
    {
        $arr = array(
            '1' => 'صادركننده كارت از انجام تراكنش صرف نظر كرد.',
            '2' => 'عمليات تاييديه اين تراكنش قبلاً با موفقيت صورت پذيرفته است.',
            '3' => 'پذيرنده فروشگاهي نامعتبر مي باشد',
            '4' => 'كارت توسط دستگاه ضبط شود.',
            '5' => 'به تراكنش رسيدگي نشد.',
            '6' => 'بروز خطا.',
            '7' => 'به دليل شرايط خاص كارت توسط دستگاه ضبط شود',
            '8' => 'با تشخيص هويت دارنده ي كارت، تراكنش موفق مي باشد.',
            '12' => 'تراكنش نامعتبر است.',
            '13' => 'مبلغ تراكنش اصلاحيه نادرست است.',
            '14' => 'شماره كارت ارسالي نامعتبر است.(وجود ندارد)',
            '15' => 'صادر كننده ي كارت نامعتبر است.(وجود ندارد)',
            '16' => 'تراكنش مورد تاييد است و اطلاعات شيار سوم كارت به روز رساني شود',
            '19' => 'تراكنش مجدداً ارسال شود.',
            '23' => 'كارمزد ارسالي پذيرنده غير قابل قبول است.',
            '25' => 'تراكنش اصلي يافت نشد.',
            '30' => 'قالب پيام داراي اشكال است.',
            '31' => 'پذيرنده توسط سوئيچ پشتيباني نمي شود.',
            '33' => 'تاريخ انقضاي كارت سپري شده است',
            '34' => 'تراكنش اصلي با موفقيت انجام نپذيرفته است.',
            '36' => 'كارت محدود شده است كارت توسط دستگاه ضبط شود.',
            '38' => 'تعداد دفعات ورود رمز غلط بيش از حد مجاز است',
            '39' => 'كارت حساب اعتباري ندارد.',
            '40' => 'عمليات درخواستي پشتيباني نمي گردد.',
            '41' => 'كارت مفقودي مي باشد. كارت توسط دستگاه ضبط شود.',
            '42' => 'كارت حساب عمومي ندارد.',
            '43' => 'كارت مسروقه مي باشد. كارت توسط دستگاه ضبط شود.',
            '44' => 'كارت حساب سرمايه گذاري ندارد.',
            '51' => 'موجودي كافي نمي باشد.',
            '52' => 'كارت حساب جاري ندارد.',
            '53' => 'كارت حساب قرض الحسنه ندارد.',
            '54' => 'تاريخ انقضاي كارت سپري شده است.',
            '55' => 'رمز كارت نامعتبر است.',
            '56' => 'كارت نامعتبر است.',
            '57' => 'بانك شما اين تراكنش را پشتيباني نميكند',
            '58' => 'انجام تراكنش مربوطه توسط پايانه ي انجام دهنده مجاز نمي باشد.',
            '59' => 'كارت مظنون به تقلب است.',
            '61' => 'مبلغ تراكنش بيش از حد مجاز مي باشد.',
            '62' => 'كارت محدود شده است.',
            '63' => 'تمهيدات امنيتي نقض گرديده است.',
            '64' => 'مبلغ تراكنش اصلي نامعتبر است.( تراكنش مالي اصلي با اين مبلغ نمي باشد).',
            '65' => 'تعداد درخواست تراكنش بيش از حد مجاز مي باشد.',
            '67' => 'كارت توسط دستگاه ضبط شود.',
            '75' => 'تعداد دفعات ورود رمز غلط بيش از حد مجاز است.',
            '77' => 'روز مالي تراكنش نا معتبر است.',
            '78' => 'كارت فعال نيست.',
            '79' => 'حساب متصل به كارت نامعتبر است يا داراي اشكال است.',
            '80' => 'تراكنش موفق عمل نكرده است.',
            '84' => 'بانك صادر كننده كارت پاسخ نميدهد',
            '86' => 'موسسه ارسال كننده شاپرك يا مقصد تراكنش در حالت Signoff است',
            '90' => 'بانك صادركننده كارت درحال انجام عمليات پايان روز ميباشد',
            '91' => 'بانك صادر كننده كارت پاسخ نميدهد',
            '92' => 'مسيري براي ارسال تراكنش به مقصد يافت نشد. ( موسسه هاي اعلامي معتبر نيستند)',
            '93' => 'تراكنش با موفقيت انجام نشد. (كمبود منابع و نقض موارد قانوني)',
            '94' => 'ارسال تراكنش تكراري.',
            '96' => 'بروز خطاي سيستمي در انجام تراكنش.',
            '97' => 'فرايند تغيير كليد براي صادر كننده يا پذيرنده در حال انجام است.',
            501 => 'پردازش هنوز انجام نشده است',
            502 => 'وضعيت تراكنش نامشخص است',
            503 => 'تراكنش اصلي ناموفق بوده است',
            504 => 'قبلا درخواست بازبيني براي اين تراكنش داده شده است',
            505 => 'قبلا درخواست تسويه براي اين تراكنش ارسال شده است',
            506 => 'قبلا درخواست بازگشت براي اين تراكنش ارسال شده است',
            507 => 'تراكنش در ليست تسويه قرار دارد',
            508 => 'تراكنش در ليست بازگشت قرار دارد',
            509 => 'امكان انجام عمليات به سبب وجود مشكل داخلي وجود ندارد',
            510 => 'هويت درخواست كننده عمليات نامعتبر است',
            601 => 'پردازش هنوز انجام نشده است',
            602 => 'وضعيت تراكنش نامشخص است',
            603 => 'تراكنش اصلي ناموفق بوده است',
            604 => 'تراكنش بازبيني نشده است',
            605 => 'قبلا درخواست بازگشت براي اين تراكنش ارسال شده است',
            606 => 'قبلا درخواست تسويه براي اين تراكنش ارسال شده است',
            607 => 'امكان انجام عمليات به سبب وجود مشكل داخلي وجود ندارد',
            608 => 'تراكنش در ليست منتظر بازگشت ها وجود دارد',
            609 => 'تراكنش در ليست منتظر تسويه ها وجود دارد',
            610 => 'هويت درخواست كننده عمليات نامعتبر است',
        );
        if(isset($arr[$result]))
            $this->responseError($this->tid, @$arr[$result]);
        else
            $this->responseError($this->tid, 'خطای نامشخص : '.@$result);
    }
    
    private function addpadding($string, $blocksize = 32)
    {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
        return $string;
    }
    
    private function strippadding($string)
    {
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if(preg_match("/$slastc{".$slast."}/", $string)) {
            $string = substr($string, 0, strlen($string)-$slast);
            return $string;
        } else {
            return false;
        }
    }
    
    private function encrypt($string = "")
    {
        $key = base64_decode($this->ap_key);
        $iv = base64_decode($this->ap_iv);
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $this->addpadding($string), MCRYPT_MODE_CBC, $iv));
    }
    
    function decrypt($string = "")
    {
        $key = base64_decode($this->ap_key);
        $iv = base64_decode($this->ap_iv);
        $string = base64_decode($string);
        return $this->strippadding(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $string, MCRYPT_MODE_CBC, $iv));
    }
}
?>