<?php
class pep extends Gateway
{
	private $cert_file;
	private $terminal_code;
    private $merchant_code;
    private $sign;
    private $invoiceDate;
    private $timeStamp;
    private $action;
    private $redirectAddress;
	private $error;
	
	public function __construct()
	{
        require_once(CORE_PATH."gateway/pep/rsa.class.php");
        require_once(CORE_PATH."gateway/pep/RSAProcessor.class.php");
        require_once(CORE_PATH."gateway/pep/parser.php");
		$this->cert_file = INCLUDE_PATH.'pep_certificate.xml';
		$this->merchant_code = PEP_MERCHANT_CODE;
		$this->terminal_code = PEP_TERMINAL_CODE;
	}
	
	public function request($value,$tid)
	{
		$this->tid = $tid;
        return true;
        
        /*$this->redirectAddress = SITE_URL.'invoice/processTransaction/'.$GLOBALS['invoice_key'].'/';
        $this->timeStamp = date("Y/m/d H:i:s");
        $this->invoiceDate = date("Y/m/d H:i:s");
        $this->action = "1003";     // 1003 : baraye darkhaste kharid
        $data = "#". $this->merchant_code ."#". $this->terminal_code ."#". $tid ."#". $this->invoiceDate ."#". $value ."#". $this->redirectAddress ."#". $this->action ."#". $this->timeStamp ."#";
        $data = sha1($data,true);
        
        $processor = new RSAProcessor($this->cert_file, RSAKeyType::XMLFile);
        $data = $processor->sign($data); // sign 
        $sign = base64_encode($data); // base64_encode 
        
        $this->sign = $sign;*/
	}
    
    public function toBank($invoice)
    {
        $this->redirectAddress = SITE_URL.'invoice/processTransaction/'.$invoice['id'].'_'.$invoice['hash'].'/';
        $this->timeStamp = date("Y/m/d H:i:s");
        $this->invoiceDate = date("Y/m/d H:i:s",strtotime($invoice['date']));
        $this->action = "1003";     // 1003 : baraye darkhaste kharid
        $data = "#". $this->merchant_code ."#". $this->terminal_code ."#". $invoice['id'] ."#". $this->invoiceDate ."#". $invoice['amount'] ."#". $this->redirectAddress ."#". $this->action ."#". $this->timeStamp ."#";
        $data = sha1($data,true);
        
        $processor = new RSAProcessor($this->cert_file, RSAKeyType::XMLFile);
        $data = $processor->sign($data); // sign 
        $sign = base64_encode($data); // base64_encode 
        
        $this->sign = $sign;
        
        
        $form = "<form method='post' action='https://pep.shaparak.ir/gateway.aspx'>
<input type='hidden' name='invoiceNumber' value='".$invoice['id']."' /><br />
<input type='hidden' name='invoiceDate' value='".$this->invoiceDate."' /><br />
<input type='hidden' name='amount' value='".$invoice['amount']."' /><br />
<input type='hidden' name='terminalCode' value='".$this->terminal_code."' /><br />
<input type='hidden' name='merchantCode' value='".$this->merchant_code."' /><br />
<input type='hidden' name='redirectAddress' value='".$this->redirectAddress."' /><br />
<input type='hidden' name='timeStamp' value='".$this->timeStamp."' /><br />
<input type='hidden' name='action' value='".$this->action."' /><br />
<input type='hidden' name='sign' value='".$this->sign."' /><br />
</form>";
        $this->autoForm($form);
    }
    
    public function process($invoice)
    {
        $this->tid = $invoice['id'];
        $fields = array('invoiceUID' => $_GET['tref'] );
        $result = post2https($fields,'https://pep.shaparak.ir/CheckTransactionResult.aspx');
        $array = makeXMLTree($result);
        $res = $array['resultObj'];
        $flag = false;
        if($res['result'] == 'True') {
            if($res['action'] == '1003') {
                if($res['invoiceNumber'] == $invoice['id']) {
                    if($res['terminalCode'] == $this->terminal_code) {
                        if($res['merchantCode'] == $this->merchant_code) {
                            if($res['amount'] == $invoice['amount']) {
                                if($this->verifyPayment($invoice)) {
                                    $this->responseOK($_GET['tref']);
                                    return true;
                                } else {
                                    $error = $this->error;
                                }
                            } else {
                                $error = 'مبلغ همخوانی ندارد';
                            }
                        } else {
                            $error = 'کد مرچنت همخوانی ندارد';
                        }
                    } else {
                        $error = 'شماره ترمینال همخوانی ندارد';
                    }
                } else {
                    $error = 'شماره تراکنش همخوانی ندارد';
                }
            } else {
                $error = 'نوع تراکنش همخوانی ندارد';
            }
        } else {
            $error = 'تراکنش انجام نشده است';
        }
        
        
        $this->responseError($invoice['id'], $error);
        return false;
    }
	
	public function verifyPayment($invoice)
	{
        $processor = new RSAProcessor($this->cert_file, RSAKeyType::XMLFile);
        $this->invoiceDate = date("Y/m/d H:i:s", strtotime($invoice['date']));
        $this->timeStamp = date("Y/m/d H:i:s");
        $data = "#". $this->merchant_code ."#". $this->terminal_code ."#". $invoice['id'] ."#". $this->invoiceDate ."#". $invoice['amount'] ."#". $this->timeStamp ."#";
        $data = sha1($data,true);
        $data =  $processor->sign($data);
        $this->sign =  base64_encode($data); // base64_encode 
        
        $post = array(
            'MerchantCode' => $this->merchant_code,             //shomare ye moshtari e shoma.
            'TerminalCode' => $this->terminal_code,             //shomare ye terminal e shoma.
            'InvoiceNumber' => $invoice['id'],              //shomare ye factor tarakonesh.
            'InvoiceDate' => $this->invoiceDate, //tarikh e tarakonesh.
            'amount' => $invoice['amount'],                     //mablagh e tarakonesh. faghat adad.
            'TimeStamp' => $this->timeStamp,     //zamane jari ye system.
            'sign' => $this->sign                             //reshte ye ersali ye code shode. in mored automatic por mishavad. 
        );
        $verifyresult = post2https($post,'https://pep.shaparak.ir/VerifyPayment.aspx');
        $array = makeXMLTree($verifyresult);
        $res = $array['actionResult'];
			
		if($res['result'] == 'True') {
			return true;
		} else {
			$this->error = $res['resultMessage'];
			return false;
		}
	}
}
?>