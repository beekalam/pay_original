<?php
class mellat extends Gateway
{
	private $terminal_code;
	private $username;
	private $password;
	private $namespace = 'http://interfaces.core.sw.bps.com/';
    private $client;
	
	public function __construct()
	{
		$this->terminal_code = MELLAT_TERMINAL_CODE;
		$this->username = MELLAT_USERNAME;
		$this->password = MELLAT_PASSWORD;
		require_once(CORE_PATH.'nusoap/nusoap.php');
        try {
            $this->client = new nusoap_client('https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl');
        } catch (Exception $e) {
            try {
                $this->client = new nusoap_client('https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl');
            } catch (Exception $e) {
                Err::view(0,'Can not connect to bankmellat servers',true);
            }
        }
		// Check for an error
		$err = $this->client->getError();
		if($err) {
			Err::view(0,'Can not connect to bankmellat servers',true);
			die();
		}
	}
    
    public function request($value,$tid)
    {
        load('date');
        $parameters = array(
            'terminalId' => $this->terminal_code,
            'userName' => $this->username,
            'userPassword' => $this->password,
            'orderId' => $tid,
            'amount' => $value,
            'localDate' => date('Ymd'),
            'localTime' => date('His'),
            'additionalData' => '',
            'callBackUrl' => (isset($GLOBALS['callback_url'])?$GLOBALS['callback_url']:(SITE_URL.'invoice/processTransaction/'.$GLOBALS['invoice_key'].'/')),
            'payerId' => 0);

        // Call the SOAP method
        $result = $this->client->call('bpPayRequest', $parameters, $this->namespace);
        
        // Check for a fault
        if($this->client->fault) {
            $this->errorCode = 'fault';
            $this->debugData = var_export($result,true);
            return false;
        }
        $res = explode(',',$result);
        if($res[0] == 0) {
            DB::update('invoices',array('bank_ref'=>$res[1]),'id='.$tid);
            return true;
        } else {
            $this->errorCode = $res[0];
            $this->debugData = $result;
            return false;
        }
    }
	
	public function toBank($invoice)
	{
		$form = '<form action="https://bpm.shaparak.ir/pgwchannel/startpay.mellat" method="POST">
	<input type="hidden" name="RefId" value="'.$invoice['bank_ref'].'" />
	<input type="submit" value="ادامه" style="display:none;" />
</form>';
		$this->autoForm($form);
	}
	
	public function process($invoice)
	{
		$res = @$_POST['ResCode'];
		$SaleReferenceId = @$_POST['SaleReferenceId'];
		$this->tid = $saleOrderId = @$_POST['SaleOrderId'];
		if($res == 0 && $saleOrderId == $invoice['id']) {
			$parameters = array(
				'terminalId' => $this->terminal_code,
				'userName' => $this->username,
				'userPassword' => $this->password,
				'orderId' => $invoice['id'],
				'saleOrderId' => $invoice['id'],
				'saleReferenceId' => $SaleReferenceId,
			);

			// Call the SOAP method
			$result = $this->client->call('bpVerifyRequest', $parameters, $this->namespace);
			
			// Check for a fault
			if($this->client->fault) {
				echo '<h2>Fault</h2><pre>';
				print_r($result);
				echo '</pre>';
				die();
			}
			
			$err = $this->client->getError();
			if($err)
			{
				// Display the error
				echo '<h2>Error</h2><pre>' . $err . '</pre>';
				die();
			} 
			elseif($result == 0)
			{
				// Call the SOAP method
				$sresult = $this->client->call('bpSettleRequest', $parameters, $this->namespace);
                $cardNumber = @$_POST['CardHolderPan'];
                $this->responseOK($SaleReferenceId, $cardNumber);
				return true;
			}
			else
			{
				return $this->inquery($parameters);
				//$this->responseError($order,@$arr[$result]);
			}
		}
		else
			$this->return_error($res, $parameters);
		return false;
	}
	
	private function inquery($params)
	{
		$result = $this->client->call('bpInquiryRequest', $params, $this->namespace);
		// Check for a fault
		if ($this->client->fault) {
			echo '<h2>Fault</h2><pre>';
			print_r($result);
			echo '</pre>';
			die();
		}
		
		$err = $this->client->getError();
		if($err)
		{
			// Display the error
			echo '<h2>Error</h2><pre>' . $err . '</pre>';
			die();
		} 
		elseif($result == 0)
		{
			// Call the SOAP method
			$sresult = $this->client->call('bpSettleRequest', $params, $this->namespace);
            $cardNumber = @$_POST['CardHolderPan'];
            $this->responseOK($SaleReferenceId, $cardNumber);
			return true;
		}
		else
		{
			$this->return_error($result, $params);
		}
		return false;
	}
	
	private function return_error($result, $params)
	{
		$arr = array(
			11 => 'شماره کارت نامعتبر است',
			12 => 'موجودی کافی نیست',
			13 => 'رمز نادرست است',
			14 => 'تعداد دفعات وارد کردن رمز بیش از حد مجاز است',
			15 => 'کارت نامعتبر است',
			16 => 'دفعات برداشت وجه بیش از حد مجاز است',
			17 => 'کاربر از انجام تراکنش منصرف شده است',
			18 => 'تاریخ انقضای کارت گذشته است',
			19 => 'مبلغ برداشت وجه بیش از حد مجاز است',
		);
		$this->reverse($params);
		$this->responseError($this->tid, @$arr[$result]);
	}
	
	private function reverse($params)
	{
		$result = $this->client->call('bpReversalRequest', $params, $this->namespace);
	}
}
?>