<?php
class fanava extends Gateway
{
    private $fanava_merchant_id;
    private $fanava_username;
    private $fanava_password;
    private $client;
    private $error;
    private $fanava_session;
    
    public function __construct()
    {
        $this->fanava_merchant_id = FANAVA_MERCHANT_ID;
        $this->fanava_username  = FANAVA_USERNAME;
        $this->fanava_password = FANAVA_PASSWORD;
    }
    
    private function login()
    {
        require_once(CORE_PATH.'nusoap/nusoap.php');
        $this->client = new nusoap_client('https://fcp.shaparak.ir/ref-payment/jax/merchantAuth?wsdl',true);
        $err = $this->client->getError();
        if($err) {
            $this->error = 'fanava connection error';
        }
        
        $info = new StdClass();  
        $info->username  = $this->fanava_username;  
        $info->password  = $this->fanava_password;
        $loginResponse = $this->client->call('login', array('loginRequest' => $info));

        // Check for a fault
        if($this->client->fault) {
            $this->error = 'fanava login error';
        } else {
            // Check for errors
            $err = $this->client->getError();
            if($err) {
                // Display the error
                $this->error = $err;
            } else {
                $this->fanava_session = $loginResponse['return'];
            }
        }
    }
    
    public function request($value,$tid)
    {
        $this->tid = $tid;
        return true;
    }
    
    public function toBank($invoice)
    {
        #$url = SITE_URL.'invoice/processTransaction/'.$invoice['id'].'_'.$invoice['hash'].'/';
        $url = (isset($GLOBALS['callback_url'])?$GLOBALS['callback_url']:(SITE_URL.'invoice/processTransaction/'.$invoice['id'].'_'.$invoice['hash'].'/'));
        $form = '
<form method="post" action="https://fcp.shaparak.ir/_ipgw_/payment/simple/">
<input type="hidden" name="resNum" value="'.$invoice['id'].'" />
<input type="hidden" name="Amount" value="'.$invoice['amount'].'" />
<input type="hidden" name="MID" value="'.$this->fanava_merchant_id.'" />
<input type="hidden" name="redirectURL" value="'.$url.'" />
<input type="hidden" name="language" value="fa" />
</form>';
        $this->autoForm($form);
    }
    
    public function process($invoice)
    {
        $this->tid = $invoice['id'];
        
        $reservationNumber = @$_POST["ResNum"];
        $refrenceNumber = @$_POST['RefNum'];
        $trace_num = @$_POST['TraceNo'];
        $card_num = @$_POST['CardMaskPan'];
        $State = @$_POST['State'];
        
        if(!empty($refrenceNumber)) {
            DB::update('invoices',array('bank_ref'=>$refrenceNumber),'id='.$invoice['id']);
        }
        
        if(strtolower($State)=='ok') {
            $this->login();
            if(empty($this->error)) {
                $contextinfo = new stdClass();
                $contextinfo->data = new stdClass();
                $contextinfo->data->entry = array('key'=>'SESSION_ID','value'=>$this->fanava_session);
                $requestinfo = new StdClass();
                $requestinfo->refNumList = $refrenceNumber;
                $str1 = $this->client->call("verifyTransaction", array('context' =>$contextinfo,'verifyRequest' =>$requestinfo));
                $amount = ($str1['return']['verifyResponseResults']['amount']);

                if($invoice['amount'] == $amount) {
                    $this->responseOK($trace_num, $card_num);
                    return true;
                } else {
                    $this->responseError($this->tid,'عدم همخوانی مبلغ تراکنش ('.$amount.')');
                }
            } else {
                $this->responseError($this->tid,$this->error);
            }
        } else {
            $this->responseError($this->tid,'پرداخت انجام نشده است');
        }
        return false;
    }
}
?>