<?php
class meli extends Gateway
{
    private $terminal_id;
    private $merchant_id;
    private $password;

    public function __construct()
    {
        $this->terminal_id = MELI_TERMINAL_ID;
        $this->merchant_id = MELI_MERCHANT_ID;
        $this->password = MELI_PASSWORD;

    }

    public function request($amount,$tid)
    {
        load('Curl');
        $url = 'https://sadad.shaparak.ir/VPG/api/v0/Request/PaymentRequest';
        $post_data = array();
        $post_data['MerchantId'] = $this->merchant_id;
        $post_data['TerminalId'] = $this->terminal_id;
        $post_data['Amount'] = $amount;
        $post_data['OrderId'] = $tid;
        $post_data['LocalDateTime'] = date("Y/m/d H:i:s");
        $post_data['ReturnUrl'] = (isset($GLOBALS['callback_url'])?$GLOBALS['callback_url']:(SITE_URL.'invoice/processTransaction/'.$GLOBALS['invoice_key'].'/'));
        $post_data['SignData'] = $this->encryptData($post_data['TerminalId'].';'.$post_data['OrderId'].';'.$post_data['Amount'],$this->password);

        $data_string = json_encode($post_data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $capt = curl_exec($ch);
        $res = json_decode($capt,true);
        if($res['Token'] != '') {
            DB::update('invoices',array('bank_ref'=>$res['Token']),'id='.$tid);
            return true;
        } else {
            $this->errorCode = $res['Description'];
            $this->debugData = $res;
            return false;
        }
    }

    public function toBank($invoice)
    {
        Routing::redirect('https://sadad.shaparak.ir/VPG/Purchase?token='.$invoice['bank_ref']);
        /*$form = '<form action="https://sadad.shaparak.ir/VPG/Purchase?token='.$invoice['bank_ref'].'" method="GET">
	<input type="submit" value="ادامه" style="display:none;" />
</form>';
        $this->autoForm($form);*/
    }

    public function process($invoice)
    {
        $this->tid = $invoice['id'];
        //$OrderId=$_POST['OrderId'];
        if($_POST['ResCode'] == 0)
        {
            $url = 'https://sadad.shaparak.ir/VPG/api/v0/Advice/Verify';
            $post_data = array();
            $post_data['Token'] = $invoice['bank_ref'];
            $post_data['SignData'] = $this->encryptData($post_data['Token'],$this->password);
            $data_string = json_encode($post_data);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $capt = curl_exec($ch);
            $res = json_decode($capt,true);
            if($res['ResCode'] === '0') {
                //$res['SystemTraceNo']; shomare peygiri
                $this->responseOK($res['RetrivalRefNo']); // shomare erja
                return true;
            } else {
                $this->responseError($this->tid, $res['resCode'].' - '.$res['Description']);
                return false;
            }
        } else {
            $this->responseError($invoice['id'],@$_POST['ResCode']);
            return false;
        }
    }

    private function encryptData($plainText,$key)
    {

        $byte = mb_convert_encoding($key, 'ASCII');

        $desKey = base64_decode($key);
        $data = mb_convert_encoding($plainText, 'ASCII');

        // add PKCS#7 padding
        $blocksize = @mcrypt_get_block_size('tripledes', 'ecb');
        $paddingSize = $blocksize - (strlen($data) % $blocksize);
        $data .= str_repeat(chr($paddingSize), $paddingSize);

        // encrypt password
        $encData = @mcrypt_encrypt('tripledes', $desKey, $data, 'ecb');

        return base64_encode($encData);
    }
}