<?php
class mabnacard extends Gateway
{
    private $mabna_public_key;
    private $mabna_private_key;
    private $mabna_merchant_id;
    private $mabna_terminal_id;
    private $client;
    private $key_resource = false;
    
    public function __construct()
    {
        $this->mabna_public_key = trim("
-----BEGIN PUBLIC KEY-----
" . trim(MABNA_PUBLIC_KEY) . "
-----END PUBLIC KEY-----
");
        $this->mabna_private_key = trim("
-----BEGIN PRIVATE KEY-----
" . trim(MABNA_PRIVATE_KEY) . "
-----END PRIVATE KEY-----
");
        $this->mabna_merchant_id = MABNA_MERCHANT_ID;
        $this->mabna_terminal_id = MABNA_TERMINAL_ID;
        require_once(CORE_PATH.'nusoap/nusoap.php');
    }
    
    public function getClient($url)
    {
        $client = new nusoap_client($url,'wsdl');
        $client->soap_defencoding = 'UTF-8';
        $this->client = $client;
    }
    
    public function encode($str)
    {
        if($this->key_resource === false) {
            $this->key_resource = openssl_get_publickey($this->mabna_public_key);
        }
        openssl_public_encrypt($str, $res, $this->key_resource);
        return base64_encode($res);
    }
    
    public function sign($str)
    {
        $SIGNATURE = '';
        $PRIVATE_KEY = openssl_pkey_get_private($this->mabna_private_key);
        if(!openssl_sign($str, $SIGNATURE, $PRIVATE_KEY, OPENSSL_ALGO_SHA1)) {
            //"OPEN SSL SIGN ERROR";
            return false;
        } else {
            return $SIGNATURE;
            //echo base64_encode($SIGNATURE);
        }
    }
    
    public function request($amount,$tid)
    {
        $this->tid = $tid;
        
        $__AMT = $amount;
        $__CRN = time();
        $__MID = $this->mabna_merchant_id;
        $__TID = $this->mabna_terminal_id;
        $__REFADD = (isset($GLOBALS['callback_url'])) ? $GLOBALS['callback_url'] : (SITE_URL.'invoice/processTransaction/'.$GLOBALS['invoice_key'].'/');
        $SOURCE = $__AMT . $__CRN . $__MID . $__REFADD . $__TID;
        
        $AMOUNT = $this->encode($__AMT);
        $CRN = $this->encode($__CRN);
        $MID = $this->encode($__MID);
        $TID = $this->encode($__TID);
        $URL = $this->encode($__REFADD);
        
        $SIGNATURE = $this->sign($SOURCE);
        
        $INPUT_ARRAY = array(
            "Token_param" => array(
                "AMOUNT" => $AMOUNT,
                "CRN" => $CRN,
                "MID" => $MID,
                "REFERALADRESS" => $URL,
                "SIGNATURE" => base64_encode($SIGNATURE),
                "TID" => $TID,
            )
        );
        
        $this->getClient('https://mabna.shaparak.ir/TokenService?wsdl');
        $WS_RESULT = $this->client->call("reservation", $INPUT_ARRAY);
        $SIGNATURE = base64_decode($WS_RESULT["return"]["signature"]);
        $VERIFY_RESULT = openssl_verify($WS_RESULT["return"]["token"], $SIGNATURE, $this->key_resource);
        openssl_free_key($this->key_resource);
        if($VERIFY_RESULT == 1) {
            DB::update('invoices',array('bank_ref'=>$WS_RESULT["return"]["token"]),'id='.$tid);
            return true;
        } elseif ($VERIFY_RESULT == 0) {
            $this->errorCode = 'request failed';
            return false;
        } else {
            $this->errorCode = 'checking signature failed';
            return false;
        }
    }
    
    public function toBank($invoice)
    {
        $form = '<form method="POST" action="https://mabna.shaparak.ir" style="display: none">
<p><input type="submit" value="GO TO PAYMENT PAGE"></p>
<input type="hidden" name="TOKEN" value="'.$invoice['bank_ref'].'">
</form>';
        $this->autoForm($form);
    }
    
    public function process($invoice)
    {
        $this->tid = $invoice['id'];
        
        $this->getClient('https://mabna.shaparak.ir/TransactionReference/TransactionReference?wsdl');
        $__CRN = @$_POST['CRN'];
        $__MID = $this->mabna_merchant_id;
        $__TID = $this->mabna_terminal_id;
        $__TRN = @$_POST['TRN'];
        $SOURCE = $__MID . $__TRN . $__CRN;
        
        $CRN = $this->encode($__CRN);
        $MID = $this->encode($__MID);
        $TRN = $this->encode($__TRN);
        
        $SIGNATURE = $this->sign($SOURCE);
        
        $INPUT_ARRAY = array(
            "SaleConf_req" => array(
                "MID" => $MID,
                "CRN" => $CRN,
                "TRN" => $TRN,
                "SIGNATURE" => base64_encode($SIGNATURE)
            )
        );
        $WSResult = $this->client->call("sendConfirmation", $INPUT_ARRAY);
        $SIGNATURE = base64_decode($WSResult["return"]["SIGNATURE"]);
        $DATA = $WSResult["return"]["RESCODE"] . $WSResult["return"]["REPETETIVE"] . $WSResult["return"]["AMOUNT"] . $WSResult["return"]["DATE"] . $WSResult["return"]["TIME"] . $WSResult["return"]["TRN"] . $WSResult["return"]["STAN"];
        $VERIFY_RESULT = openssl_verify($DATA, $SIGNATURE, $this->key_resource);
        if(!empty($WSResult['return']['RESCODE'])) {
            // success
            if(($WSResult['return']['RESCODE'] == '00') && ($WSResult['return']['successful'] == true)) {
                $this->responseOK($WSResult['return']['TRN']);
                return true;
            } elseif ($WSResult['return']['RESCODE'] == '200') { // cancel
                $this->responseError($this->tid,'تراکنش توسط کاربر  کنسل شد');
            } elseif ($WSResult['return']['RESCODE'] == '107') { // cancel
                $this->responseError($this->tid,'تراکنش توسط کاربر  کنسل شد');
            } elseif ($WSResult['return']['description'] != "") { // other problem
                $this->responseError($this->tid,$WSResult['return']['description']);
            } else {
                $this->responseError($this->tid,'خطای نامشخص ('.$WSResult['return']['RESCODE'].')');
            }
        } else {
            $this->responseError($this->tid,'تراکنش نامعتبر است');
        }
        return false;
    }
}
?>