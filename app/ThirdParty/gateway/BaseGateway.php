<?php
class BaseGateway
{
	public $tid;
    public $errorCode;
    public $debugData;
	
	protected function autoForm($form,$showRef=false)
	{
		echo $form;
		die('');
		// Template::setTitle('');
		// Template::set('form',$form);
        // if(defined('RETURN_CUSTOM_RESULT')) {
		//     $res = Template::load('core','payment','form',2);
        //     $GLOBALS['CUSTOM_RESULT_FORM'] = $res;
        // } else {
        //     Template::load('core','payment','form',1);
        //     die('');
        // }
	}

	protected function responseOK($bank_code = '', $card_number='')
	{
		$arr = array(
			'status' => 1,
			'bank_code' => $bank_code,
		);
        if(!empty($card_number)) {
            $arr['card_number'] = $card_number;
		}
		$db = app()->make('db');
		$db->table('invoices')->where('id',$this->tid)->update($arr);
		$redirect_url = $db->table('invoices')->where('id',$this->tid)->first()->return_url;
		var_dump($redirect_url);
		exit;
		// Header('Location: '. DB::table('invoices')->where('id',$this->tid)->get)
		// DB::update('invoices',$arr,'id='.$this->tid);
	}

	protected function responseOK_old($bank_code = '', $card_number='')
	{
		$arr = array(
			'status' => 1,
			'bank_code' => $bank_code,
		);
        if(!empty($card_number)) {
            $arr['card_number'] = $card_number;
        }
		DB::update('invoices',$arr,'id='.$this->tid);
	}

	public function responseError($tid, $error_text = '')
	{
		$db = app()->make('db');
		$arr = array(
			'status'=>2,
            'bank_error'=>$error_text,
        );
		$db->table('invoices')->where('id',$tid)->update($arr);
		Header('Location: ' . $db->table('invoices')->where('id',$tid)->first()->return_url);
		exit;
		//if(!empty($error_text))
			//Template::set('error_text',$error_text);
		//Template::setTitle('خطا در پرداخت آنلاین');
		//Template::load('site','payment','error');
	}

	public function responseError_old($tid, $error_text = '')
	{
		DB::update('invoices',array(
            'status'=>2,
            'bank_error'=>$error_text,
		),'id='.(int)$tid);
		
		//if(!empty($error_text))
			//Template::set('error_text',$error_text);
		//Template::setTitle('خطا در پرداخت آنلاین');
		//Template::load('site','payment','error');
	}
	
	protected function connectionError($tid=null)
	{
		//if($tid != null)
			//DB::update('transactions',array('status'=>2),'id='.(int)$tid);
		//Template::setTitle('خطا در برقراری ارتباط با سرور بانک');
		//Template::load('site','payment','cerror');
	}
}
?>