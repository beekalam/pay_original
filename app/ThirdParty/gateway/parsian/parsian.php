<?php
class parsian extends Gateway
{
    private $parsian_pin;
    private $client;
    
    public function __construct()
    {
        if(extension_loaded('soap')) {
            $this->parsian_pin = PARSIAN_PIN;
            //$this->client = new SoapClient('https://pec.shaparak.ir/pecpaymentgateway/eshopservice.asmx?wsdl');
        } else
            Err::view(0,'Soap Extension Required',true);
    }
    
    private function getClient($url)
    {
        $this->client = new SoapClient($url);
    }
    
    public function request($amount,$tid)
    {
        $this->getClient('https://pec.shaparak.ir/NewIPGServices/Sale/SaleService.asmx?wsdl');
        $params = array (
            "LoginAccount" => $this->parsian_pin,
            "Amount" => $amount,
            "OrderId" => $tid,
            "CallBackUrl" => isset($GLOBALS['callback_url'])?$GLOBALS['callback_url']:(SITE_URL.'invoice/processTransaction/'.$GLOBALS['invoice_key'].'/'), 
        );
        $res = $this->client->SalePaymentRequest(array('requestData' => $params));
        file_put_contents(ROOT_PATH.'result.txt',var_export($res,true));
        $res = $res->SalePaymentRequestResult;
        $token = $res->Token;
        $status = $res->Status;

        if($token && $status===0) {
            DB::update('invoices',array('bank_ref'=>$token),'id='.$tid);
            return true;
        } else {
            $this->errorCode = $status;
            $this->debugData = $res;
            return false;
        }
    }
    
    public function toBank($invoice)
    {
        $form = '<form action="https://pec.shaparak.ir/NewIPG/?Token='.$invoice['bank_ref'].'" method="POST">
<input type="submit" value="ادامه" style="display:none;" />
</form>';
        $this->autoForm($form);
    }
    
    public function process($invoice)
    {
        $tid = $this->tid = $invoice['id'];
        if($invoice = DB::getRow('invoices','id='.$tid)) {
            $token = $_POST['Token'];
            $status = $_POST['status'];
            $order_id = $_POST['OrderIds'];
            $terminal_no = $_POST['TerminalNo'];
            $rrn = $_POST['RRN'];

            $this->getClient('https://pec.shaparak.ir/NewIPGServices/Confirm/ConfirmService.asmx?wsdl');

            if($rrn > 0 && ($status==0)) {
                $params = array(
                    'LoginAccount' => $this->parsian_pin,
                    'Token' => $invoice['bank_ref']
                );
                $res = $this->client->ConfirmPayment(array('requestData' => $params));
                $res = $res->ConfirmPaymentResult;
                $status = $res->Status;
                $rrn2 = $res->RRN;
                $card_num = $res->CardNumberMasked;

                if($status==0) {
                    $this->responseOK($rrn2,$card_num);
                    return true;
                } else {
                    $this->responseError($tid,'کد خطا '.$status);
                }
            } elseif($status) {
                if($status == 5) {
                    $this->responseError($tid, 'انصراف از پرداخت');
                } else {
                    $this->responseError($tid, 'کد خطا '.$status);
                }
            } else
                $this->responseError($tid, 'پاسخی از سرور بانک دریافت نشد');
        } else
            $this->responseError($tid, 'مشکل در اطلاعات دریافتی سرور پرداخت آنلاین');
        return false;
    }
}
?>