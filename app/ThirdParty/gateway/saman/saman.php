<?php
require_once base_path("app/ThirdParty/gateway/BaseGateway.php");
require_once base_path("app/ThirdParty/nusoap/nusoap.php");

class saman extends BaseGateway
{
    private $saman_merchant_id;
    private $client;
    
    public function __construct($saman_merchant_id)
    {
        $this->saman_merchant_id = $saman_merchant_id;
        $client = new nusoap_client('https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL','wsdl');
        //$this->client = $client;
        $this->client = $client->getProxy();
    }
    
    public function request($value,$tid)
    {
        return true;
    }
    
    public function toBank($invoice)
    {
        $key = $invoice->id.'_'.$invoice->hash;
        // $process_trans_url = 'http://pal.sabanovin.com/invoice/processTransaction/'.$key;
        $process_trans_url = route('invoice.process_transaction',['key' => $key]);
        $form = '<form action="https://sep.shaparak.ir/Payment.aspx" method="POST">
<input type="hidden" name="MID" value="'.$this->saman_merchant_id.'">
<input type="hidden" name="RedirectURL" value="'.$process_trans_url.'">
<input type="hidden" name="Amount" value="'.$invoice->amount.'">
<input type="hidden" name="ResNum" value="'.$invoice->id.'"></form>';
        return $form;
    }
    
    public function toBank_old($invoice)
    {
        $key = $invoice['id'].'_'.$invoice['hash'];
        $form = '<form action="https://sep.shaparak.ir/Payment.aspx" method="POST">
<input type="hidden" name="MID" value="'.$this->saman_merchant_id.'">
<input type="hidden" name="RedirectURL" value="'.SITE_URL.'invoice/processTransaction/'.$key.'">
<input type="hidden" name="Amount" value="'.$invoice['amount'].'">
<input type="hidden" name="ResNum" value="'.$invoice['id'].'"></form>';
        $this->autoForm($form);
    }
    
    public function process($invoice)
    {
        $this->tid = $tid = $invoice['id'];
        $db = app()->make('db');
        if(@$_POST['RefNum']!='' && @$_POST['State']=='OK') {
            // $inv = DB::getRow('invoices','bank_ref=?',$_POST['RefNum']);
            $inv = $db->table('invoices')->where('bank_ref',$_POST['RefNum'])->first();
            if(!$inv) {
                $res = $this->client->VerifyTransaction($_POST['RefNum'],$this->saman_merchant_id);
                //$res = $this->client->call('VerifyTransaction',array('String_1'=>$_POST['RefNum'],'String_2'=>$this->saman_merchant_id));
                if($res <= 0) {
                    $this->return_error($res);
                } else {
                    if($invoice['amount'] == $res) {
                        $traceNo = @$_POST['TRACENO'];
                        $refNo = @$_POST['RefNum'];
                        if(!empty($refNo)) {
                            // DB::update('invoices',array('bank_ref'=>$refNo),'id='.$invoice['id']);
                            $db->table('invoices')->where('id',$invoice['id'])->update(['bank_ref' => $refNo]);
                        }
                        $this->responseOK($traceNo,@$_POST['SecurePan']);
                        return true;
                    } else
                        $this->responseError($tid,'مبلغ پرداختی تطابق ندارد.<br>رسید دیجیتالی شما جهت پیگیری : '.$_POST['RefNum']);
                }
            } else {
                $this->responseError($tid,'رسید دیجیتالی تکراریست - ش.ت:'.$inv['id']);
            }
        } else
            $this->return_error(@$_POST['State']);
        return false;
    }

    public function process_old($invoice)
    {
        $this->tid = $tid = $invoice['id'];
        if(@$_POST['RefNum']!='' && @$_POST['State']=='OK') {
            $inv = DB::getRow('invoices','bank_ref=?',$_POST['RefNum']);
            if(!$inv) {
                $res = $this->client->VerifyTransaction($_POST['RefNum'],$this->saman_merchant_id);
                //$res = $this->client->call('VerifyTransaction',array('String_1'=>$_POST['RefNum'],'String_2'=>$this->saman_merchant_id));
                if($res <= 0) {
                    $this->return_error($res);
                } else {
                    if($invoice['amount'] == $res) {
                        $traceNo = @$_POST['TRACENO'];
                        $refNo = @$_POST['RefNum'];
                        if(!empty($refNo)) {
                            DB::update('invoices',array('bank_ref'=>$refNo),'id='.$invoice['id']);
                        }
                        $this->responseOK($traceNo,@$_POST['SecurePan']);
                        return true;
                    } else
                        $this->responseError($tid,'مبلغ پرداختی تطابق ندارد.<br>رسید دیجیتالی شما جهت پیگیری : '.$_POST['RefNum']);
                }
            } else {
                $this->responseError($tid,'رسید دیجیتالی تکراریست - ش.ت:'.$inv['id']);
            }
        } else
            $this->return_error(@$_POST['State']);
        return false;
    }
    
    private function return_error($result)
    {
        $arr = array(
            -1 => 'خطای در پردازش اطلاعات ارسالی',
            -3 => 'ورودی ها حاوی کارکترهای غیرمجاز می باشند',
            -4 => 'کد فروشنده اشتباه است',
            -5 => '-5',
            -6 => 'سند قبلا برگشت کامل یافته است',
            -7 => 'رسید دیجیتالی تهی است',
            -8 => 'طول ورودی ها بیشتر از حد مجاز است',
            -9 => 'وجود کارکترهای غیرمجاز در مبلغ برگشتی',
            -10 => 'رسید دیجیتالی به صورت Base64 نیست',
            -11 => 'طول ورودی ها کمتر از حد مجاز است',
            -12 => 'مبلغ برگشتی منفی است',
            -13 => 'مبلغ برگشتی برای برگشت جزئی بیش از مبلغ برگشت نخورده ی رسید دیجیتالی است',
            -14 => 'چنین تراکنشی تعریف نشده است',
            -15 => 'مبلغ برگشتی به صورت اعشاری داده شده است',
            -16 => 'خطای داخلی سیستم',
            -17 => 'برگشت زدن جزیی تراکنش مجاز نمی باشد',
            -18 => 'IP Address فروشنده نا معتبر است',
            'Canceled By User' => 'تراکنش توسط خریدار کنسل شده',
            'Invalid Amount' => 'مبلغ سند برگشتی، از مبلغ تراکنش بیشتر است',
            'Invalid Card Number' => 'شماره کارت اشتباه است',
            'No Such Issuer' => 'چنین صادر کننده کارتی وجود ندارد',
            'Expired Card Pick Up' => 'تاریخ انقضای کارت گذشته است',
            'Allowable PIN Tries Exceeded Pick Up' => 'رمز کارت باطل شده است',
            'Incorrect PIN' => 'رمز کارت اشتباه وارد شده',
            'Exceeds Withdrawal Amount Limit' => 'مبلغ بیش از سقف برداشت می باشد',
            'Transaction Cannot Be Completed' => 'خطای بانکی! مجدد تلاش کنید',
            'Response Received Too Late' => 'تاخیر در پاسخ شبکه بانکی',
            'Suspected Fraud Pick Up' => 'تاریخ انقضا یا cvv2 اشتباه بوده',
            'No Sufficient Funds' => 'عدم موجودی کافی',
            'Issuer Down Slm' => 'سیستم بانک صادر کننده کارت قطع است',
            'TME Error' => 'خطای بانکی! مجدد تلاش کنید',
        );
        if(isset($arr[$result]))
            $this->responseError($this->tid, @$arr[$result]);
        else
            $this->responseError($this->tid, 'خطای نامشخص : '.@$result);
    }
}
?>