<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model
{
    protected $table = 'terminals';
    protected $fillable = [
        'user_id',
        'gateway_id',
        'api_key',
        'karmozd_type',
        'karmozd',
        'title',
        'domain',
        'no_domain_lock',
        'status',
        'date',
        'total_revenue',
    ];

    public function gateway(){
        return $this->hasOne('App\Gateway', 'id', 'gateway_id');
    }
}
