<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function terminal()
    {
        return $this->hasOne('App\Terminal', 'id', 'terminal_id');
    }

}
