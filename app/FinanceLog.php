<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinanceLog extends Model
{
    protected $table = "finance_logs";
}
