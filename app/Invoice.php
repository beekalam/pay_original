<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = "invoices";
    protected $fillable = [
        'user_id',
        'type',
        'entity_id',
        'data_id',
        'terminal_id',
        'hash',
        'amount',
        'karmozd',
        'karmozd_type',
        'return_url',
        'card_number',
        'date',
        'ip',
        'status',
        'bank_code',
        'bank_error',
        'bank_ref',
        'card_number',
        'ip',
        'order_id',
        'order_desc',
        'gateway_id',
    ];
    public function terminal()
    {
        return $this->hasOne('App\Terminal', 'id', 'terminal_id');
    }
    public $timestamps = false;

}
