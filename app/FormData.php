<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormData extends Model
{
   protected $table="forms_data";
   protected $fillable = ['form_id','status','data'];

   public function form(){
       return $this->hasOne('App\Forms','id','form_id');
   }

   public function terminal(){
       return $this->hasOne('App\Terminal','id','terminal_id');
   }
}
