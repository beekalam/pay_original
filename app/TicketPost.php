<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketPost extends Model
{
    protected $fillable = [
        'ticket_id',
        'text',
        'by'
    ];

}
