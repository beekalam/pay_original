<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    private $_roles = null;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email', 
        'password',
        'father',
        'mobile',
        'state_id',
        'city_id',
        'city_title',
        'address',
        'zipcode',
        'sh_melli',
        'birthday',
        'phone',
        'jalali_month',
        'last_signin',
        'last_ip',
        'needs_verify',
        'verified',
        'email_verified',
        'email_verification_code',
        'mobile_activiation_code',
        'scan_hash',
        'total_revenue',
        'balance',
        'bank_name',
        'bank_shaba',
        'karmozd_type',
        'guestpay_description',
        'can_withdraw',
        'sh_scan_file',
        'melli_scan_file',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users');
    }

    public function isAdmin(){
        if(is_null($this->_roles))
            $this->_roles = $this->roles()->pluck('name')->toArray();

        return in_array('admin',$this->_roles);
    }
}
