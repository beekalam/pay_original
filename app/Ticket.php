<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable=[
        'department_id',
        'user_id',
        'title',
        'status',
        'last_post_by'
    ];

    public function posts(){
        return $this->hasMany('App\TicketPost');
    }
    
    
}
