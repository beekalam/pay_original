<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash',100);
            $table->unsignedInteger('user_id');
            $table->string('title',100);
            $table->bigInteger('amount');
            $table->string('callback_url_success',1024);
            $table->string('callback_url_fail',1024);
            $table->unsignedInteger('terminal_id');
            $table->text('fields');
            $table->tinyInteger('status')->default('1');
            $table->bigInteger('total_revenue')->default('0');
            
            $table->timestamps();
        });

        Schema::create('forms_data', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('form_id');
            $table->bigInteger('amount');
            $table->binary('data');
            $table->tinyInteger('status')->default('0')->comment('0:pending | 1:payed | 2:error');
            
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
        Schema::dropIfExists('forms_data');
    }
}
