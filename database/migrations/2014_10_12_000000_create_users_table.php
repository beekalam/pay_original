<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('father');
            $table->integer('state_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('state_title')->nullable();
            $table->string('city_title')->nullable();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('sh_melli')->nullable();
            $table->string('birthday')->nullable();
            $table->string('password');
            $table->string('mobile');
            $table->string('phone')->nullable();
            $table->tinyInteger('jalali_month')->nullable();
            $table->date('last_signin')->nullable();
            $table->string('last_ip')->nullable();
            $table->tinyInteger('needs_verify')->nullable();
            $table->tinyInteger('verified')->nullable();
            $table->tinyInteger('email_verified')->nullable();
            $table->tinyInteger('mobile_verified')->nullable();
            $table->string('email_activiation_code')->nullable();
            $table->string('mobile_activation_code')->nullable();
            $table->string('scan_hash')->nullable();
            $table->bigInteger('total_revenue')->nullable();
            $table->bigInteger('balance')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_shaba')->nullable();
            $table->tinyInteger('karmozd_type')->nullable();
            $table->string('guestpay_description')->nullable();
            $table->tinyInteger('can_withdraw')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
