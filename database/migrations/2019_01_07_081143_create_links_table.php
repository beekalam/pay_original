<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash', 255);
            $table->unsignedInteger('user_id');
            $table->string('title', 100);
            $table->integer('amount');
            $table->string('callback_url_success', 1024);
            $table->string('callback_url_fail', 1024);
            $table->unsignedInteger('terminal_id');
            $table->tinyInteger('status')->default('1');
            $table->bigInteger('total_revenue')->default('0');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
