<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('gateway_id');
            $table->string('api_key',32)->nullable();
            $table->tinyInteger('karmozd_type')->default('2');  //1: from payer | 2: from user
            $table->decimal('karmozd',4,2)->default(0.00);
            $table->string('title',100)->nullable();
            $table->string('domain',255)->nullable();
            $table->tinyInteger('no_domain_lock')->default('0');
            $table->tinyInteger('status')->default('1');
            $table->bigInteger('total_revenue')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminals');
    }
}
