<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->create_user('beekalam@gmail.com', 3,'beekalam@gmail.com');
        $this->create_user('test@test.com', 4,'test@test.com');

        $this->create_user('admintest@test.com',3,'admintest@test.com');
        $this->create_user('usertest@test.cm',4,'usertest@test.com');
    }

    private function create_user($email, $role_id,$name,$password='123456')
    {
        $user = DB::table('users')->where('email', $email)->first();
        if (!is_null($user)) {
            DB::table('notifications')->where('user_id', $user->id)->delete();
            DB::table('role_users')->where('user_id', $user->id)->delete();
            DB::table('users')->where('email', $email)->delete();
        }
        $id = DB::table('users')->insertGetId([
            'name' => $name,
            'email' => $email,
            'father' => '',
            'mobile' => '',
            'password' => bcrypt($password),
        ]);
        DB::table('role_users')->insert(['user_id' => $id, 'role_id' => $role_id]);
    }

}
