<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        DB::table('roles')->insert(['name' => 'admin', 'slug' => 'admin']);
        DB::table('roles')->insert(['name' => 'user', 'slug' => 'user']);
    }
}
