<?php

use Illuminate\Database\Seeder;
use App\Department;
class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::truncate();
        $d = [
            ["title" => 'بخش مالی'],
            ["title" => 'بخش فنی'],
            ["title" => 'پیشنهادات و انتقادات'],
            ["title" => 'مدیریت'],
        ];
        
        foreach ($d as $dep) {
            $tmp = new Department($dep);
            $tmp->save();
        }
    }
}
