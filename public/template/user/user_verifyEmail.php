<?php
if(!$_SESSION['user']['email_verified'] && !empty($_SESSION['user']['email'])) {
?>
<p>آدرس ایمیل شما : <?=$_SESSION['user']['email'];?></p>
<p>لینک تایید آدرس ایمیل برای شما ارسال شده است. لطفا به ایمیل خود مراجعه کرده و بر روی لینک مذکور کلیک کنید.<br>
در صورتی که ایمیل داخل اینباکس شما نبود، پوشه spam را نیز چک کنید.</p>

در صورتی که ایمیلی برای شما ارسال نشده است میتوانید برای ارسال مجدد <a href="<?=USERCP;?>/user/resendVerificationEmail">اینجا کلیک کنید</a>.<br>
<?php
} elseif (!empty($_SESSION['user']['email'])) {
?>
ایمیل شما تایید شده است.
<?php
}
?>