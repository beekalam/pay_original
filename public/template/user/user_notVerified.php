برای استفاده از خدمات سایت نیاز است که حساب خود را فعال نمایید.<br>
برای این منظور موارد زیر باید رفع شوند :<br>
<?php
if(!$_SESSION['user']['email_verified']) {?>
<p class="text-danger">
    ایمیل شما هنوز تایید نشده است. برای تایید ایمیل خود <a href="<?=USERCP;?>/user/verifyEmail">اینجا کلیک کنید</a>.
</p>
<?php 
}
if(!$_SESSION['user']['mobile_verified'] && get('sms_active')) {?>
<p class="text-danger">
    شماره موبایل شما هنوز تایید نشده است. برای تایید شماره موبایل خود <a href="<?=USERCP;?>/user/verifyMobile">اینجا کلیک کنید</a>.
</p>
<?php 
}
if(empty($_SESSION['user']['state_id']) || ($_SESSION['user']['needs_verify']==0 && $_SESSION['user']['verified'] == 0)) {?>
<p class="text-danger">
    اطلاعات پروفایل شما ناقص است، جهت تکمیل اطلاعات پروفایل <a href="<?=USERCP;?>/user/editProfile">اینجا کلیک کنید</a>.
</p>
<?php 
} elseif($_SESSION['user']['needs_verify'] == 1) {?>
<p class="text-danger">
    مدارک شما هنوز بررسی و تایید نشده اند، این پروسه ممکن است تا 24 ساعت زمان ببرد.
</p>
<?php 
}
?>