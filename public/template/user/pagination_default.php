<?php
if($pages_count > 1) {?>
<div class="row">
    <div class="col-xs-6">
	    <ul class="pagination">
<?php
	$class = (Routing::$page == 1) ? ' disabled' : '';
	echo '<li class="first'.$class.'"><a href="'.$path.'page:1">صفحه اول</a></li>';
	$range = 3;
	$start = (Routing::$page - $range > 0) ? Routing::$page - $range : 1;
	$end = (Routing::$page + $range < $pages_count) ? Routing::$page + $range : $pages_count;
	for($i = $start; $i <= $end; $i++)
	{
		$class = '';
		if($i == Routing::$page)
			$class = 'active ';
		if($i == $pages_count && $i == Routing::$page)
			$class .= 'last ';
		if($i == 1 && $i == Routing::$page)
			$class .= 'first ';
		
		if(!empty($class))
			$class = ' class="'.rtrim($class,' ').'"';
		echo '<li'.$class.'><a href="'.$path.'page:'.$i.'">'.$i.'</a></li>';
		
	}
	$class = (Routing::$page >= $pages_count) ? ' disabled' : '';
	echo '<li class="last'.$class.'"><a href="'.$path.'page:'.$pages_count.'">صفحه آخر</a></li>';
?>
        </ul>
    </div>
    <div class="col-xs-6">
        <span class="pagination-info">نمایش <?=$pagination_from;?> تا <?=$pagination_to;?> از مجموع <?=$total_records;?> مورد</span>
    </div>
</div>
<?php
}
?>