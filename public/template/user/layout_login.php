<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?php echo $BASE; ?>" />
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap-rtl.min.css" />
<link rel="stylesheet" type="text/css" href="template/admin/login.css" />
<title><?=$title_for_layout;?></title>
</head>

<body>
	<div class="form-signin<?php if(isset($error) && !empty($error)) {?> error<?php } ?>">
        <?php if(isset($error) && !empty($error)) {?><div class="alert alert-danger"><?=@$error ?></div><?php } ?>
        <div class="inner">
		<h2 class="form-signin-heading"><?=$title_for_layout;?></h2>
		<?=$content_for_layout;?>
        </div>
        <?php show_cr(true);?>
	</div>
</body>
</html>