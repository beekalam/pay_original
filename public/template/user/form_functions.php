<?php
function make_obj(&$item, $id, $vars = array())
{
    $class = array();
    $field_defaults = ' name="'.$id.'" id="field_'.$id.'"'.@$item['extra'];
    $field_defaults2 = '';
    $validate = jquery_validate($item);
    if(!empty($validate) && $item['field'] != 'file')
        $class[] = 'validate['. implode(',',$validate).']';
    
    if(isset($item['dir']))
        $field_defaults2 .= ' dir="'.$item['dir'].'"';
    if(isset($item['align']))
        $field_defaults2 .= ' align="'.$item['align'].'"';
    if(isset($item['class']))
        $class[] = $item['class'];
    if(Routing::$section == '' && isset($item['title'])){
        $item['placeholder'] = $item['title'];
    }
    
    $field_defaults .= $field_defaults2;
    
    switch($item['field']) {
        case 'textbox':
        $type = (isset($item['field_type'])) ? $item['field_type'] : 'text';
        $maxLength = (isset($item['length'])) ? $item['length'] : ((@$item['type']=='text' && isset($item['max'])) ? $item['max'] : false);
        $addOnClass = $preAddOn = $postAddOn = '';
        if(isset($item['pre-add-on'])) {
            $addOnClass = 'input-group';
            $preAddOn = '<span class="input-group-addon">'.$item['pre-add-on'].'</span>';
        }
        if(isset($item['post-add-on'])) {
            $addOnClass .= ' input-group';
            $postAddOn = '<span class="input-group-addon">'.$item['post-add-on'].'</span>';
        }
        if(!empty($addOnClass))
            echo '<div class="'.$addOnClass.'">'.$preAddOn;
        $placeholder = (isset($item['placeholder']))?' placeholder="'.$item['placeholder'].'"':'';
        $class[] = 'form-control';
        ?>
        <input type="<?=$type?>"<?=$field_defaults.$placeholder;?><?php if($maxLength){?> maxlength="<?=$maxLength?>"<?php } if(isset($item['field_size'])){?> size="<?=$item['field_size']?>"<?php }?> value="<?=@$vars[$id]?>"<?=make_class($class);?> />
        <?php 
        if(!empty($addOnClass))
            echo $postAddOn.'</div>';
        break;
        
        case 'textarea':
        $placeholder = (isset($item['placeholder']))?' placeholder="'.$item['placeholder'].'"':'';
        $class[] = 'form-control';
        ?>
        <textarea <?=$field_defaults.$placeholder?><?php if(isset($item['cols'])){?> cols="<?=$item['cols']?>"<?php } if(isset($item['rows'])){?> rows="<?=$item['rows']?>"<?php } ?><?=make_class($class);?>><?=@$vars[$id]?></textarea>
        <?php break;
        
        case 'tag':
        new_template_file('site','js/tags/tags','css');
        new_template_file('site','js/tags/tags','js');
        $item['extra_ending'] = '<div class="tag_show" id="tag_show_'.$id.'"></div>';
        if(!isset($item['description']))
            $item['description'] = 'با کاما (,) جدا کنید و یا بعد از وارد کردن هر عبارت Enter کنید';
        ?>
        <input type="text" name="_tags_<?=$id;?>" class="form-control _tags" id="_tags_<?=$id;?>" value="<?=@$vars[$id]?>"<?=$field_defaults2;?> style="width:350px;" />
        <input type="hidden" class="hidden_tags" name="<?=$id?>" id="tags_<?=$id;?>" />
        <?php break;
        
        case 'editor':
        $class[] = 'ckeditor';
        ?>
        <textarea <?=$field_defaults?><?=make_class($class);?>><?=@$vars[$id]?></textarea>
        <?php break;
        
        case 'select':
            if($item['options'] == 'fromdb') {
                $item['options'] = DB::get_rows_k2v($item['field_key'],$item['field_val'],$item['table'],@$item['condition']);
                $item['options'][''] = 'انتخاب کنید';
                ksort($item['options']);
            } elseif($item['options'] == 'fromFunction') {
                $func = $item['function'];
                $item['options'] = $func();
                $item['options'][''] = 'انتخاب کنید';
                ksort($item['options']);
            } elseif($item['options'] == 'lists') {
                $item['options'] = $GLOBALS['lists'][$item['list_key']];
            }
            $class[] = 'form-control';
        ?>
        <select<?=$field_defaults;?> style="max-width:350px;"<?=make_class($class);?>>
        <?php
            foreach($item['options'] as $k => $v){
        ?>
            <option value="<?=$k;?>"<?php if(@$vars[$id] == $k){?> selected="selected"<?php } ?>><?=$v;?></option>
        <?php } ?>
        </select>
        <?php break;
        
        case 'multiple_select':
            if(@$item['list'] == 'lists')
            {
                $item['options'] = $GLOBALS['lists'][$item['option_key']];
            }
            $class[] = 'multiselect';
        ?>
        <select name="<?=$id;?>[]" id="field_<?=$id;?>_"<?=make_class($class);?> multiple="multiple" style="width:440px; height: 200px" data-placeholder="<?=$item['title'];?>">
        <?php
            foreach($item['options'] as $k => $v){
        ?>
            <option value="<?=$k;?>"<?php if(@in_array($k,@$vars[$id])){?> selected="selected"<?php } ?>><?=$v;?></option>
        <?php } ?>
        </select>
        <div class="cb"></div>
        <?php break;
        
        case 'states':
        ?>
        <select<?=$field_defaults;?> onchange="loadCities(this.selectedIndex)" class="form-control"></select>
        <div class="cb"></div>
        <?php break;
        
        case 'cities':
        ?>
        <select<?=$field_defaults;?> class="form-control"></select>
        <div class="cb"></div>
        <?php break;
        case 'checkbox':
        if(Routing::$section == '')
            echo '<label>';
        ?>
        <input type="checkbox"<?=$field_defaults;?> value="1"<?php if(isset($vars[$id]) && $vars[$id]==1){?> checked="checked"<?php } ?><?=make_class($class);?>/>
        <?php
        if(Routing::$section == '')
            echo $item['title'].'</label>';
        
        break;
        case 'boolean':
        ?>
        <ul class="formee-list">
            <li><input type="radio" name="<?=$id;?>" id="radio_<?=$id;?>_yes" value="1"<?php if(isset($vars[$id]) && $vars[$id]==true){?> checked="checked"<?php } ?>/><label for="radio_<?=$id;?>_yes">بلی</label></li>
            <li><input type="radio" name="<?=$id;?>" id="radio_<?=$id;?>_no" value="0"<?php if(isset($vars[$id]) && $vars[$id]==false){?> checked="checked"<?php } ?>/><label for="radio_<?=$id;?>_no">خیر</label></li>
        </ul>
        <?php break;
        case 'option':
        ?>
        
        <?php break;
        case 'file':
        ?>
        <input type="file"<?=$field_defaults;?><?=make_class($class);?>  style="display:none" />
        <div class="input-group">
            <input id="alt_field_<?=$id;?>" class="form-control" type="text" onclick="$('input[id=field_<?=$id;?>]').click();">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" onclick="$('input[id=field_<?=$id;?>]').click();"><?=(isset($item['btn_title'])?$item['btn_title']:'انتخاب تصویر');?></button>
            </span>
        </div>
         
        <script type="text/javascript">
        $('input[id=field_<?=$id;?>]').change(function() {
           $('#alt_field_<?=$id;?>').val($(this).val());
        });
        </script>
        <?php break;
        case 'birthday':
        $class[] = 'form-control';
        if(isset($vars[$id]) && !empty($vars[$id])) {
            list($year,$month,$day) = explode('-',@$vars[$id]);
        } else {
            $year = 1394;
            $month = 1;
            $day = 1;
        }
        ?>
        <div style="width: 30%; float: right;">
            <select id="f_year" style="width: 100%; padding: 0 6px; border-left: 0px; border-radius: 0 3px 3px 0;" class="form-control">
<?php for($i=1394; $i>=1300; $i--) {?>
                <option value="<?=$i;?>"<?=selected($year,$i);?>><?=$i;?></option>
<?php }?>
            </select>
        </div>
        <div style="width: 45%;  float: right;">
            <select id="f_month" style="width: 100%; padding: 0 6px; border-left: 0px; border-radius: 0" class="form-control">
                <option value="1"<?=selected($month,1);?>>فروردین</option>
                <option value="2"<?=selected($month,2);?>>اردیبهشت</option>
                <option value="3"<?=selected($month,3);?>>خرداد</option>
                <option value="4"<?=selected($month,4);?>>تیر</option>
                <option value="5"<?=selected($month,5);?>>مرداد</option>
                <option value="6"<?=selected($month,6);?>>شهریور</option>
                <option value="7"<?=selected($month,7);?>>مهر</option>
                <option value="8"<?=selected($month,8);?>>آبان</option>
                <option value="9"<?=selected($month,9);?>>آذر</option>
                <option value="10"<?=selected($month,10);?>>دی</option>
                <option value="11"<?=selected($month,11);?>>بهمن</option>
                <option value="12"<?=selected($month,12);?>>اسفند</option>
            </select>
        </div>
        <div style="width: 25%;  float: right;">
            <select id="f_day" style="width: 100%; padding: 0 6px; border-radius: 3px 0 0 3px" class="form-control">
<?php for($i=1; $i<=31; $i++) {?>
                <option value="<?=$i;?>"<?=selected($day,$i);?>><?=$i;?></option>
<?php }?>
            </select>
            <input type="hidden"<?=$field_defaults;?>>
        </div>
<script type="text/javascript">
$(document).ready(function(){
    $('#f_year,#f_month,#f_day').change(function(){
        $('#field_<?=$id;?>').val($('#f_year').val()+'-'+$('#f_month').val()+'-'+$('#f_day').val());
    });
    $('#f_year').change();
});
</script>
               
        <?php break;
        case 'captcha':
        $class[] = 'form-control';
        ?>
            <div class="row">
                <div class="col-sm-6">
                    <input<?=$field_defaults;?><?=make_class($class);?> type="text" maxlength="5" />
                </div>
                <div class="col-sm-4">
                    <img src="captcha" width="90" height="30" id="field_img_<?=$id;?>" />
                </div>
                <div class="col-sm-2">
                    <a style="position: absolute; right: -80px; top: 9px" tabindex="1000" href="#" title="بارگزاری تصویر جدید" onclick="$('#field_img_<?=$id;?>').attr('src','captcha/?'+Math.floor((Math.random()*100000)+1)); return false;">
                        <span class="glyphicon glyphicon-refresh"></span>
                    </a>
                </div>
            </div>
               
        <?php break;
        case 'custom':
            echo $item['data'];
            if(isset($item['eval']))
                eval($item['eval']);
        ?>
        
        <?php break;
        case 'buttons':
            foreach($item as $button) {
                if(is_array($button)) {
                    if(substr(@$button['title'],0,2)=='__')
                        $button['title'] = __(substr($button['title'],2));
                    $field_defaults = '';
                    if(Routing::$section == '')
                        $field_defaults .= ' class="btn btn-success btn-block '.@$button['class'].'"';
                    else
                        $field_defaults .= ' class="btn btn-success '.@$button['class'].'"';
                    if(isset($button['extra']))
                        $field_defaults .= ' '.@$button['extra'];
        ?>
        <button type="<?=$button['field_type']?>" <?=$field_defaults?>>
            <span><?=$button['title']?></span>
        </button> &nbsp;&nbsp;
    <?php
                }
            }
    }        
}

function make_class($arr)
{
    $res = implode(' ',$arr);
    return ' class="'.$res.'"';
}

function jquery_validate($item)
{
    # jquery validate
    $validate = array();
    if(@$item['required'] === true || @$item['required'] === '1' || @$item['required'] === 1)
        $validate[] = 'required';
    if(@$item['type'] == 'int') {
        $validate[] = 'custom[integer]';
        if(isset($item['min']))
            $validate[] = 'min['.$item['min'].']';
        if(isset($item['max']))
            $validate[] = 'max['.$item['max'].']';
    } elseif(@$item['type'] == 'text') {
        if(isset($item['min']))
            $validate[] = 'minSize['.$item['min'].']';
        if(isset($item['max']))
            $validate[] = 'maxSize['.$item['max'].']';
    } elseif(@$item['type'] == 'email')
        $validate[] = 'custom[email]';
    return $validate;
}

function selected($x, $y) {
    return ($x == $y) ? ' selected="selected"' : '';
}
?>