<?php
$no_layout = true;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box mgb">
            <div class="title">تراکنش های 10 روز گذشته</div>
            <div class="in">
                <div id="graph_revenues" style="height:200px;width:100%;"></div>
            
            </div>
        </div>
        <div class="box table">
            <div class="title">مجموع تراکنش ها</div>
            <table class="table table-bordered">
            <tr>
                <th>امروز</th>
                <th>دیروز</th>
                <th>این هفته</th>
                <th>این ماه</th>
                <th>کل</th>
            </tr>
            <tr>
                <td><?=number_format($revenues['today']/10);?> تومان</td>
                <td><?=number_format($revenues['yesterday']/10);?> تومان</td>
                <td><?=number_format($revenues['week']/10);?> تومان</td>
                <td><?=number_format($revenues['month']/10);?> تومان</td>
                <td><?=number_format($revenues['total']/10);?> تومان</td>
            </tr>
        </table>
        </div>
    </div>
</div>
<style type="text/css">
.dxc-tooltip{
    direction: ltr;
    text-align: left;
}
</style>
<script type="text/javascript">
var revenues = [<?=$revenues_data;?>];
function separator(input) {
    var input = (parseInt(input)).toString();
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(input)) {
        input = input.replace(rgx, '$1' + ',' + '$2');
    }
    return input;
}
$(document).ready(function(){
    $("#graph_revenues").dxChart({
        dataSource: revenues,
        commonSeriesSettings: {
            argumentField: "date2",
            tagField: "info"
        },
        series: [
            { valueField: "count", name: "Count", color: "#27c24c" },
        ],
        tooltip:{
            enabled: true,
            customizeText: function () {
                return separator(this.point.tag.amount/10) + 'T (' + this.valueText + ')';
            },
            font: { size: 14 }
        },
        legend: {
            visible: false
        },
        valueAxis:{
            grid:{
                color: '#9D9EA5',
                width: 0.5
            }
        },
        margin: {
            bottom: 20,
            left: 20,
        },
        rtlEnabled: true,
        valueAxis: {
            label: { alignment: 'center' }
        }
    });
});
</script>