<?php
$no_layout = true;
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <p>آدرس صفحه پرداخت شخصی شما :</p>
                <div class="row">
                    <div class="col-xs-12 col-md-10 col-lg-8">
                        <div class="input-group">
                            <input type="text" readonly="readonly" dir="ltr" onfocus="this.select()" onclick="this.select()" align="right" class="form-control" value="<?=SITE_URL.$_SESSION['user']['username'];?>/">
                            <span class="input-group-btn">
                                <a class="btn btn-info" href="<?=SITE_URL.$_SESSION['user']['username'];?>/" target="_blank">نمایش</a>
                            </span>
                        </div>
                    </div>
                </div>
                <p>&nbsp;</p>
                <p>میتوانید آدرس این صفحه را در اختیار دیگران قرار دهید تا از این  طریق مبالغ مورد نظر را به حساب شما پرداخت کنند.</p>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="box">
            <div class="title">تنظیمات صفحه پرداخت شخصی</div>
            <div class="in">
                <?=$form;?>
            </div>
        </div>
    </div>
</div>