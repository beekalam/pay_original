<?php
$no_layout = true;
?>
<style type="text/css">
.box.info{
    border: 4px solid #46b8da;
}
.hiddenHelper{
    color: #46b8da;
    font-weight: bold;
    display: inline-block;
    font-size: 20px;
    line-height: 20px;
    border-radius: 13px;
    border: 2px solid #46b8da;
    width: 25px;
    height: 25px;
    opacity:0;
    text-align: center;
}
</style>
<script type="text/javascript">
function do_filter(){
    var path = '<?=USERCP;?>/invoice/index/';
    path += $('#invoice_status').val()+'/';
    path += $('#invoice_type').val()+'/';
    path += $('#invoice_ipp').val()+'/';
    path += ($('#hidden_exact_date').val() !== '') ? $('#hidden_exact_date').val().replace(/\//g, '-')+'/' : 'null/';
    path += ($('#exact_hour').val() !== '') ? $('#exact_hour').val()+'/' : 'null/';
    path += ($('#bank_code').val() !== '') ? $('#bank_code').val()+'/' : 'null/';
    path += ($('#amount').val() !== '') ? $('#amount').val()+'/' : 'null/';
    path += ($('#terminal_id').val() !== '') ? $('#terminal_id').val()+'/' : 'null/';
    path += ($('#invoice_id').val() !== '') ? $('#invoice_id').val()+'/' : 'null/';
    path += ($('#order_id').val() !== '') ? $('#order_id').val()+'/' : 'null/';
    window.location = path;
}
function do_export(){
    var path = '<?=USERCP;?>/invoice/export/';
    path += $('#invoice_status').val()+'/';
    path += $('#invoice_type').val()+'/';
    path += ($('#hidden_exact_date').val() !== '') ? $('#hidden_exact_date').val().replace(/\//g, '-')+'/' : 'null/';
    path += ($('#exact_hour').val() !== '') ? $('#exact_hour').val()+'/' : 'null/';
    path += ($('#bank_code').val() !== '') ? $('#bank_code').val()+'/' : 'null/';
    path += ($('#amount').val() !== '') ? $('#amount').val()+'/' : 'null/';
    path += ($('#terminal_id').val() !== '') ? $('#terminal_id').val()+'/' : 'null/';
    path += ($('#invoice_id').val() !== '') ? $('#invoice_id').val()+'/' : 'null/';
    path += ($('#order_id').val() !== '') ? $('#order_id').val()+'/' : 'null/';
    window.location = path;
}
$(document).ready(function(){
    // date picker
    $('#exact_date').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        dateFormat: 'DD، d MM yy',
        altField: '#hidden_exact_date',
        altFormat: 'yy/mm/dd',
        /*maxDate: "0",*/
    });
    <?php if(isset($default_exact_date)){?>
    $('#exact_date').datepicker('setDate',new JalaliDate(<?=$default_exact_date;?>));
    <?php } ?>
});

var invSerachHelpActive = false;
function invSerachHelp()
{
    if(!invSerachHelpActive) {
        $('#invSearchHelpBox').slideDown('slow');
        $('.hiddenHelper').css({opacity:1});
        invSerachHelpActive = true;
    }
}
</script>
<div class="box mgb">
            <form onsubmit="do_filter(); return;">
            <div class="in">
                <div class="row mgb">
                    <div class="col-xs-3<?php if(isset($exact_date_error)){?> has-error<?php }?>">
                        <label for="exact_date">تاریخ <span class="hiddenHelper">1</span></label>
                        <input type="text" id="exact_date" readonly="readonly" class="form-control" />
                        <input type="hidden" name="exact_date" id="hidden_exact_date" />
                    </div>
                    <div class="col-xs-3<?php if(isset($exact_date_error)){?> has-error<?php }?>">
                        <label for="exact_hour">ساعت <span class="hiddenHelper">1</span></label>
                        <input type="text" id="exact_hour" class="form-control" value="<?=@$exact_hour;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="bank_code">شماره پیگیری <span class="hiddenHelper">2</span></label>
                        <input type="text" id="bank_code" class="form-control" value="<?=@$bank_code;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="amount">مبلغ تراکنش (ریال)</label>
                        <input type="text" id="amount" class="form-control" value="<?=@$amount;?>" />
                    </div>
                </div>
                <div class="row mgb">
                    <div class="col-xs-3">
                        <label for="exact_hour">ترمینال</label>
                        <select id="terminal_id" class="form-control">
                            <option value="null">همه موارد</option><?php foreach($terminals as $terminal) {?>
                            <option value="<?=$terminal['id'];?>"<?php if(@$terminal_id == $terminal['id']){?> selected="selected"<?php }?>><?=$terminal['title'];?></option><?php }?>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label for="invoice_status">وضعیت تراکنش</label>
                        <select id="invoice_status" class="form-control">
                            <option value="null">همه موارد</option>
                            <option value="0"<?php if(@$status == '0'){?> selected="selected"<?php }?>>معلق</option>
                            <option value="1"<?php if(@$status == '1'){?> selected="selected"<?php }?>>پرداخت موفق</option>
                            <option value="2"<?php if(@$status == '2'){?> selected="selected"<?php }?>>پرداخت ناموفق</option>
                            <option value="3"<?php if(@$status == '3'){?> selected="selected"<?php }?>>خطای اتصال</option>
                            <option value="4"<?php if(@$status == '4'){?> selected="selected"<?php }?>>تراکنش غیر مجاز</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label for="invoice_type">نوع تراکنش</label>
                        <select id="invoice_type" class="form-control">
                            <option value="null">همه موارد</option>
                            <option value="1"<?php if(@$type == '1'){?> selected="selected"<?php }?>>از طریق API</option>
                            <option value="2"<?php if(@$type == '2'){?> selected="selected"<?php }?>>از طریق لینک پرداخت</option>
                            <option value="3"<?php if(@$type == '3'){?> selected="selected"<?php }?>>از طریق فرم پرداخت</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label for="invoice_ipp">تعداد در هر صفحه</label>
                        <select id="invoice_ipp" class="form-control">
                            <option value="10">10</option>
                            <option value="20"<?php if(@$ipp == '20'){?> selected="selected"<?php }?>>20</option>
                            <option value="50"<?php if(@$ipp == '50'){?> selected="selected"<?php }?>>50</option>
                            <option value="100"<?php if(@$ipp == '100'){?> selected="selected"<?php }?>>100</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        <label for="invoice_id">شناسه تراکنش</label>
                        <input type="text" id="invoice_id" class="form-control" value="<?=@$invoice_id;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="order_id">شناسه سفارش <span class="hiddenHelper">3</span></label>
                        <input type="text" id="order_id" class="form-control" value="<?=@$order_id;?>" />
                    </div>
                    <div class="col-xs-6">
                        <div>&nbsp;</div>
                        <button class="btn btn-success" type="button" onclick="do_filter()">
                            <span>اعمال فیلتر</span>
                        </button> &nbsp; 
                        <button class="btn btn-primary" type="button" onclick="do_export()">
                            <span>خروجی اکسل</span>
                        </button> &nbsp; 
                        <button class="btn btn-info pull-left" type="button" onclick="invSerachHelp()">
                            <span>راهنما</span>
                        </button>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="box mgb info" style="display: none;" id="invSearchHelpBox">
            <div class="in">
                <p><b>راهنمای جستجو و فیلتر کردن تراکنش ها :</b></p>
                <p>1- در صورتی که میخواهید تراکنش های یک زمان خاص را مشاهده کنید، میبایست تاریخ و ساعت انجام تراکنش را انتخاب کنید. در صورتی که فقط یکی از این دو فیلد را انتخاب کنید، تاریخ در فیلتر اعمال نخواهد شد.</p>
                <p>2- از گزینه شماره پیگیری میتوانید برای جستجوی کد رفرنس بانک، کد پیگیری بانک و یا کد خطای برگشتی از بانک استفاده کنید.</p>
                <p>2- شناسه سفارش همان شناسه ای میباشد که از طریق سایت شما (در صورت استفاده از API) به وبسرویس ارسال میشود و مربتط با سایت شماست.</p>
            </div>
        </div>
        <div class="box table">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <?=$table;?>
            </div>
        </div>