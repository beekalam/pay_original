<?php
$no_layout = true;
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اطلاعات تراکنش</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>شناسه</dt>
                    <dd><?=$invoice['id'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>ترمینال</dt>
                    <dd><?=$invoice['terminal_title'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>نوع</dt>
                    <dd><?=$GLOBALS['lists']['invoice_types'][$invoice['type']];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>مبلغ</dt>
                    <dd><?=number_format($invoice['amount']);?> ریال</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>وضعیت</dt>
                    <dd><?=$GLOBALS['lists']['invoice_statuses'][$invoice['status']];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تاریخ ایجاد</dt>
                    <dd><?=Date::formatDate($invoice['date'],'j M Y ساعت H:i');?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>IP</dt>
                    <dd><?=$invoice['ip'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>شناسه سفارش</dt>
                    <dd><?=$invoice['order_id'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>توضیحات سفارش</dt>
                    <dd><?=$invoice['order_desc'];?></dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="box mgb">
            <div class="title">اطلاعات پرداخت</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>درگاه</dt>
                    <dd><?=$invoice['gateway_title'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>رفرنس بانک</dt>
                    <dd><?=$invoice['bank_ref'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>شناسه پیگیری پرداخت</dt>
                    <dd><?=$invoice['bank_code'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>متن خطا</dt>
                    <dd><?=$invoice['bank_error'];?></dd>
                </dl><?php if(!empty($invoice['card_number'])) {?>
                <dl class="dl-horizontal">
                    <dt>شماره کارت</dt>
                    <dd dir="ltr" class="text-right"><?=$invoice['card_number'];?></dd>
                </dl><?php }?>
            </div>
        </div><?php if($invoice['type'] == 4) {?> 
        <div class="box">
            <div class="title">اطلاعات فرم</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>نام پرداخت کننده</dt>
                    <dd><?=$guest_payment['name'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>موبایل</dt>
                    <dd><?=$guest_payment['mobile'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>ایمیل</dt>
                    <dd><?=$guest_payment['email'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>توضیحات</dt>
                    <dd><?=$guest_payment['description'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>نحوه کسر کارمزد</dt>
                    <dd><?=$GLOBALS['lists']['karmozd_types'][$guest_payment['karmozd_type']];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>مبلغ اصلی</dt>
                    <dd><?=number_format(($guest_payment['karmozd_type']==1)?($guest_payment['amount_total']-$guest_payment['karmozd']):$guest_payment['amount_total']);?> ریال</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کارمزد</dt>
                    <dd><?=number_format($guest_payment['karmozd']);?> ریال</dd>
                </dl>
            </div>
        </div><?php } ?>
    </div>
</div>