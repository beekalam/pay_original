<!DOCTYPE html>
<html>
<head>
<base href="<?=SITE_URL; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="developer" content="XDev.ir">
<script type="text/javascript" src="template/cp/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="template/cp/js/ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="template/cp/plugins/datepicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="template/cp/plugins/datepicker/bootstrap-datepicker.fa.min.js"></script>
<script type="text/javascript" src="template/cp/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="template/core/core.js"></script>
<script type="text/javascript" src="template/core/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="template/core/ckeditor/config.js"></script>
<script type="text/javascript" src="template/cp/plugins/globalize/globalize.min.js"></script>
<script type="text/javascript" src="template/cp/plugins/DevExpressChartJS/dx.chartjs.js"></script>
<script type="text/javascript" src="template/cp/plugins/multiselect/js/ui.multiselect.js"></script>
<script type="text/javascript" src="template/cp/js/script.js"></script>
<?php template_files(); ?>
<link rel="stylesheet" type="text/css" href="template/cp/plugins/jqueryui/all/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="template/cp/plugins/datepicker/bootstrap-datepicker.min.css" />
<link rel="stylesheet" type="text/css" href="template/cp/plugins/multiselect/css/ui.multiselect.css" />
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap-rtl.min.css" />
<link rel="stylesheet" type="text/css" href="template/cp/css/style.css" />
<title><?=$title_for_layout;?></title>
</head>
<body class="preload min-space">
<div class="loading"><span>در حال بارگذاری ...</span></div>
<div class="sidebar-container">
    <div class="head">

        <img src="template/cp/images/logo.png" alt="" />
    </div>
    <div id="navC">
        <ul class="nav">
        <li>
            <a href="<?=USERCP;?>/">
                <i class="glyphicon glyphicon-dashboard">
                    <b></b>
                </i>
                <span>داشبورد</span>
            </a>
        </li>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-headphones">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>پشتیبانی</span>
            </a>
            <ul>
                <li>
                    <a href="<?=USERCP;?>/ticket/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست تیکت ها
                    </a>
                </li>
                <li>
                    <a href="<?=USERCP;?>/ticket/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ایجاد تیکت جدید
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-sort">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیریت ترمینال ها</span>
            </a>
            <ul>
                <li>
                    <a href="<?=USERCP;?>/terminal/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست ترمینال ها
                    </a>
                </li>
                <li>
                    <a href="<?=USERCP;?>/terminal/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        افزودن ترمینال جدید
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-link">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیریت لینک ها</span>
            </a>
            <ul>
                <li>
                    <a href="<?=USERCP;?>/link/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست لینک های پرداخت
                    </a>
                </li>
                <li>
                    <a href="<?=USERCP;?>/link/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ایجاد لینک پرداخت جدید
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-check">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیریت فرم ها</span>
            </a>
            <ul>
                <li>
                    <a href="<?=USERCP;?>/form/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست فرم های پرداخت
                    </a>
                </li>
                <li>
                    <a href="<?=USERCP;?>/form/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ایجاد فرم پرداخت جدید
                    </a>
                </li>
                <li>
                    <a href="<?=USERCP;?>/form/dataIndex">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فرم های ثبت شده
                    </a>
                </li>
            </ul>
        </li><?php if(get('withdraw_active')) { ?>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-usd">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>تسویه حساب</span>
            </a>
            <ul>
                <li>
                    <a href="<?=USERCP;?>/withdraw/request">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        درخواست تسویه
                    </a>
                </li>
                <li>
                    <a href="<?=USERCP;?>/withdraw/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست تسویه حساب ها
                    </a>
                </li>
                <li>
                    <a href="<?=USERCP;?>/account/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                       حساب های بانکی
                    </a>
                </li>
            </ul>
        </li><?php }?>
        <li>
            <a href="<?=USERCP;?>/invoice/index">
                <i class="glyphicon glyphicon-list">
                    <b></b>
                </i>
                <span>فهرست تراکنش ها</span>
            </a>
        </li>
        <li>
            <a href="<?=USERCP;?>/guestpay">
                <i class="glyphicon glyphicon-user">
                    <b></b>
                </i>
                <span>صفحه پرداخت شخصی</span>
            </a>
        </li>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-cog">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>تنظیمات</span>
            </a>
            <ul>
                <li>
                    <a href="<?=USERCP;?>/user/editProfile">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ویرایش اطلاعات پروفایل
                    </a>
                </li>
                <li>
                    <a href="<?=USERCP;?>/user/changePass">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        تغییر کلمه عبور
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="<?=USERCP;?>/notification/index">
                <i class="glyphicon glyphicon-bullhorn">
                    <b></b>
                </i>
                <span>آرشیو اطلاعیه ها</span>
            </a>
        </li>
    </ul>
    </div>
    <div class="foot">
        پنل کاربری
    </div>
</div>
<div class="content-container">
    <div class="head">
        <div class="balance pull-right">
            <span>موجودی : <?=number_format($_SESSION['user']['balance']/10);?> تومان</span>
        </div>
        <div class="profile">
            <span><?=$_SESSION['user']['name'];?></span>
        </div>
        <div class="signout">
            <a href="<?=USERCP;?>/user/signout">
                <i class="glyphicon glyphicon-remove-sign"></i>
                <span>&nbsp; خروج</span>
            </a>
        </div>
        <div class="navBtn">
            <a href="#">
                <i class="glyphicon glyphicon-menu-hamburger"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="content">
        <?php
        if(!$_SESSION['user']['email_verified']) {?>
        <div class="alert alert-warning" role="alert">
            ایمیل شما هنوز تایید نشده است. برای تایید ایمیل خود <a href="<?=USERCP;?>/user/verifyEmail">اینجا کلیک کنید</a>.
        </div>
        <?php 
        }
        if(get('sms_active') && !$_SESSION['user']['mobile_verified']) {?>
        <div class="alert alert-warning" role="alert">
            شماره موبایل شما هنوز تایید نشده است. برای تایید شماره موبایل خود <a href="<?=USERCP;?>/user/verifyMobile">اینجا کلیک کنید</a>.
        </div>
        <?php 
        }
        if(empty($_SESSION['user']['state_id']) || (!$_SESSION['user']['verified'] && !$_SESSION['user']['needs_verify'])) {?>
        <div class="alert alert-warning" role="alert">
            اطلاعات پروفایل شما ناقص است، جهت تکمیل اطلاعات پروفایل <a href="<?=USERCP;?>/user/editProfile">اینجا کلیک کنید</a>.
        </div>
        <?php 
        }
        if(empty($_SESSION['user']['password2'])) {?>
        <div class="alert alert-warning" role="alert">
            جهت ثبت درخواست تسویه حساب و ویرایش اطلاعات اکانت لازم است که رمز دومی برای اکانت تان تعیین کنید، با اینکار امنیت اکانت شما افزایش می یابد.<br>
            برای این منظور <a href="<?=USERCP;?>/user/pass2">اینجا کلیک کنید</a>.
        </div>
        <?php 
        }
        if(!empty($unread_notifs)) {
            $unread_notifs_count = count($unread_notifs); ?>
        <div class="alert alert-info" role="alert">
            شما <?=$unread_notifs_count;?> اطلاعیه خوانده نشده دارید، برای خواندن آن<?php if($unread_notifs_count>1){?>ها<?php }?> <a href="<?=USERCP;?>/notification/index">اینجا کلیک کنید</a>.
        </div>
        <?php 
        }
        if(isset($system_message)){ ?>
        <div class="alert alert-<?=(isset($system_message_type)?$system_message_type:'success');?>" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>پیغام سیستم : </strong>
            <?=$system_message;?>
        </div>
        <?php } ?><?php if(isset($error)) { ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>خطا : </strong>
            <?=$error;?>
        </div><?php } ?>
        <?php if(isset($no_layout)) {
            echo $content_for_layout;
        } else { ?>
        <div class="box">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <?=$content_for_layout;?>
            </div>
        </div>
        <?php } ?>
        <div id="inline_load" style="display: none;"></div>
        <?php if(isset($page_description)) { ?>
        <div class="box mgt">
            <div class="body">
                <p class="text-info">
                    <?=$page_description;?>
                </p>
            </div>
        </div><?php } ?>
        <div class="box footer mgt">
            <div class="in">
                <div class="txt-r">
                    <?php show_cr(false);?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="DeleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">پیغام سیستم</h4>
            </div>
            <div class="modal-body">
                <p>آیا از حذف این رکورد اطمینان دارید؟</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal"> انصراف </button>
                <button type="button" class="btn btn-warning" onclick="ModalOkButton();"> &nbsp; بلی &nbsp; </button>
            </div>
        </div>
    </div>
</div>
</body>
</html>