<form method="post" accept="<?=USERCP;?>/user/signin/<?=$return;?>">
    <div class="form-group">
        <div class="input-group">
	        <input type="text" name="mobile" value="<?=@$_POST['mobile'];?>" dir="ltr" class="form-control" placeholder="mobile" autofocus>
            <div class="input-group-addon">
                <i class="glyphicon glyphicon-user"></i>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <input type="password" name="password" value="<?=@$_POST['password'];?>" dir="ltr" class="form-control" placeholder="Password">
            <div class="input-group-addon">
                <i class="glyphicon glyphicon-lock"></i>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group" id="captchaWrapper">
	        <input type="text" name="captcha" dir="ltr" class="form-control" placeholder="Captcha" autocomplete="off">
            <span class="input-group-btn">
                <button onclick="refreshCaptcha()" class="btn btn-info" type="button">
                    <i class="glyphicon glyphicon-refresh"></i>
                </button>
            </span>
            <img src="captcha" alt="captcha" id="captcha" />
        </div>
    </div>
    <div class="form-group">
	    <button class="btn btn-large btn-danger btn-block" type="submit">ورود</button>
    </div>
	<p><a href="user/forgotpass" class="forgot">کلمه عبور خود را فراموش کرده اید؟</a></p>
</form>
<script type="text/javascript">
function refreshCaptcha() {
    document.getElementById('captcha').src = 'captcha/?'+Math.floor(Math.random()*1000000)
    return false;
}
</script>