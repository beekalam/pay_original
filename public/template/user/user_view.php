<?php
$no_layout = true;
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اطلاعات کاربر</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>نام</dt>
                    <dd><?=$user['name'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>ایمیل</dt>
                    <dd><?=$user['email'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>موبایل</dt>
                    <dd><?=$user['mobile'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تاریخ ایجاد اکانت</dt>
                    <dd><?=Date::formatDate($user['date'],'j M Y ساعت H:i');?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>آخرین IP</dt>
                    <dd><?=$user['last_ip'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تاریخ آخرین ورود</dt>
                    <dd><?=(empty($user['last_signin']))?'-':Date::formatDate($user['last_signin'],'j M Y ساعت H:i');?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>نام بانک</dt>
                    <dd><?=$user['bank_name'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>شبای حساب</dt>
                    <dd><?=$user['bank_shaba'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>شماره کارت</dt>
                    <dd><?=$user['bank_card'];?></dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="box">
            <div class="title">آمار کاربر</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>ترمینال ها</dt>
                    <dd><?=number_format($terminalsCount);?> (<a href="<?=ADMINCP;?>/terminal/index/<?=$user['id'];?>">لیست</a>)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کل درآمد</dt>
                    <dd><?=number_format($user['total_revenue']);?> ریال</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کل پرداختی ها به کاربر</dt>
                    <dd><?=number_format($totalPayments);?> ریال</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>موجودی فعلی</dt>
                    <dd><?=number_format($user['balance']);?> ریال</dd>
                </dl>
            </div>
        </div>
    </div>
</div>