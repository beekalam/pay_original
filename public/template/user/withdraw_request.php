<?php
if(isset($withdraw_error)) {
    if($withdraw_error == 1) {
?>
متاسفانه موجودی شما هنوز به حدنصاب جهت درخواست تسویه نرسیده است.<br>
موجودی شما : <?=number_format($_SESSION['user']['balance']);?> ریال<br>
حد نصاب : <?=number_format($withdraw_min);?> ریال
<?php
    } elseif($withdraw_error == 2) {
?>
بین درخواست های تسویه باید حداقل <?=$hours;?> ساعت فاصله باشد.
<?php
    } elseif($withdraw_error == 3) {
        if($_POST['type'] == 1) {
            ?>
            حساب انتخاب شده شماره کارت ندارد!
            <?php
        } else { ?>
            حساب انتخاب شده شماره شبا ندارد!
            <?php
        }
    }
} else {
?>
<?=$form;?>
<?php
}
?>