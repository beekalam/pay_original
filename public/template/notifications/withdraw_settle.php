<p><?=$user['name'];?> عزیز،</p>
<p>موجودی حساب شما تسویه شد.</p>
<p>&nbsp;</p>
<p>اطلاعات مبلغ واریزی به شرح ذیل میباشد :</p>
<p>تاریخ : <?=Date::formatDate('now');?></p>
<p>مبلغ واریزی : <?=$settle['value'];?></p>
<p>شناسه واریز : <?=$settle['track_number'];?></p>
<p>توضیحات : <?=$settle['description'];?></p>
