<?php
$no_layout = true;
?>
<div class="alert alert-info">
    توضیح : این اطلاعات آرشیو شده میباشد و ممکن است با اطلاعات فعلی کاربر متفاوت باشد.
</div>
<div class="row">
    <div class="col-xs-6">
        <div class="box mgb">
            <div class="title">اطلاعات</div>
            <div class="in">
                <p>
                    <span>نام و نام خانوادگی :</span>
                    <?=$info['name'];?>
                </p>
                <p>
                    <span>کد ملی :</span>
                    <?=$info['sh_melli'];?>
                </p>
                <p>
                    <span>تاریخ تولد :</span>
                    <?=$info['birthday'];?>
                </p>
                <p>
                    <span>موبایل :</span>
                    <?=$info['mobile'];?>
                </p>
                <p>
                    <span>کد پستی :</span>
                    <?=$info['zipcode'];?>
                </p>
                <p>
                    <span>آدرس :</span>
                    <?=$info['state_title'];?> - <?=$info['city_title'];?> - <?=nl2br($info['address']);?>
                </p>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اسکن کارت ملی</div>
            <div class="in">
                <div class="text-center">
                    <img src="upload/user-scan-archive-<?=$user['id'];?>-<?=$info['scan_hash'];?>.jpg" alt="اسکن کارت ملی" style="max-width: 100%;" />
                </div>
            </div>
        </div>
    </div>
</div>