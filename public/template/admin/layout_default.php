<!DOCTYPE html>
<html>
<head>
<base href="<?=SITE_URL; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="developer" content="XDev.ir">
<script type="text/javascript" src="template/cp/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="template/cp/js/ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="template/cp/plugins/datepicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="template/cp/plugins/datepicker/bootstrap-datepicker.fa.min.js"></script>
<script type="text/javascript" src="template/cp/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="template/core/core.js"></script>
<script type="text/javascript" src="template/core/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="template/core/ckeditor/config.js"></script>
<script type="text/javascript" src="template/cp/plugins/globalize/globalize.min.js"></script>
<script type="text/javascript" src="template/cp/plugins/DevExpressChartJS/dx.chartjs.js"></script>
<script type="text/javascript" src="template/cp/plugins/multiselect/js/ui.multiselect.js"></script>
<script type="text/javascript" src="template/cp/js/script.js?v=3.00"></script>
<?php template_files(); ?>
<link rel="stylesheet" type="text/css" href="template/cp/plugins/jqueryui/all/themes/base/jquery.ui.all.css" />
<link rel="stylesheet" type="text/css" href="template/cp/plugins/datepicker/bootstrap-datepicker.min.css" />
<link rel="stylesheet" type="text/css" href="template/cp/plugins/multiselect/css/ui.multiselect.css" />
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap-rtl.min.css" />
<link rel="stylesheet" type="text/css" href="template/cp/css/style.css" />
<title><?=$title_for_layout;?></title>
</head>
<body class="preload min-space">
<div class="loading"><span>در حال بارگذاری ...</span></div>
<div class="sidebar-container">
    <div class="head">
        <img src="template/cp/images/logo.png" alt="G8Way" />
    </div>
    <div id="navC">
        <ul class="nav">
        <li>
            <a href="<?=ADMINCP;?>/">
                <i class="glyphicon glyphicon-dashboard">
                    <b></b>
                </i>
                <span>داشبورد</span>
            </a>
        </li><?php if(get_sv('script') != 1) {?>
        <li>
            <a href="<?=ADMINCP;?>/ticket/index">
                <i class="glyphicon glyphicon-headphones">
                    <b></b>
                </i>
                <span>تیکت های پشتیبانی</span>
                <?php if(isset($pending_tickets)) {?>
                <span class="badge bg-red"><?=$pending_tickets;?></span>
                <?php }?>
            </a>
        </li> 
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-king">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیران</span>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/admin/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست مدیران
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/admin/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        افزودن مدیر جدید
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/department/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        مدیریت دپارتمان ها
                    </a>
                </li>
            </ul>
        </li> 
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-user">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیریت کاربران</span>
                <?php if($pending_infos>0) {?>
                <span class="badge bg-red"><?=(int)($pending_infos);?></span>
                <?php }?>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/user/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست کاربران
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/user/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        افزودن کاربر جدید
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/user/pendingInfo">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        اطلاعات در انتظار تایید<?php if($pending_infos>0) {?> <span class="badge bg-red"><?=(int)($pending_infos);?><?php }?>
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/guestpay/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست صفحات پرداخت مهمان
                    </a>
                </li>
            </ul>
        </li><?php }?> 
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-sort">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیریت ترمینال ها</span>
                <?php if(@$pending_terminals>0) {?>
                <span class="badge bg-red"><?=(int)(@$pending_terminals);?></span>
                <?php }?>
            </a>
            <ul><?php if(get_sv('script') != 1) {?>
                <li>
                    <a href="<?=ADMINCP;?>/terminal/pendingIndex">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ترمینال های در انتظار تایید <?php if($pending_terminals>0) {?> <span class="badge bg-red"><?=(int)($pending_terminals);?><?php }?>
                    </a>
                </li><?php } ?> 
                <li>
                    <a href="<?=ADMINCP;?>/terminal/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست ترمینال ها
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/terminal/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        افزودن ترمینال جدید
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-link">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیریت لینک ها</span>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/link/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست لینک های پرداخت
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/link/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ایجاد لینک پرداخت جدید
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-check">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیریت فرم ها</span>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/form/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست فرم های پرداخت
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/form/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ایجاد فرم پرداخت جدید
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/form/dataIndex">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فرم های ثبت شده
                    </a>
                </li>
            </ul>
        </li><?php if(get_sv('script') != 1) {?>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-bullhorn">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>اطلاع رسانی</span>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/notification/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست اطلاعیه های عمومی
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/notification/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ارسال اطلاعیه عمومی جدید
                    </a>
                </li>
            </ul>
        </li><?php if(get('withdraw_active')) { ?>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-usd">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>تسویه حساب</span>
                <?php if(isset($pending_withdraws)) {?>
                <span class="badge bg-red"><?=$pending_withdraws;?></span>
                <?php }?>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/withdraw/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست درخواست های تسویه <?php if(isset($pending_withdraws)) {?><span class="badge bg-red"><?=$pending_withdraws;?></span><?php }?>
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/withdraw/archive">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        آرشیو تسویه حساب ها
                    </a>
                </li>
            </ul>
        </li><?php }}?>
        <li>
            <a href="<?=ADMINCP;?>/invoice/index">
                <i class="glyphicon glyphicon-list">
                    <b></b>
                </i>
                <span>فهرست تراکنش ها</span>
            </a>
        </li> 
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-road">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>درگاه های بانکی</span>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/gateway/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست درگاه های بانکی
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/gateway/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        افزودن درگاه بانکی
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/gateway/setting">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        درگاه پیشفرض
                    </a>
                </li>
            </ul>
        </li><?php if(get_sv('script') != 1) {?> 
        <li>
            <a href="<?=ADMINCP;?>/report/revenues">
                <i class="glyphicon glyphicon-stats">
                    <b></b>
                </i>
                <span>گزارش درآمدها</span>
            </a>
            <ul>
            </ul>
        </li> 
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-comment">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>مدیریت محتوا</span>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/page/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست صفحات ثابت
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/page/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        افزودن صفحه جدید
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/news/index">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست اخبار
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/news/add">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        افزودن خبر جدید
                    </a>
                </li>
            </ul>
        </li><?php } ?>
        <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-cog">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>تنظیمات</span>
            </a>
            <ul>
                <li>
                    <a href="<?=ADMINCP;?>/setting/edit/site">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        تنظیمات اصلی سایت
                    </a>
                </li><?php if(get_sv('script') != 1) {?>
                <li>
                    <a href="<?=ADMINCP;?>/setting/edit/moderate">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        تنظیمات مدیریتی
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/setting/edit/withdraw">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        تنظیمات تسویه حساب
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/setting/edit/karmozd">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        تنظیمات کارمزد
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/setting/notifs">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        تنظیمات اطلاع رسانی
                    </a>
                </li><?php } ?>
                <li>
                    <a href="<?=ADMINCP;?>/setting/edit/sms">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        تنظیمات SMS
                    </a>
                </li>
                <li>
                    <a href="<?=ADMINCP;?>/admin/config">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        اطلاعات ورود
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    </div>
    <div class="foot">
        پنل مدیریت
    </div>
</div>
<div class="content-container">
    <div class="head">
        <div class="profile">
            <span><?=$_SESSION['admin']['username'];?></span>
        </div>
        <div class="signout">
            <a href="<?=ADMINCP;?>/admin/signout">
                <i class="glyphicon glyphicon-remove-sign"></i>
                <span>&nbsp; خروج</span>
            </a>
        </div>
        <div class="navBtn">
            <a href="#">
                <i class="glyphicon glyphicon-menu-hamburger"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="content">
        <?php if(isset($system_message)){ ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>پیغام سیستم : </strong>
            <?=$system_message;?>
        </div>
        <?php } ?><?php if(isset($error)) { ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>خطا : </strong>
            <?=$error;?>
        </div><?php } ?>
        <?php if(isset($no_layout)) {
            echo $content_for_layout;
        } else { ?>
        <div class="box">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <?=$content_for_layout;?>
            </div>
        </div>
        <?php } ?>
        <div id="inline_load" style="display: none;"></div>
        <?php if(isset($page_description)) { ?>
        <div class="box mgt">
            <div class="in">
                <p class="text-info">
                    <?=$page_description;?>
                </p>
            </div>
        </div><?php } ?>
        <div class="box footer mgt">
            <div class="in">
                <div class="txt-r">
                    <?php show_cr(false);?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="DeleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">پیغام سیستم</h4>
            </div>
            <div class="modal-body">
                <p>آیا از حذف این رکورد اطمینان دارید؟</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal"> انصراف </button>
                <button type="button" class="btn btn-warning" onclick="ModalOkButton();"> &nbsp; بلی &nbsp; </button>
            </div>
        </div>
    </div>
</div>
</body>
</html>