<?php
$no_layout = true;
?>
<style type="text/css">
.box.info{
    border: 4px solid #46b8da;
}
.hiddenHelper{
    color: #46b8da;
    font-weight: bold;
    display: inline-block;
    font-size: 20px;
    line-height: 20px;
    border-radius: 13px;
    border: 2px solid #46b8da;
    width: 25px;
    height: 25px;
    opacity:0;
    text-align: center;
}
.grouped{
    margin-bottom: 5px;
    padding-bottom: 5px;
    border-radius: 5px;
    border: 2px solid transparent;
    border-bottom-color: rgb(127, 184, 230);
    background: rgba(127,184,230,0.05);
}
</style>
<script type="text/javascript">
function do_filter(){
    var path = '<?=ADMINCP;?>/invoice/index/';
    path += $('#invoice_status').val()+'/';
    path += $('#invoice_type').val()+'/';
    path += $('#invoice_ipp').val()+'/';
    path += ($('#hidden_from_date').val() !== '') ? $('#hidden_from_date').val().replace(/\//g, '-')+'/' : 'null/';
    path += ($('#hidden_to_date').val() !== '') ? $('#hidden_to_date').val().replace(/\//g, '-')+'/' : 'null/';
    path += ($('#hidden_exact_date').val() !== '') ? $('#hidden_exact_date').val().replace(/\//g, '-')+'/' : 'null/';
    path += ($('#exact_hour').val() !== '') ? $('#exact_hour').val()+'/' : 'null/';
    path += ($('#bank_code').val() !== '') ? $('#bank_code').val()+'/' : 'null/';
    path += ($('#amount').val() !== '') ? $('#amount').val()+'/' : 'null/';
    path += ($('#card_number').val() !== '') ? $('#card_number').val()+'/' : 'null/';
    path += ($('#ip').val() !== '') ? $('#ip').val()+'/' : 'null/';
    path += ($('#user_id').val() !== '') ? $('#user_id').val()+'/' : 'null/';
    path += ($('#terminal_id').val() !== '') ? $('#terminal_id').val()+'/' : 'null/';
    path += ($('#gateway_id').val() !== '') ? $('#gateway_id').val()+'/' : 'null/';
    path += ($('#invoice_id').val() !== '') ? $('#invoice_id').val()+'/' : 'null/';
    path += ($('#order_id').val() !== '') ? $('#order_id').val()+'/' : 'null/';
    window.location = path;
}
function do_export(){
    var path = '<?=ADMINCP;?>/invoice/export/';
    path += $('#invoice_status').val()+'/';
    path += $('#invoice_type').val()+'/';
    path += ($('#hidden_from_date').val() !== '') ? $('#hidden_from_date').val().replace(/\//g, '-')+'/' : 'null/';
    path += ($('#hidden_to_date').val() !== '') ? $('#hidden_to_date').val().replace(/\//g, '-')+'/' : 'null/';
    path += ($('#hidden_exact_date').val() !== '') ? $('#hidden_exact_date').val().replace(/\//g, '-')+'/' : 'null/';
    path += ($('#exact_hour').val() !== '') ? $('#exact_hour').val()+'/' : 'null/';
    path += ($('#bank_code').val() !== '') ? $('#bank_code').val()+'/' : 'null/';
    path += ($('#amount').val() !== '') ? $('#amount').val()+'/' : 'null/';
    path += ($('#card_number').val() !== '') ? $('#card_number').val()+'/' : 'null/';
    path += ($('#ip').val() !== '') ? $('#ip').val()+'/' : 'null/';
    path += ($('#user_id').val() !== '') ? $('#user_id').val()+'/' : 'null/';
    path += ($('#terminal_id').val() !== '') ? $('#terminal_id').val()+'/' : 'null/';
    path += ($('#gateway_id').val() !== '') ? $('#gateway_id').val()+'/' : 'null/';
    path += ($('#invoice_id').val() !== '') ? $('#invoice_id').val()+'/' : 'null/';
    path += ($('#order_id').val() !== '') ? $('#order_id').val()+'/' : 'null/';
    window.location = path;
}
$(document).ready(function(){
    // date picker
    $('#exact_date').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        dateFormat: 'DD، d MM yy',
        altField: '#hidden_exact_date',
        altFormat: 'yy/mm/dd',
        /*maxDate: "0",*/
    });
    <?php if(isset($default_exact_date)){?>
    $('#exact_date').datepicker('setDate',new JalaliDate(<?=$default_exact_date;?>));
    <?php } ?>
    $('#from_date').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        dateFormat: 'DD، d MM yy',
        altField: '#hidden_from_date',
        altFormat: 'yy/mm/dd',
        /*maxDate: "0",*/
    });
    <?php if(isset($default_from_date)){?>
    $('#from_date').datepicker('setDate',new JalaliDate(<?=$default_from_date;?>));
    <?php } ?>
    $('#to_date').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        dateFormat: 'DD، d MM yy',
        altField: '#hidden_to_date',
        altFormat: 'yy/mm/dd',
        /*maxDate: "0",*/
    });
    <?php if(isset($default_to_date)){?>
    $('#to_date').datepicker('setDate',new JalaliDate(<?=$default_to_date;?>));
    <?php } ?>
});

var invSerachHelpActive = false;
function invSerachHelp()
{
    if(!invSerachHelpActive) {
        $('#invSearchHelpBox').slideDown('slow');
        $('.hiddenHelper').css({opacity:1});
        invSerachHelpActive = true;
    }
}
</script>
        <div class="box mgb">
            <form onsubmit="do_filter(); return;">
            <div class="in">
                <div class="row mgb">
                    <div class="col-xs-6">
                        <div class="grouped">
                            <div class="row">
                                <div class="col-xs-6<?php if(isset($range_date_error)){?> has-error<?php }?>">
                                    <label for="from_date">از تاریخ <span class="hiddenHelper">1</span></label>
                                    <input type="text" id="from_date" readonly="readonly" class="form-control" />
                                    <input type="hidden" name="from_date" id="hidden_from_date" />
                                </div>
                                <div class="col-xs-6<?php if(isset($range_date_error)){?> has-error<?php }?>">
                                    <label for="to_date">تا تاریخ <span class="hiddenHelper">1</span></label>
                                    <input type="text" id="to_date" readonly="readonly" class="form-control" />
                                    <input type="hidden" name="to_date" id="hidden_to_date" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="grouped">
                            <div class="row">
                                <div class="col-xs-6<?php if(isset($exact_date_error)){?> has-error<?php }?>">
                                    <label for="exact_date">تاریخ <span class="hiddenHelper">2</span></label>
                                    <input type="text" id="exact_date" readonly="readonly" class="form-control" />
                                    <input type="hidden" name="exact_date" id="hidden_exact_date" />
                                </div>
                                <div class="col-xs-6<?php if(isset($exact_date_error)){?> has-error<?php }?>">
                                    <label for="exact_hour">ساعت <span class="hiddenHelper">2</span></label>
                                    <input type="text" id="exact_hour" class="form-control" value="<?=@$exact_hour;?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mgb">
                    <div class="col-xs-3">
                        <label for="bank_code">شماره پیگیری <span class="hiddenHelper">3</span></label>
                        <input type="text" id="bank_code" class="form-control" value="<?=@$bank_code;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="amount">مبلغ تراکنش (ریال)</label>
                        <input type="text" id="amount" class="form-control" value="<?=@$amount;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="card_number">شماره کارت <span class="hiddenHelper">4</span></label>
                        <input type="text" id="card_number" class="form-control" value="<?=@$card_number;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="ip">IP</label>
                        <input type="text" id="ip" class="form-control" value="<?=@$ip;?>" />
                    </div>
                </div>
                <div class="row mgb">
                    <div class="col-xs-3">
                        <label for="invoice_id">شناسه تراکنش</label>
                        <input type="text" id="invoice_id" class="form-control" value="<?=@$invoice_id;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="user_id">کاربر <span class="hiddenHelper">5</span></label>
                        <input type="text" id="user_id" class="form-control" value="<?=@$user_id;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="terminal_id">ترمینال <span class="hiddenHelper">6</span></label>
                        <input type="text" id="terminal_id" class="form-control" value="<?=@$terminal_id;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="gateway_id">درگاه</label>
                        <select id="gateway_id" class="form-control">
                            <option value="null">همه موارد</option><?php foreach($gateways as $gateway) {?>
                            <option value="<?=$gateway['id'];?>"<?php if(@$gateway_id == $gateway['id']){?> selected="selected"<?php }?>><?=$gateway['title'];?></option><?php }?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3">
                        <label for="order_id">شناسه سفارش (کاربر) <span class="hiddenHelper">7</span></label>
                        <input type="text" id="order_id" class="form-control" value="<?=@$order_id;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="invoice_status">وضعیت تراکنش</label>
                        <select id="invoice_status" class="form-control">
                            <option value="null">همه موارد</option>
                            <option value="0"<?php if(@$status == '0'){?> selected="selected"<?php }?>>معلق</option>
                            <option value="1"<?php if(@$status == '1'){?> selected="selected"<?php }?>>پرداخت موفق</option>
                            <option value="2"<?php if(@$status == '2'){?> selected="selected"<?php }?>>پرداخت ناموفق</option>
                            <option value="3"<?php if(@$status == '3'){?> selected="selected"<?php }?>>خطای اتصال</option>
                            <option value="4"<?php if(@$status == '4'){?> selected="selected"<?php }?>>تراکنش غیر مجاز</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label for="invoice_type">نوع تراکنش</label>
                        <select id="invoice_type" class="form-control">
                            <option value="null">همه موارد</option>
                            <option value="1"<?php if(@$type == '1'){?> selected="selected"<?php }?>>از طریق API</option>
                            <option value="2"<?php if(@$type == '2'){?> selected="selected"<?php }?>>از طریق لینک پرداخت</option>
                            <option value="3"<?php if(@$type == '3'){?> selected="selected"<?php }?>>از طریق فرم پرداخت</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label for="invoice_ipp">تعداد در هر صفحه</label>
                        <select id="invoice_ipp" class="form-control">
                            <option value="10">10</option>
                            <option value="20"<?php if(@$ipp == '20'){?> selected="selected"<?php }?>>20</option>
                            <option value="50"<?php if(@$ipp == '50'){?> selected="selected"<?php }?>>50</option>
                            <option value="100"<?php if(@$ipp == '100'){?> selected="selected"<?php }?>>100</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div>&nbsp;</div>
                        <button class="btn btn-success" type="button" onclick="do_filter()">
                            <span>اعمال فیلتر</span>
                        </button> &nbsp; 
                        <button class="btn btn-primary" type="button" onclick="do_export()">
                            <span>خروجی اکسل</span>
                        </button> &nbsp; 
                        <button class="btn btn-info" type="button" onclick="invSerachHelp()">
                            <span>راهنما</span>
                        </button>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="box mgb info" style="display: none;" id="invSearchHelpBox">
            <div class="in">
                <p><b>راهنمای جستجو و فیلتر کردن تراکنش ها :</b></p>
                <p>1- در صورتی که میخواهید تراکنش های یک بازه زمانی یا یک روز خاصی را مشاهده کنید دو فیلد از تاریخ و تا تاریخ را انتخاب کنید. در صورتی که فقط یکی از این دو فیلد را انتخاب کنید، تاریخ در فیلتر اعمال نخواهد شد.</p>
                <p>2- در صورتی که میخواهید تراکنش های یک روز و ساعت خاص را مشاهده کنید، میبایست تاریخ و ساعت انجام تراکنش را انتخاب کنید. در صورتی که فقط یکی از این دو فیلد را انتخاب کنید، تاریخ و ساعت در فیلتر اعمال نخواهد شد.</p>
                <p>3- از گزینه شماره پیگیری میتوانید برای جستجوی کد رفرنس بانک، کد پیگیری بانک و یا کد خطای برگشتی از بانک استفاده کنید.</p>
                <p>4- در صورتی که شماره کارت ها کامل ذخیره نمیشوند و فقط 6 رقم اول و 4 رقم آخر نمایش داده میشود برای جستجو نیز از همین فرمت استفاده کنید و یا فقط 4 رقم آخر را جستجو کنید.</p>
                <p>5- برای جستجوی کاربر، میتوانید شناسه کاربر، ایمیل، موبایل و یا نام کاربری (صفحه پرداخت مهمان) را برای گزینه کاربر وارد کنید.</p>
                <p>6- برای جستجوی ترمینال میتوانید شناسه ترمینال و یا دامین آن را برای گزینه ترمینال وارد کنید.</p>
                <p>7- شناسه سفارش همان شناسه ای میباشد که از طریق سایت کاربر (در صورت استفاده از API) به وبسرویس ارسال میشود و مرتبط با سایت کاربر است.</p>
            </div>
        </div>
        <div class="box table">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <?=$table;?>
            </div>
        </div>