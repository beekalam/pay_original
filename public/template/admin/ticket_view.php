<?php
$no_layout = true;
?>
<div class="box mgb">
    <div class="title"><?=$title_for_layout;?></div>
    <div class="body">
        <table class="table table-bordered" cellpadding="5" cellspacing="2" border="0" width="100%">
            <thead>
                <tr>
                    <td>وضعیت : <?=$GLOBALS['lists']['ticket_status'][$ticket['status']];?>
                    <td>دپارتمان : <?=@$GLOBALS['departments'][$ticket['department']];?> (<a href="<?=ADMINCP;?>/ticket/transferToDep/<?=$ticket['id'];?>">تغییر</a>)
        <?php if($ticket['status']!=0){?> (<a href="<?=ADMINCP;?>/ticket/close/<?=$ticket['id'];?>">بستن تیکت</a>)<?php } ?></td>
                    <td>توسط : <a href="<?=ADMINCP;?>/user/view/<?=$user['id'];?>"><?=$user['name'];?></a></td>
                    <td>تاریخ ایجاد : <?=Date::formatDate($ticket['submit_date'],'d M Y');?></td>
                </tr>
            </thead>
        </table>
        <div class="in">
            <form action="<?=ADMINCP;?>/ticket/post/<?=$ticket['id'];?>" method="post">
                <div>متن :</div>
                <p><textarea name="text" class="ckeditor" rows="4"></textarea></p>
                <p>
                    تغییر وضعیت * : 
                    <select name="status" id="field_status" style="max-width:350px;" class="validate[required] chzn-select">
                        <option value="0">بسته</option>
                        <option value="1">در انتظار بررسی</option>
                        <option value="3" selected="selected">پاسخ داده شد</option>
                        <option value="4">در حال بررسی</option>
                        <option value="2">حذف کامل تیکت</option>
                    </select>
                </p>
                <input type="submit" value="ثبت پاسخ / تغییر وضعیت" class="btn btn-success" />
            </form>
        </div>
    </div>
</div>
<?php foreach($posts as $post) {?>
<div class="box mgb">
    <div class="body table">
        <table class="table table-bordered" cellpadding="5" cellspacing="2" border="0" width="100%">
            <thead>
                <tr>
                    <th>ارسال شده توسط <?=$post['by']==1?'کاربر':'شما';?></th>
                    <th>در <?=Date::formatDate($post['date'],'d M Y ساعت H و i دقیقه');?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2"><?=$post['text'];?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php }?>