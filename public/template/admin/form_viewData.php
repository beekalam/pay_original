<?php
$no_layout = true;
$modal_template = true;
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اطلاعات ثبت شده</div>
            <div class="in">
<?php foreach($data['data'] as $k => $v) {?>
                <dl class="dl-horizontal">
                    <dt><?=$fields[$k]['title'];?></dt>
                    <dd><?=nl2br($v);?></dd>
                </dl><?php } ?>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اطلاعات فرم و پرداخت</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>مبلغ</dt>
                    <dd><?=number_format($data['amount']);?> ریال</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>وضعیت</dt>
                    <dd><?=$GLOBALS['lists']['invoice_statuses'][$data['status']];?><?php if(isset($invoice)){?> (<a href="<?=ADMINCP;?>/invoice/view/<?=$invoice['id'];?>">مشاهده تراکنش</a>)<?php }?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تاریخ ثبت</dt>
                    <dd><?=Date::formatDate($data['date'],'j M Y ساعت H:i');?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>