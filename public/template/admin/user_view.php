<?php
$no_layout = true;
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اطلاعات کاربر</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>شناسه کاربر</dt>
                    <dd><?=$user['id'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>نام و نام خانوادگی</dt>
                    <dd><?=$user['name'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>پدر</dt>
                    <dd><?=$user['father'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کد ملی</dt>
                    <dd><?=$user['sh_melli'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تاریخ تولد :</dt>
                    <dd><?=$user['birthday'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>ایمیل</dt>
                    <dd><?=$user['email'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>موبایل</dt>
                    <dd><?=$user['mobile'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تلفن ثابت</dt>
                    <dd><?=$user['phone'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کد پستی</dt>
                    <dd><?=$user['zipcode'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>آدرس</dt>
                    <dd><?=$user['state_title'];?> - <?=$user['city_title'];?> - <?=nl2br($user['address']);?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تاریخ ایجاد اکانت</dt>
                    <dd><?=Date::formatDate($user['date'],'j M Y ساعت H:i');?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>آخرین IP</dt>
                    <dd><?=$user['last_ip'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تاریخ آخرین ورود</dt>
                    <dd><?=(empty($user['last_signin']))?'-':Date::formatDate($user['last_signin'],'j M Y ساعت H:i');?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>نام بانک</dt>
                    <dd><?=$user['bank_name'];?></dd>
                </dl>
                <!--<dl class="dl-horizontal">
                    <dt>شبای حساب</dt>
                    <dd class="en">IR<?=$user['bank_shaba'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>شماره کارت</dt>
                    <dd><?=$user['bank_card'];?></dd>
                </dl>-->
                <dl class="dl-horizontal">
                    <dt>اسکن کارت ملی</dt>
                    <dd>
                        <a href="upload/user-scan-<?=$user['id'];?>-<?=$user['scan_hash'];?>.jpg" target="_blank">
                            <img src="upload/user-scan-<?=$user['id'];?>-<?=$user['scan_hash'];?>.jpg" width="100" height="auto" alt="اسکن کارت ملی" />
                        </a>
                    </dd>
                </dl><?php if(get('photo_shenasname')) { ?>
                <dl class="dl-horizontal">
                    <dt>اسکن شناسنامه</dt>
                    <dd>
                        <a href="upload/user-scan2-<?=$user['id'];?>-<?=$user['scan_hash'];?>.jpg" target="_blank">
                            <img src="upload/user-scan2-<?=$user['id'];?>-<?=$user['scan_hash'];?>.jpg" width="100" height="auto" alt="اسکن شناسنامه" />
                        </a>
                    </dd>
                </dl><?php } ?>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="box mgb">
            <div class="title">آمار کاربر</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>ترمینال ها</dt>
                    <dd><?=number_format($terminalsCount);?> (<a href="<?=ADMINCP;?>/terminal/index/<?=$user['id'];?>">لیست</a>)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>لینک ها</dt>
                    <dd><?=number_format($linksCount);?> (<a href="<?=ADMINCP;?>/link/index/<?=$user['id'];?>">لیست</a>)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>فرم ها</dt>
                    <dd><?=number_format($formsCount);?> (<a href="<?=ADMINCP;?>/form/index/<?=$user['id'];?>">لیست</a>)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کل درآمد</dt>
                    <dd><?=number_format($user['total_revenue']);?> ریال</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کل پرداختی ها به کاربر</dt>
                    <dd><?=number_format($totalPayments);?> ریال</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>موجودی فعلی</dt>
                    <dd><?=number_format($user['balance']);?> ریال</dd>
                </dl>
            </div>
        </div>
        <div class="box mgb">
            <div class="title">عملیات</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>کاربر</dt>
                    <dd>
                        <a href="<?=ADMINCP;?>/user/signinAs/<?=$user['id'];?>" class="btn btn-info">ورود به پنل <i class="glyphicon glyphicon-user"></i></a> &nbsp;
                        <a href="<?=ADMINCP;?>/user/edit/<?=$user['id'];?>" class="btn btn-info">ویرایش اطلاعات <i class="glyphicon glyphicon-edit"></i></a>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>وضعیت اکانت</dt>
                    <?php if($user['status'] != 3) {?>
                    <dd><span class="text-success">آزاد</span> (<a href="<?=ADMINCP;?>/user/ban/<?=$user['id'];?>" class="btn btn-danger">مسدود کردن اکانت</a>)</dd>
                    <?php } else { ?>
                    <dd><span class="text-danger">مسدود</span> (<a href="<?=ADMINCP;?>/user/unban/<?=$user['id'];?>" class="btn btn-success">رفع مسدودی اکانت</a>)</dd>
                    <?php } ?>
                </dl>
                <dl class="dl-horizontal">
                    <dt>درخواست تسویه</dt>
                    <?php if($user['can_withdraw'] == 1) {?>
                    <dd><span class="text-success">آزاد</span> (<a href="<?=ADMINCP;?>/user/suspendWithdraw/<?=$user['id'];?>" class="btn btn-danger">مسدود کردن تسویه</a>)</dd>
                    <?php } else { ?>
                    <dd><span class="text-danger">مسدود</span> (<a href="<?=ADMINCP;?>/user/suspendWithdraw/<?=$user['id'];?>" class="btn btn-success">آزاد کردن تسویه</a>)</dd>
                    <?php } ?>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تغییر موجودی</dt>
                    <dd>
                        <a href="<?=ADMINCP;?>/user/changeBalance/<?=$user['id'];?>/0" class="btn btn-success">افزایش موجودی</a> &nbsp;
                        <a href="<?=ADMINCP;?>/user/changeBalance/<?=$user['id'];?>/1" class="btn btn-danger">کاهش موجودی</a>
                    </dd>
                </dl>
                <hr />
                <p class="txt-c">
                    <a href="<?=ADMINCP;?>/user/infoArchive/<?=$user['id'];?>" class="btn btn-info">آرشیو اطلاعات و مدارک کاربر</a>
                </p><?php if(!$user['email_verified'] || !$user['mobile_verified'] || !$user['verified']) {?>
                <hr />
                <p class="txt-c"><?php if(!$user['email_verified']) {?>
                    <a href="<?=ADMINCP;?>/user/verifyEmail/<?=$user['id'];?>" class="btn btn-warning">تایید دستی ایمیل کاربر</a> &nbsp; 
                    <?php }
                    if(!$user['mobile_verified']) { ?>
                    <a href="<?=ADMINCP;?>/user/verifyMobile/<?=$user['id'];?>" class="btn btn-warning">تایید دستی موبایل کاربر</a>
                    <?php }
                    if(!$user['verified']) { ?>
                    <a href="<?=ADMINCP;?>/user/approveInfo/<?=$user['id'];?>" class="btn btn-warning">تایید دستی اطلاعات کاربر</a>
                    <?php } ?>
                </p><?php }?>
            </div>
        </div>
    </div>
</div>