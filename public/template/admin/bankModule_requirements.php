<?php if($ajax_request) { ?><div style="display:none" id="inline_load_hidden_title"><?=$title_for_layout;?></div><?php } ?>
<div class="in">
	<p><?php if($configured) {?><b style="color: #008000;">تنظیمات این ماژول انجام شده است</b><?php } else {?><b style="color: #FF0000;">تنظیمات این ماژول هنوز انجام نشده است</b><?php } ?></p>
	<p>برای فعال سازی این ماژول نیاز است که تنظیمات زیر را در فایل config.php انجام دهید.</p>
	<p>لازم به ذکر است که این فایل (config.php) در دایکتوری include/ قرار دارد.</p>
	<p>کدهای زیر میبایست در انتهای فایل قبل از <code dir="ltr">?&gt;</code> قرار بگیرند.</p>

	<pre dir="ltr" style="text-align: left;"><?php foreach($requirements as $req) {?>
define('<?=$req['constant']?>','xxx');
<?php } ?>
</pre>
	<p>به جای <code><b>xxx</b></code> در کد های بالا میبایست مقادیری که از بانک مربوطه دریافت میکنید را قرار دهید.</p>
	<?php if(!empty($bankModule['description'])) {?>
	<p><b>توضیحات اضافی : </b> <?=$bankModule['description'];?></p>
	<?php }?>
</div>