<?php
$no_layout = true;
?>
<script type="text/javascript">
function do_filter(){
    var path = '<?=ADMINCP;?>/report/revenues/';
    path += (($('#type').val() != '')?$('#type').val():'null')+'/';
    path += $('#ipp').val()+'/';
    window.location = path;
}
</script>
<div class="box mgb">
            <div class="in">
                <div class="row">
                    <div class="col-xs-3">
                        <label for="type">نوع گزارش</label>
                        <select id="type" class="form-control">
                            <option value="terminals"<?php if(@$type == 'terminals'){?> selected="selected"<?php }?>>درآمد روزانه ترمینال ها</option>
                            <option value="users"<?php if(@$type == 'users'){?> selected="selected"<?php }?>>درآمد روزانه کاربران</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label for="ipp">تعداد در هر صفحه</label>
                        <select id="ipp" class="form-control">
                            <option value="10">10</option>
                            <option value="20"<?php if(@$ipp == '20'){?> selected="selected"<?php }?>>20</option>
                            <option value="50"<?php if(@$ipp == '50'){?> selected="selected"<?php }?>>50</option>
                            <option value="100"<?php if(@$ipp == '100'){?> selected="selected"<?php }?>>100</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <div>&nbsp;</div>
                        <button class="btn btn-success" type="button" onclick="do_filter()">
                            <span>نمایش گزارش</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php if(isset($revenues)) {?>
        <div class="box table">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <table class="table table-bordered">
                    <colgroup width="20%"></colgroup>
                    <colgroup width="11%"></colgroup>
                    <colgroup width="11%"></colgroup>
                    <colgroup width="11%"></colgroup>
                    <colgroup width="11%"></colgroup>
                    <colgroup width="11%"></colgroup>
                    <colgroup width="11%"></colgroup>
                    <colgroup width="11%"></colgroup>
                    <tr>
                        <th></th>
                        <th><?=$graph_lables[0];?></th>
                        <th><?=$graph_lables[1];?></th>
                        <th><?=$graph_lables[2];?></th>
                        <th><?=$graph_lables[3];?></th>
                        <th><?=$graph_lables[4];?></th>
                        <th><?=$graph_lables[5];?></th>
                        <th><?=$graph_lables[6];?></th>
                    </tr>
                    <?php foreach($revenues as $row) {?> 
                    <tr>
                        <th>
                            <?php if($type == 'terminals') {?>
                            <a href="<?=ADMINCP;?>/terminal/view/<?=$row['terminal_id'];?>"><?=$row['domain'];?></a>
                            <?php } else { ?>
                            <a href="<?=ADMINCP;?>/user/view/<?=$row['user_id'];?>"><?=$row['name'];?>
                            <?php } ?>
                        </th>
                        <td><?=number_format(@$row['revenues'][6]/10);?> تومان</td>
                        <td><?=number_format(@$row['revenues'][5]/10);?> تومان</td>
                        <td><?=number_format(@$row['revenues'][4]/10);?> تومان</td>
                        <td><?=number_format(@$row['revenues'][3]/10);?> تومان</td>
                        <td><?=number_format(@$row['revenues'][2]/10);?> تومان</td>
                        <td><?=number_format(@$row['revenues'][1]/10);?> تومان</td>
                        <td><?=number_format(@$row['revenues'][0]/10);?> تومان</td>
                    </tr>
                    <?php }?> 
                </table>
                <?php if(!empty($pagination)) { ?>
                <div class="foot-pagi">
                    <?=@$pagination;?>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php }?> 