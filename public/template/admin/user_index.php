<?php
$no_layout = true;
?>
<script type="text/javascript">
function do_filter(){
    var path = '<?=ADMINCP;?>/user/index/';
    path += (($('#name').val() != '')?$('#name').val():'null')+'/';
    path += (($('#sh_melli').val() != '')?$('#sh_melli').val():'null')+'/';
    path += (($('#email').val() != '')?$('#email').val():'null')+'/';
    path += (($('#mobile').val() != '')?$('#mobile').val():'null')+'/';
    path += $('#status').val()+'/';
    path += $('#ipp').val()+'/';
    window.location = path;
}
</script>
<div class="box mgb">
            <div class="in">
                <div class="row">
                    <div class="col-xs-3">
                        <label for="name">نام</label>
                        <input id="name" name="name" class="form-control" value="<?=@$name;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="sh_melli">کد ملی</label>
                        <input id="sh_melli" name="sh_melli" class="form-control" value="<?=@$sh_melli;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="email">ایمیل</label>
                        <input id="email" name="email" class="form-control" value="<?=@$email;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="mobile">موبایل</label>
                        <input id="mobile" name="mobile" class="form-control" value="<?=@$mobile;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="status">وضعیت کاربر</label>
                        <select id="status" class="form-control">
                            <option value="null">همه موارد</option>
                            <option value="0"<?php if(@$status == '0'){?> selected="selected"<?php }?>>در انتظار تایید</option>
                            <option value="1"<?php if(@$status == '1'){?> selected="selected"<?php }?>>تایید شده</option>
                            <option value="3"<?php if(@$status == '3'){?> selected="selected"<?php }?>>حساب مسدود</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label for="ipp">تعداد در هر صفحه</label>
                        <select id="ipp" class="form-control">
                            <option value="10">10</option>
                            <option value="20"<?php if(@$ipp == '20'){?> selected="selected"<?php }?>>20</option>
                            <option value="50"<?php if(@$ipp == '50'){?> selected="selected"<?php }?>>50</option>
                            <option value="100"<?php if(@$ipp == '100'){?> selected="selected"<?php }?>>100</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <div>&nbsp;</div>
                        <button class="btn btn-success" type="button" onclick="do_filter()">
                            <span>اعمال فیلتر/جستجو</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
<div class="box table">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <?=$table;?>
            </div>
        </div>