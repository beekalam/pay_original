<?php
$no_layout = true;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box mgb">
            <div class="title">تراکنش های 10 روز گذشته</div>
            <div class="in">
                <div id="graph_revenues" style="height:200px;width:100%;"></div>
            
            </div>
        </div>
        <div class="box mgb table">
            <div class="title">مجموع تراکنش ها و سود سیستم</div>
            <table class="table table-bordered">
            <tr>
                <th></th>
                <th>امروز</th>
                <th>دیروز</th>
                <th>این هفته</th>
                <th>این ماه</th>
                <th>کل</th>
            </tr>
            <tr>
                <th>مجموع تراکنش ها</th>
                <td><?=number_format($revenues['today']['amount']/10);?> تومان</td>
                <td><?=number_format($revenues['yesterday']['amount']/10);?> تومان</td>
                <td><?=number_format($revenues['week']['amount']/10);?> تومان</td>
                <td><?=number_format($revenues['month']['amount']/10);?> تومان</td>
                <td><?=number_format($revenues['total']['amount']/10);?> تومان</td>
            </tr>
            <tr>
                <th>سود سیستم</th>
                <td><?=number_format($revenues['today']['karmozd']/10);?> تومان</td>
                <td><?=number_format($revenues['yesterday']['karmozd']/10);?> تومان</td>
                <td><?=number_format($revenues['week']['karmozd']/10);?> تومان</td>
                <td><?=number_format($revenues['month']['karmozd']/10);?> تومان</td>
                <td><?=number_format($revenues['total']['karmozd']/10);?> تومان</td>
            </tr>
        </table>
        </div>
        <div class="box table">
            <div class="title">آمار و ارقام</div>
            <table class="table table-bordered">
            <tr>
                <th>موجودی فعلی کاربران</th>
                <th>تعداد کاربران عضو</th>
                <th>تعداد کاربران فعال و وریفای شده</th>
                <th>تعداد ترمینال ها</th>
                <th>تعداد لینک های پرداخت</th>
                <th>تعداد فرم های پرداخت</th>
            </tr>
            <tr>
                <td><?=number_format(getUsersTotalBalance()/10);?> تومان</td>
                <td><?=number_format(getUsersCount());?></td>
                <td><?=number_format(getVerifiedUsersCount());?></td>
                <td><?=number_format(getTerminalsCount());?></td>
                <td><?=number_format(getLinksCount());?></td>
                <td><?=number_format(getFormsCount());?></td>
            </tr>
        </table>
        </div>
    </div>
</div>
<style type="text/css">
.dxc-tooltip{
    direction: ltr;
    text-align: left;
}
</style>
<script type="text/javascript">
var revenues = [<?=$revenues_data;?>];
function separator(input) {
    var input = (parseInt(input)).toString();
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(input)) {
        input = input.replace(rgx, '$1' + ',' + '$2');
    }
    return input;
}
$(document).ready(function(){
    $("#graph_revenues").dxChart({
        dataSource: revenues,
        commonSeriesSettings: {
            argumentField: "date2",
            tagField: "info"
        },
        series: [
            { valueField: "count", name: "Count", color: "#27c24c" },
        ],
        tooltip:{
            enabled: true,
            customizeText: function () {
                return separator(this.point.tag.amount/10) + 'T (' + this.valueText + ')';
            },
            font: { size: 14 }
        },
        legend: {
            visible: false
        },
        valueAxis:{
            grid:{
                color: '#9D9EA5',
                width: 0.5
            },
            label: { alignment: 'center' }
        },
        margin: {
            bottom: 20,
            left: 20,
        },
        rtlEnabled: true,
    });
});
</script>