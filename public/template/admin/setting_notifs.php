<?php
$no_layout = true;
?>
        <div class="box table">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <form method="POST" action="sysadmin/setting/notifs" class="form-horizontal">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>ارسال Email</th>
                                <th>ارسال SMS</th>
                                <th>ارسال Notification</th>
                            </tr>
                        </thead>
                        <tbody><?php
                        $tr_class = 'odd';
                        foreach($notifs as $notif => $ttl) {
                            $tr_class = ($tr_class == 'odd') ? 'even' : 'odd';
                        ?> 
                            <tr class="<?=$tr_class;?>">
                                <td><?=$ttl;?></td>
                                <td><input class="form-control" type="checkbox" value="1" name="<?=$notif;?>[email]"<?php if(@$data[$notif]['email']==true){?> checked="checked"<?php }?> /></td>
                                <td><input <?php if(!get('sms_active')) {?>disabled="disabled" <?php }?>class="form-control" type="checkbox" value="1" name="<?=$notif;?>[sms]"<?php if(@$data[$notif]['sms']==true){?> checked="checked"<?php }?> /></td>
                                <td><input <?php if($notif == 'invoice_payed') {?>disabled="disabled" <?php }?>class="form-control" type="checkbox" value="1" name="<?=$notif;?>[notif]"<?php if(@$data[$notif]['notif']==true){?> checked="checked"<?php }?> /></td>
                            </tr><?php }?>
                        </tbody>
                    </table>
                    <div class="buttons">
                        <button type="submit" class="btn btn-success">ثبت تغییرات</button>
                    </div>
                </form>
            </div>
        </div><?php if(!get('sms_active')) {?>
        <div class="box">
            <div class="in">
                توجه داشته باشید که اطلاع رسانی از طریق پیامک (SMS) نیازمند این میباشد که تنظیمات سامانه پیامک در اسکریپت ثبت شده باشد.<br>
                در صورتی که این کار را انجام نداده اید و میخواهید اطلاع رسانی پیامکی را فعال کنید با پشتیبانی اسکریپت تماس بگیرید.
            </div>
        </div><?php } ?>