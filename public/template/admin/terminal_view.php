<?php
$no_layout = true;
$show_btns = false;
if($terminal['status']==1) {
    $st = 'success';
    $desc = 'این ترمینال تایید شده است';
} elseif($terminal['status']==1) {
    $st = 'info';
    $desc = 'این ترمینال در انتظار تایید است';
    $show_btns = true;
} elseif($terminal['status'] == 3) {
    $st = 'danger';
    $desc = 'این ترمینال مسدود شده است';
} elseif($terminal['status'] == 4) {
    $st = 'danger';
    $desc = 'این ترمینال رد شده است';
}
?>
<div class="alert alert-<?=$st;?>">
    توضیح : <?=$desc;?>
</div>
<div class="row">
    <div class="col-xs-6">
        <div class="box mgb">
            <div class="title">اطلاعات ترمینال</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>شناسه ترمینال</dt>
                    <dd><?=$terminal['id'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>API KEY</dt>
                    <dd><input class="form-control" style="display: inline-block; width: 300px;" type="text" readonly="readonly" onfocus="this.select()" onclick="this.select()" dir="ltr" value="<?=$terminal["api_key"];?>" /></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کاربر</dt>
                    <dd><?=$user['name'];?> (<a href="<?=ADMINCP;?>/user/view/<?=$terminal['user_id'];?>/" target="_blank">نمایش کاربر در تب جدید</a>)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>عنوان ترمینال</dt>
                    <dd><?=$terminal['title'];?></dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>دامین</dt>
                    <dd><?=$terminal['domain'];?> (<a href="http://<?=$terminal['domain'];?>/" target="_blank">نمایش در تب جدید</a>)</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>کل درآمد ترمینال</dt>
                    <dd><?=number_format($terminal['total_revenue']/10);?> تومان</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>وضعیت ترمینال</dt>
                    <dd><?=$GLOBALS['lists']['terminal_status'][$terminal['status']];?></dd>
                </dl>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <?php if($terminal['status'] == 0) {?> 
        <div class="box mgb">
            <div class="title">تصمیم گیری</div>
            <div class="in">
                <p class="text-center"><a href="<?=ADMINCP;?>/terminal/approve/<?=$terminal['id'];?>" class="btn btn-success">تایید ترمینال</a></p>
                <hr />
                <form method="POST" action="<?=ADMINCP;?>/terminal/decline/<?=$terminal['id'];?>" class="">
                    <div class="input-group">
                        <input type="text" name="reason" class="form-control" placeholder="دلیل">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-danger">رد کردن ترمینال</button>
                        </span>
                    </div>
                </form>
            </div>
        </div><?php } ?> 
        <div class="box">
            <div class="title">عملیات</div>
            <div class="in">
                <p class="txt-c">
                    <a href="<?=ADMINCP;?>/terminal/edit/<?=$terminal['id'];?>" class="btn btn-info">ویرایش ترمینال <i class="glyphicon glyphicon-edit"></i></a> &nbsp; 
<?php if($terminal['status'] == 1) {?>
                    <a href="<?=ADMINCP;?>/terminal/ban/<?=$terminal['id'];?>" class="btn btn-danger">مسدود کردن ترمینال</a>
<?php } elseif($terminal['status'] == 3) { ?>
                    <a href="<?=ADMINCP;?>/terminal/unban/<?=$terminal['id'];?>" class="btn btn-danger">رفع مسدودی ترمینال</a>
<?php } ?>
                </p>
            </div>
        </div>
    </div>
</div>