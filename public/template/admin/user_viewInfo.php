<?php
$no_layout = true;
$show_btns = false;
if($user['verified']==1) {
    $st = 'success';
    $desc = 'اطلاعات این کاربر تایید شده است';
} elseif($user['needs_verify']==1) {
    $st = 'info';
    $desc = 'اطلاعات این کاربر در انتظار تایید است';
    $show_btns = true;
} else {
    $st = 'danger';
    if(empty($user['state_id']))
        $desc = 'این کاربر هنوز اطلاعات فردی اش را وارد نکرده است';
    else
        $desc = 'اطلاعات این کاربر رد شده است';
}
?>
<div class="alert alert-<?=$st;?>">
    توضیح : <?=$desc;?>
</div>
<div class="row">
    <div class="col-xs-6">
        <div class="box mgb">
            <div class="title">اطلاعات کاربر</div>
            <div class="in">
                <p>
                    <span>نام و نام خانوادگی :</span>
                    <?=$user['name'];?>
                </p>
                <p>
                    <span>پدر :</span>
                    <?=$user['father'];?>
                </p>
                <p>
                    <span>کد ملی :</span>
                    <?=$user['sh_melli'];?>
                </p>
                <p>
                    <span>تاریخ تولد :</span>
                    <?=$user['birthday'];?>
                </p>
                <p>
                    <span>موبایل :</span>
                    <?=$user['mobile'];?>
                </p>
                <p>
                    <span>کد پستی :</span>
                    <?=$user['zipcode'];?>
                </p>
                <p>
                    <span>آدرس :</span>
                    <?=$user['state_title'];?> - <?=$user['city_title'];?> - <?=nl2br($user['address']);?>
                </p>
            </div>
        </div><?php if($show_btns) {?> 
        <div class="box">
            <div class="title">تصمیم گیری</div>
            <div class="in">
                <p class="text-center"><a href="<?=ADMINCP;?>/user/approveInfo/<?=$user['id'];?>" class="btn btn-success">تایید اطلاعات</a></p>
                <hr />
                <form method="POST" action="<?=ADMINCP;?>/user/declineInfo/<?=$user['id'];?>" class="">
                    <div class="input-group">
                        <input type="text" name="reason" class="form-control" placeholder="دلیل">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-danger">رد کردن اطلاعات</button>
                        </span>
                    </div>
                </form>
            </div>
        </div><?php } ?>
    </div>
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اسکن کارت ملی</div>
            <div class="in">
                <div class="text-center">
                    <img src="upload/user-scan-<?=$user['id'];?>-<?=$user['scan_hash'];?>.jpg" alt="اسکن کارت ملی" style="max-width: 100%;" />
                </div>
            </div>
        </div>
    </div>
</div>