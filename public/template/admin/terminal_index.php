<?php
$no_layout = true;
?>
<script type="text/javascript">
function do_filter(){
    var path = '<?=ADMINCP;?>/terminal/index/';
    path += (($('#user_id').val() != '')?$('#user_id').val():'0')+'/';
    path += (($('#title').val() != '')?$('#title').val():'null')+'/';
    path += (($('#domain').val() != '')?$('#domain').val():'null')+'/';
    path += $('#status').val()+'/';
    path += $('#ipp').val()+'/';
    window.location = path;
}
</script>
<div class="box mgb">
            <div class="in">
                <div class="row">
                    <div class="col-xs-3">
                        <label for="user_id">شناسه کاربر</label>
                        <input id="user_id" name="user_id" class="form-control" value="<?=@$user_id;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="title">عنوان</label>
                        <input id="title" name="title" class="form-control" value="<?=@$title;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="domain">دامین</label>
                        <input id="domain" name="domain" class="form-control" value="<?=@$domain;?>" />
                    </div>
                    <div class="col-xs-3">
                        <label for="status">وضعیت ترمینال</label>
                        <select id="status" class="form-control">
                            <option value="null">همه موارد</option>
                            <option value="0"<?php if(@$status == '0'){?> selected="selected"<?php }?>>در انتظار تایید</option>
                            <option value="1"<?php if(@$status == '1'){?> selected="selected"<?php }?>>تایید شده</option>
                            <option value="3"<?php if(@$status == '3'){?> selected="selected"<?php }?>>ترمینال مسدود</option>
                            <option value="3"<?php if(@$status == '4'){?> selected="selected"<?php }?>>رد شده</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <label for="ipp">تعداد در هر صفحه</label>
                        <select id="ipp" class="form-control">
                            <option value="10">10</option>
                            <option value="20"<?php if(@$ipp == '20'){?> selected="selected"<?php }?>>20</option>
                            <option value="50"<?php if(@$ipp == '50'){?> selected="selected"<?php }?>>50</option>
                            <option value="100"<?php if(@$ipp == '100'){?> selected="selected"<?php }?>>100</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <div>&nbsp;</div>
                        <button class="btn btn-success" type="button" onclick="do_filter()">
                            <span>اعمال فیلتر/جستجو</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
<div class="box table">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <?=$table;?>
            </div>
        </div>