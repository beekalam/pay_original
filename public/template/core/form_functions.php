<?php
function make_obj(&$item, $id, $vars = array())
{
	$class = array();
	$field_defaults = ' name="'.$id.'" id="field_'.$id.'"'.@$item['extra'];
	$field_defaults2 = '';
	$validate = jquery_validate($item);
	if(!empty($validate) && $item['field'] != 'file')
		$class[] = 'validate['. implode(',',$validate).']';
	
	if(isset($item['dir']))
		$field_defaults2 .= ' dir="'.$item['dir'].'"';
	if(isset($item['align']))
		$field_defaults2 .= ' align="'.$item['align'].'"';
	if(isset($item['class']))
		$class[] = $item['class'];
	
	$field_defaults .= $field_defaults2;
	
	switch($item['field']) {
		case 'textbox':
		$type = (isset($item['field_type'])) ? $item['field_type'] : 'text';
		$maxLength = (isset($item['length'])) ? $item['length'] : ($item['type']=='text' && isset($item['max'])) ? $item['max'] : false;
		$addOnClass = $preAddOn = $postAddOn = '';
		if(isset($item['pre-add-on'])) {
			$addOnClass = 'input-prepend';
			$preAddOn = '<span class="add-on">'.$item['pre-add-on'].'</span>';
		}
		if(isset($item['post-add-on'])) {
			$addOnClass .= ' input-append';
			$postAddOn = '<span class="add-on">'.$item['post-add-on'].'</span>';
		}
		if(!empty($addOnClass))
			echo '<div class="'.$addOnClass.'">'.$preAddOn;
		$placeholder = (isset($item['placeholder']))?' placeholder="'.$item['placeholder'].'"':'';
		?>
		<input type="<?=$type?>"<?=$field_defaults.$placeholder;?><?php if($maxLength){?> maxlength="<?=$maxLength?>"<?php } if(isset($item['field_size'])){?> size="<?=$item['field_size']?>"<?php }?> value="<?=@$vars[$id]?>"<?=make_class($class);?> />
		<?php 
		if(!empty($addOnClass))
			echo $postAddOn.'</div>';
		break;
		
		case 'textarea':
		?>
		<textarea <?=$field_defaults?><?php if(isset($item['cols'])){?> cols="<?=$item['cols']?>"<?php } if(isset($item['rows'])){?> rows="<?=$item['rows']?>"<?php } ?><?=make_class($class);?>><?=@$vars[$id]?></textarea>
		<?php break;
        
        case 'bbeditor':
        $class[] = 'bbeditor';
        ?>
        <textarea <?=$field_defaults?><?=make_class($class);?>><?=@$vars[$id]?></textarea>
        <?php break;
		
		case 'tag':
		new_template_file('core','tags','css');
		new_template_file('core','tags','js');
		$item['extra_ending'] = '<div class="tag_show" id="tag_show_'.$id.'"></div>';
		if(!isset($item['description']))
			$item['description'] = 'با کاما (,) جدا کنید و یا بعد از وارد کردن هر عبارت Enter کنید';
		?>
		<input type="text" name="_tags_<?=$id;?>" class="_tags" id="_tags_<?=$id;?>" value="<?=@$vars[$id]?>"<?=$field_defaults2;?> style="width:350px;" />
		<input type="hidden" class="hidden_tags" name="<?=$id?>" id="tags_<?=$id;?>" />
		<?php break;
		
		case 'editor':
		$class[] = 'ckeditor';
		?>
		<textarea <?=$field_defaults?><?=make_class($class);?>><?=@$vars[$id]?></textarea>
		<?php break;
		
		case 'select':
			if($item['options'] == 'fromdb')
			{
				$item['options'] = DB::getRowsK2V($item['field_key'],$item['field_val'],$item['table'],@$item['condition']);
				$item['options'][''] = 'انتخاب کنید';
				ksort($item['options']);
			}
			elseif($item['options'] == 'lists')
			{
				$item['options'] = $GLOBALS['lists'][$item['list_key']];
			}
			$class[] = 'chzn-select';
		?>
		<select<?=$field_defaults;?> style="max-width:350px;"<?=make_class($class);?>>
		<?php
			foreach($item['options'] as $k => $v){
		?>
			<option value="<?=$k;?>"<?php if(@$vars[$id] == $k){?> selected="selected"<?php } ?>><?=$v;?></option>
		<?php } ?>
		</select>
		<?php break;
		
		case 'multiple_select':
			if(@$item['list'] == 'lists')
			{
				$item['options'] = $GLOBALS['lists'][$item['option_key']];
			}
			$class[] = 'multiple_select';
		?>
		<select name="<?=$id;?>[]" id="field_<?=$id;?>"<?=make_class($class);?> multiple="multiple" style="width:240px" data-placeholder="<?=$item['title'];?>">
		<?php
			foreach($item['options'] as $key => $val){
		?>
			<optgroup label="<?=$val['title'];?>">
<?php			foreach($val['items'] as $k => $v){ ?>
				<option value="<?=$k;?>"<?php if(@in_array($k,@$vars[$id])){?> selected="selected"<?php } ?>><?=$v;?></option>
<?php			} ?>
			</optgroup>
		<?php } ?>
		</select>
		<div class="cb"></div>
		<?php break;
		case 'checkbox':
		?>
		<input type="checkbox"<?=$field_defaults;?> value="1"<?php if(isset($vars[$id]) && $vars[$id]==1){?> checked="checked"<?php } ?><?=make_class($class);?>/>
		<?php break;
		case 'boolean':
		?>
		<ul class="formee-list">
			<li><input type="radio" name="<?=$id;?>" id="radio_<?=$id;?>_yes" value="1"<?php if(isset($vars[$id]) && $vars[$id]==true){?> checked="checked"<?php } ?>/><label for="radio_<?=$id;?>_yes">بلی</label></li>
			<li><input type="radio" name="<?=$id;?>" id="radio_<?=$id;?>_no" value="0"<?php if(isset($vars[$id]) && $vars[$id]==false){?> checked="checked"<?php } ?>/><label for="radio_<?=$id;?>_no">خیر</label></li>
		</ul>
		<?php break;
		case 'option':
		?>
		
		<?php break;
		case 'file':
		?>
		<input type="file"<?=$field_defaults;?><?=make_class($class);?>  style="display:none" />
		<div class="input-append">
		   <input id="alt_field_<?=$id;?>" class="input-large" type="text" onclick="$('input[id=field_<?=$id;?>]').click();">
		   <a class="btn" onclick="$('input[id=field_<?=$id;?>]').click();">انتخاب تصویر</a>
		</div>
		 
		<script type="text/javascript">
		$('input[id=field_<?=$id;?>]').change(function() {
		   $('#alt_field_<?=$id;?>').val($(this).val());
		});
		</script>
		<?php break;
		case 'custom':
			echo $item['data'];
			if(isset($item['eval']))
				eval($item['eval']);
		?>
		
		<?php break;
		case 'buttons':
			foreach($item as $button) {
				if(is_array($button)) {
					$field_defaults = '';
					$field_defaults .= ' class="btn btn-success '.@$button['class'].'"';
					if(isset($button['extra']))
						$field_defaults .= ' '.@$button['extra'];
		?>
		<button type="<?=$button['field_type']?>" <?=$field_defaults?>>
			<span><?=$button['title']?></span>
		</button> &nbsp;&nbsp;
	<?php
				}
			}
	}		
}

function make_class($arr)
{
	$res = implode(' ',$arr);
	return ' class="'.$res.'"';
}

function jquery_validate($item)
{
	# jquery validate
	$validate = array();
	if(@$item['required'] === true || @$item['required'] === '1' || @$item['required'] === 1)
		$validate[] = 'required';
	if(@$item['type'] == 'int') {
		$validate[] = 'custom[integer]';
		if(isset($item['min']))
			$validate[] = 'min['.$item['min'].']';
		if(isset($item['max']))
			$validate[] = 'max['.$item['max'].']';
	} elseif(@$item['type'] == 'text') {
		if(isset($item['min']))
			$validate[] = 'minSize['.$item['min'].']';
		if(isset($item['max']))
			$validate[] = 'maxSize['.$item['max'].']';
	} elseif(@$item['type'] == 'email')
		$validate[] = 'custom[email]';
	return $validate;
}
?>