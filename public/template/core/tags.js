$(document).ready(function(){
	$('._tags').keypress(function(event){
		if (event.which == '13') {
			event.preventDefault();
			add_tags(this);
		}
	});
	init_tags();
	$('.tag_show .tag').live('click', function(){
		remove($(this).parent().siblings('._tags'), $(this).text());
	});
	$('form').submit(function(){
		init_tags();
	});
	//init_tags();
});

// JavaScript Document
//var tags = new Array();
//var first =  false;
function init_tags()
{
	$('._tags').each(function(){
		if($(this).val() != '')
			add_tags(this);
	});
}


function add_tags(obj)
{
	var tags = tags2arr(obj);
	clear_tags(obj);	
	arr2tags(obj, tags);
}

// create an array containing old and new tags
function tags2arr(obj)
{
	tags = get_tags(obj);
	new_tags = $(obj).val().split(',');
	new_tags = $.map(new_tags, function(n){
		n = $.trim(n);
		if($.inArray(n,tags) == -1 && n.length > 2) {
			return n;
		} else {
			return null;
		}
	});
	$.merge(tags,new_tags);
	return tags;
}

function clear_tags(obj)
{
	$(obj).val('');
	$(obj).siblings('.hidden_tags').val('');
	$(obj).siblings('.tag_show').empty();
}

function arr2tags(obj, tags)
{
	$(obj).siblings('.hidden_tags').val(tags.join(','));
	$.map(tags, function(n){
		$(obj).siblings('.tag_show').append('<div class="tag" title="حذف کردن">'+n+'</div>');
	})
}

function remove(obj, str)
{
	tags = get_tags(obj);
	var index = $.inArray(str,tags);
	if(index != -1)
	{
		tags.splice(index,1);
		clear_tags(obj);
		arr2tags(obj,tags);
	}
}

function get_tags(obj)
{
	var res = $(obj).siblings('.hidden_tags').val();
	if(res != '' && res != null)
		return  res.split(',');
	else
		return new Array();
}