<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?php echo SITE_URL; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body{
	background:#999;
	font:11px/18px Tahoma;
}
a{
	text-decoration: none;
	color: #0000FF;
}
.box{
	background:#FFF;
	border:1px solid #CCC;
	width:400px;
	margin-top:60px;
	z-index:1;
	position:relative;
	-moz-border-radius: 10px; /* Firefox */
	-webkit-border-radius: 10px; /* Safari, Chrome */
	border-radius: 10px; /* universal */
	-moz-box-shadow: 2px 2px 5px #666; /* Firefox */
	-webkit-box-shadow: 2px 2px 5px #666; /* Safari, Chrome */
	box-shadow: 2px 2px 5px #666; /* CSS3 */
}
.box .inner{
	margin:20px 10px;
	text-align:right;
	direction:rtl;
}
input{font-family: tahoma;}
</style>
<title>پرداخت آنلاین</title>
</head>

<body>
<div align="center">
	<div class="icon"></div>
	<div class="box">
		<div class="inner">
			<p>بعد از یادداشت کردن کد رهگیری برای مراجعه به سایت بانک و پرداخت آنلاین بر روی "ادامه" کلیک کنید.</p>
			<p><b>کد رهگیری : <?=$bank_ref;?></b></p>
			<p align="center"><?=$form;?></p>
		</div>
	</div>
</div>
</body>
</html>