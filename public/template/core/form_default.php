<?php require_once('form_functions.php'); ?>
<form class="form-horizontal" action="<?=$form_info['action']?>" method="<?=$form_info['method']?>"<?php if(isset($form_info['name'])){ ?> name="<?=$form_info['name']?>"
<?php } if(isset($form_info['id'])){ ?> id="<?=$form_info['id']?>"<?php } if(isset($form_info['enctype'])){ ?>  enctype="<?=$form_info['enctype']?>"<?php } if(isset($form_info['onSubmit'])){ ?> onSubmit="<?=$form_info['onSubmit']?>"<?php } ?>>
<?php if(isset($form_description)) { ?>
<div class="row-fluid">
	<div class="span12">
		<p class="text-info">
			<?=$form_description;?>
		</p>
	</div>
</div>
<?php
}
$item_count = count($params) - 1;
$i = 0;
foreach($params as $id => $item){
	$i++;
	if(isset($item['noform']) && $item['noform'])
		continue;
	$extra_ending = '';
	?>
	<?php if($id != 'buttons') { ?> 
	<div class="control-group<?php if(isset($data[$id]['result']) && !$data[$id]['result']){?> error<?php }?>">
		<label for="field_<?=$id?>" class="control-label"><?=$item['title']?><?=(isset($item['required']) && $item['required'] === true)?' *':''?></label>
		<div class="controls">
			<?php make_obj($item, $id, $vars); ?>
	<?php } else {
	?>
	<p class="form-submit" align="center">
	<?php
		make_obj($item, $id, $vars);
	}
	if(@$item['type'] != 'group') {
		if(isset($data[$id]['result']) && !$data[$id]['result']){?>
		<span class="help-inline"><?=$data[$id]['error']?></span>
		<?php }
		if(isset($item['description'])) {?>
		<span class="help-block"><?=$item['description']?></span>
		<?php } 
		if(!empty($item['extra_ending']))
			echo $item['extra_ending'];
	}
	?>
	<?php if($id != 'buttons') { ?> 
		</div>
	</div>
	<?php } else { ?>
	</p>
	<?php } ?>
	
<?php } ?>
<?php if(isset($return_url)) { ?>
	<input type="hidden" name="return_url" value="<?=$return_url;?>" />
<?php }?>
</form>