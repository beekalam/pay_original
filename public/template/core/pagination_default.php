<?php
if($pages_count <= 1)
{
	// empty pagination
}
else
{
	echo '<div class="pull-left"><span class="muted">نمایش '.$ipp.' مورد از '.$total_records.' مورد</span></div>';
	echo '<div class="pagination">';
	echo '<ul>';
	$class = (Routing::$page == 1) ? ' disabled' : '';
	echo '<li class="first'.$class.'"><a href="'.$path.'page:1">&laquo;</a></li>';
	$range = 3;
	$start = (Routing::$page - $range > 0) ? Routing::$page - $range : 1;
	$end = (Routing::$page + $range < $pages_count) ? Routing::$page + $range : $pages_count;
	for($i = $start; $i <= $end; $i++)
	{
		$class = '';
		if($i == Routing::$page)
			$class = 'active ';
		if($i == $pages_count && $i == Routing::$page)
			$class .= 'last ';
		if($i == 1 && $i == Routing::$page)
			$class .= 'first ';
		
		if(!empty($class))
			$class = ' class="'.rtrim($class,' ').'"';
		echo '<li'.$class.'><a href="'.$path.'page:'.$i.'">'.$i.'</a></li>';
		
	}
	$class = (Routing::$page >= $pages_count) ? ' disabled' : '';
	echo '<li class="last'.$class.'"><a href="'.$path.'page:'.$pages_count.'">&raquo;</a></li>';
	echo '</ul>';
	echo '</div>';
}
?>