/*
 * jQuery UI Multiselect 2.0
 *
 * Authors:
 *  Yanick Rochon (yanick.rochon[at]gmail[dot]com)
 *
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://mind2soft.com/labs/jquery/multiselect/
 *
 *
 * Localization : FA
 *
 */

(function($) {

$.uix.multiselect.i18n['fa'] = {
  itemsSelected_nil: 'هیچ موردی انتخاب نشده',          // 0
  itemsSelected: '{count} مورد انتخاب شده',          // 0, 1
  itemsSelected_plural: '{count} مورد انتخاب شده',  // n
  //itemsSelected_plural_two: ...                    // 2
  //itemsSelected_plural_few: ...                    // 3, 4
  itemsAvailable_nil: 'هیچ موردی نمانده',
  itemsAvailable: '{count} مورد مانده',
  itemsAvailable_plural: '{count} مورد مانده',
  //itemsAvailable_plural_two: ...
  //itemsAvailable_plural_few: ...
  itemsFiltered_nil: 'هیچ موردی یافت نشد',
  itemsFiltered: '{count} مورد یافت شد',
  itemsFiltered_plural: '{count} مورد یافت شد',
  //itemsFiltered_plural_two: ...
  //itemsFiltered_plural_few: ...
  selectAll: 'انتخاب همه',
  deselectAll: 'حذف همه',
  search: 'جستجو',
  collapseGroup: 'Collapse Group',
  expandGroup: 'Expand Group',
  selectAllGroup: 'Select All Group',
  deselectAllGroup: 'Deselect All Group'
};

// link locales
$.uix.multiselect.i18n['fa_IR'] = $.uix.multiselect.i18n['fa'];
// ...

})(jQuery);
