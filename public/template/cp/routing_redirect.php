<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<?=SITE_URL; ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="template/cp/css/style.css" />
<title>در حال انتقال</title>
<script type="text/javascript">
setTimeout('redirect()',5000);
function redirect()
{
	window.location = '<?=$path;?>';
}
</script>
</head>

<body dir="rtl" style="background: #f0f0f0;">
<div align="center">
	<div class="box" style="margin-top: 100px; width: 500px;">
		<div class="title">پیغام سیستم</div>
		<div class="in" align="right">
			<p><b><?=$message;?></b></p>
			<p>اگر به صورت خودکار منتقل نشدید <a href="<?=$path;?>">اینجا کلیک کنید</a>.</p>
		</div>
	</div>
</div>
</body>
</html>