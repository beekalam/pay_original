jQuery(document).ready(function($){
    $('.loading').hide(0);
    
    $('#navC ul.nav').slimScroll({
        height: 'auto',
        alwaysVisible: true,
        width:'250px',
        wheelStep: 3,
        position: 'left',
        railVisible: true,
        railColor: '#222',/*
        size: '10px',
        color: '#ffcc00',
        distance: '20px',
        start: $('#child_image_element'),
        railOpacity: 0.3,
        allowPageScroll: false,
        disableFadeOut: false*/
    });
    
	$('ul.nav > li > a.menu').click(function(e){
        console.log( "in event");
        e.preventDefault();
        var flag = ($(this).hasClass('active'));
        $('ul.nav > li.active > ul').slideUp('fast');
        $('ul.nav > li > a.menu.active').removeClass('active');
        $('ul.nav > li.active').removeClass('active');
        if(!flag) {
            $(this).siblings('ul').slideToggle('fast');
            $(this).toggleClass('active');
            $(this).parent().toggleClass('active');
        }
        if($(this).parent().hasClass('active'))
            $.cookie('navigation',$(this).parent().index(),{expires:30,path:'/'});
        else
            $.cookie('navigation','',{expires:30,path:'/'});
        //$('#navC ul.nav').slimScroll({scrollBy: '10px'});
    });
    if($.cookie('navigation') != null && $.cookie('navigation') != '') {
        var temp = $('ul.nav > li:eq('+$.cookie('navigation')+')');
        if(temp.find('>a').hasClass('menu')) {
            temp.addClass('active');
            temp.find('>a').addClass('active');
            temp.find('>ul').css({display:'block'});
        }
    }
    
    $('.navBtn a').click(function(e){
        e.preventDefault();
        if(!$('body').hasClass('showMenu')) {
            $('.sidebar-container').animate({right:0});
            $('.content-container').animate({right:250},function(){
                $('.content-container,.sidebar-container').removeAttr('style');
            });
        } else {
            $('.content-container').animate({right:0});
            $('.sidebar-container').animate({right:-250},function(){
                $('.content-container,.sidebar-container').removeAttr('style');
            });
        }
        $('body').toggleClass('showMenu');
    });
    
    if ($.fn.button.noConflict != undefined) {
        $.fn.button.noConflict();
    }
    $.extend($.ui.multiselect, {
        locale: {
            addAll:'Ø§ÙØ²ÙˆØ¯Ù† Ù‡Ù…Ù‡',
            removeAll:'Ø­Ø°Ù Ù‡Ù…Ù‡',
            itemsCount:'Ù…ÙˆØ±Ø¯ Ø§Ù†ØªØ®Ø§Ø¨ Ø´Ø¯Ù‡'
        }
    });
    $('.multiselect').multiselect({
        searchable: false,
        height: 200,
        minWidth: 325,
    });
});

function inline_load(obj,e)
{
	$('#inline_load').slideUp('slow');
	ajax_link(obj,e,$('#content'),$('#inline_load'),false,"inline_load_callback()");
}

function inline_load_callback()
{
	$('#inline_load').slideDown('slow');
	
	$('<div class="dashed ajax_dashed">').appendTo(".title:not(:has(.dashed))");
	$('.ajax_dashed').each(function(){
		$(this).width($(this).parent().outerWidth() - 6);
	});
}

var ModalAction;
function ShowDeleteModal(action)
{
	ModalAction = action;
	$('#DeleteModal').modal();
	return false;
}

function ModalOkButton()
{
	if(ModalAction != '') {
		document.location = ModalAction;
	}
	return false;
}