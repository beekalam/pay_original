jQuery(document).ready(function($){
	$('#sidebar .menu ul li.drawer a').click(function(e){
		e.preventDefault();
		$(this).parent().siblings('li.drawer').removeClass('open').find('ul').slideUp('fast');
		$(this).parent().find('ul').slideToggle('fast');
		$(this).parent().toggleClass('open');
		$(this).blur();
		if($(this).parent().hasClass('open'))
			$.cookie('navigation',$(this).parent().index(),{expires:30,path:'/'});
		else
			$.cookie('navigation','',{expires:30,path:'/'});
	});
	if($.cookie('navigation') != null) {
		var temp = $('#sidebar .menu ul > li:eq('+$.cookie('navigation')+')');
		if(temp.hasClass('drawer')) {
			temp.find('ul').show(0);
			temp.addClass('open');
		}
	}
	$('#sidebar .menu ul li.drawer ul a').unbind('click');
});

function inline_load(obj,e)
{
	$('#inline_load').slideUp('slow');
	ajax_link(obj,e,$('#content'),$('#inline_load'),false,"inline_load_callback()");
}

function inline_load_callback()
{
	$('#inline_load').slideDown('slow');
	
	$('<div class="dashed ajax_dashed">').appendTo(".title:not(:has(.dashed))");
	$('.ajax_dashed').each(function(){
		$(this).width($(this).parent().outerWidth() - 6);
	});
}

var ModalAction;
function ShowDeleteModal(action)
{
	ModalAction = action;
	$('#DeleteModal').modal();
	return false;
}

function ModalOkButton()
{
	if(ModalAction != '') {
		document.location = ModalAction;
	}
	return false;
}

function showNews(a)
{
	$('#newsModal .modal-body').html('<p style="background:url(template/core/images/loading2.gif) no-repeat center"> صبر کنید ... </p>');
	//$('#newsModal').modal({remote:a.href});
	$('#newsModal .modal-header h3').html(a.innerHTML);
	$('#newsModal').removeData("modal");
	//return false;
}