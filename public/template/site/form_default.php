<?php require_once('form_functions.php'); ?>
<form class="form-horizontal" action="<?=$form_info['action']?>" method="<?=$form_info['method']?>"<?php if(isset($form_info['name'])){ ?> name="<?=$form_info['name']?>"
<?php } if(isset($form_info['id'])){ ?> id="<?=$form_info['id']?>"<?php } if(isset($form_info['enctype'])){ ?>  enctype="<?=$form_info['enctype']?>"<?php } if(isset($form_info['onSubmit'])){ ?> onSubmit="<?=$form_info['onSubmit']?>"<?php } ?>>
<?php if(isset($form_description)) { ?>
<div class="row help">
    <div class="col-xs-12">
        <p class="text-info">
            <?=$form_description;?>
        </p>
    </div>
</div>
<?php
}
$item_count = count($params) - 1;
$i = 0;
$buttons = $params['buttons'];
unset($params['buttons']);
foreach($params as $id => $item){
    $i++;
    if(isset($item['noform']) && $item['noform'])
        continue;
    $extra_ending = '';
    ?> 
    <div class="form-group<?php if(isset($data[$id]['result']) && !$data[$id]['result']){?> error<?php }?>">
        <?php if(!isset($formLayout)) {?>
        <label for="field_<?=$id?>" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label"><?=$item['title']?><?=(isset($item['required']) && $item['required'] === true)?' *':'';?></label>
        <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10<?=(isset($item['required']) && $item['required'] === true)?' isReq':'';?>">
            <div class="row">
                <div class="<?php if(in_array($item['field'],array('multiple_select'))){?>col-xs-12<?php }else{?>col-xs-5<?php }?>">
                    <?php make_obj($item, $id, $vars); ?>
                </div>
            </div>
            <div class="clearfix"></div>
        <?php } else { ?>
        <div class="col-xs-12<?=(isset($item['required']) && $item['required'] === true)?' isReq':'';?>">
            <?php make_obj($item, $id, $vars); ?>
        <?php } ?>
    <?php
    if(isset($data[$id]['result']) && !$data[$id]['result']){?>
    <div class="col-xs-12">
        <span class="text-danger"><?=$data[$id]['error']?></span>
    </div>
    <div class="clearfix"></div>
    <?php }
    if(isset($item['description'])) {?>
    <div class="col-xs-12">
        <span class="help-block"><?=$item['description']?></span>
    </div>
    <div class="clearfix"></div>
    <?php } 
    if(!empty($item['extra_ending']))
        echo $item['extra_ending'];
    ?>
        </div>
    </div>
<?php } ?>
    <p class="form-submit" align="center">
    <?php make_obj($buttons, 'buttons', $vars); ?>
    </p>
<?php if(isset($return_url)) { ?>
    <input type="hidden" name="return_url" value="<?=$return_url;?>" />
<?php }?>
</form>