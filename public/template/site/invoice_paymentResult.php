<?php $layout = 'layout_forms.php'; ?>
        <div class="row">
            <div class="col-xs-12">
                <?php if($invoice['status'] == 1) {?>
                <div class="alert alert-success">
                    پرداخت با موفقیت انجام شد.
                </div>
                <?php } else {?>
                <div class="alert alert-danger">
                    پرداخت انجام نشده است.
                </div>
                <?php }?>
                <article>
                    <h1 class="title">نتیجه پرداخت آنلاین</h1>
                    <p>مبلغ : <?=number_format($invoice['amount']/10);?> تومان</p>
                    <p>تاریخ : <?=Date::formatDate($invoice['date']);?></p>
                    <p>شناسه تراکنش : <?=$invoice['id'].'_'.$invoice['hash'];?> (جهت پیگیری های بعدی این کد را یادداشت کنید)</p><?php if($invoice['status'] == 1) {?>
                    <p>کد پیگیری بانک : <?=$invoice['bank_code'];?></p><?php } else {?>
                    <?php if(!empty($invoice['bank_ref'])) {?> 
                    <p>رفرنس بانک : <?=$invoice['bank_ref'];?></p><?php }?> 
                    <p>در صورتی که مبلغ از حساب شما کسر شده است، حداکثر طی 72 ساعت به حساب تان بر میگردد.</p>
                    <?php }?>
                </article>
            </div>
        </div>
