<!DOCTYPE html>
<html>
<head>
<base href="<?=SITE_URL;?>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title><?=get('site_page_title');?></title>
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap-rtl.min.css">
<link rel="stylesheet" type="text/css" href="template/site/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="template/site/plugins/layerslider/css/layerslider.css">
<link rel="stylesheet" type="text/css" href="template/site/css/style.css">
<link rel='shortcut icon' href='template/site/images/fav.png' type='image/x-icon' />
<link rel='icon' href='template/site/images/fav.png' type='image/x-icon' />
<link rel='apple-touch-icon' href='template/site/images/fav.png' />
<script type="text/javascript" src="template/site/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="template/site/plugins/layerslider/js/greensock.js"></script>
<script type="text/javascript" src="template/site/plugins/layerslider/js/layerslider.transitions.js"></script>
<script type="text/javascript" src="template/site/plugins/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<script type="text/javascript" src="template/site/js/script.js"></script>
<?php template_files(); ?>
</head>
<?php 
if(isset($sublayout)) {
    
} elseif($module == 'page' && $action == 'home')
    $sublayout = 'home';
else
    $sublayout = 'other';
?>
<body>
<div id="wrapper">
    <header>
        <div class="cont">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="logo">
                        <a href="./">Sabanovin</a>
                        <h1 class="tagline">ارائه دهنده درگاه واسط بانکی</h1>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <ul class="social-icons">
                        <li>
                            <a href="#" class="telegram">
                                <i class="fa fa-telegram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="instagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="facebook">
                                <i class="fa fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div id="contact-details">
                        <ul>
                            <li>
                                <i class="fa fa-envelope"></i>
                                support@sabanovin.com
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                +71 32922
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <nav>
                <ul>
                    <li>
                        <a href="./">صفحه اصلی</a>
                    </li>
                    <li>
                        <a href="user/signin">ورود</a>
                    </li>
                    <li>
                        <a href="user/signup">ثبت نام</a>
                    </li>
                    <li>
                        <a href="page/view/faq">پرسش های متداول</a>
                    </li>
                    <li>
                        <a href="page/view/about">درباره ما</a>
                    </li>
                    <li>
                        <a href="contact">تماس با ما</a>
                    </li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </div>
    </header>
    
    <div id="content">
        <?php
        include(TEMPLATE_PATH.'site/sublayout_'.$sublayout.'.php');
        ?>
    </div>
    <div id="footer">
        <div class="nav cont">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <h3><span>دسترسی سریع</span></h3>
                    <ul>
                        <li><a href="./">صفحه اصلی</a></li>
                        <li><a href="user/signin">ورود</a></li>
                        <li><a href="user/signup">عضویت</a></li>
                        <li><a href="contact">تماس با ما</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <h3><span>بیشتر بدانید</span></h3>
                    <ul>
                        <li><a href="page/view/about">درباره ما</a></li>
                        <li><a href="page/view/tos">قوانین</a></li>
                        <li><a href="page/view/faq">سوالات متداول</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <h3><span>اخبار</span></h3>
                    <ul>
                    <?php $news = get_news(4);
                    foreach($news as $_news) {?> 
                    <li><a href="news/view/<?=$_news['slug'];?>"><?=$_news['title'];?></a></li><?php }?> 
                    <li><a href="news/index">آرشیو اخبار</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <h3><span>ارتباط با ما</span></h3>
                    <p>تلفن تماس : <span class="important">07132922</span></p>
                    <p>ایمیل : <span class="important">info@domain.com</span></p>
                    <p>آدرس : فارس - شیراز - مشیر فاطمی - بن بستالف4 - پلاک71 -ط2 - واحد 4</p>
                </div>
            </div>
            <div class="split"></div>
            <div class="copy">
                <div class="row">
                    <div class="col-xs-6">
                        کلیه حقوق محفوظ است
                    </div>
                    <div class="col-xs-6 txt-l">
                        <?php show_cr(false); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
