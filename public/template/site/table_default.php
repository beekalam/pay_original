<?php if(isset($config['buttons'])){ ?>
<div class="row-fluid"><p>
	<?php foreach($config['buttons'] as $k => $v) {
		if(!is_array($v)) {
			$href = $base_url_for_links.$k;
			$title = $v;
		} else {
			$href = $v['url'];
			$title = $v['title'];
		}
		?>

	<a href="<?=$href;?>" class="btn btn-success"><?=$title;?></a>
<?php } ?>
</p></div>
<?php }?>
<?php if(!empty($data)) { ?>
<table class="table table-bordered table-striped" width="100%" cellpadding="0" cellspacing="0">
<?php 
$GLOBALS['no_body_in_widget'] = true;
$cols = count($config['cols']);
$colgroups = '';
$ths = '';
$align = array();
foreach($config['cols'] as $id => $c) {
	$width = (isset($c['width'])) ? ' width="'.$c['width'].'"' : '';
	$td[$id] = (isset($c['align'])) ? ' align="'.$c['align'].'"' : '';
	$td[$id] .= (isset($c['dir'])) ? ' dir="'.$c['dir'].'"' : '';
	$class = (isset($c['class'])) ? ' class="'.$c['class'].'"' : '';
	$colgroups .= "<colgroup".$width.$class."></colgroup>\n";
	$ths .= "<th>".@$c['title']."</th>\n";
}
$actions = '';
if(isset($config['actions']))
foreach($config['actions'] as $action)
{
	if(is_array($action))
	{
		$actions .= '<a href="'.((!isset($action['no_base']))?$base_url_for_links:'').$action['url'].'"'.@$action['extra'].'><img src="'.$action['image'].'" alt="'.$action['title'].'" title="'.$action['title'].'" /></a> ';
	}
	else
	{
		switch($action)
		{
			case 'increase':
				$actions .= '<a href="'.$base_url_for_links.'increase/[id]"><img src="template/core/images/increase.png" alt="increase" title="افزایش" /></a> ';
				break;
			case 'decrease':
				$actions .= '<a href="'.$base_url_for_links.'decrease/[id]"><img src="template/core/images/decrease.png" alt="decrease" title="کاهش" /></a> ';
				break;
			case 'delete':
				$actions .= '<a href="'.$base_url_for_links.'delete/[id]" onclick="return ShowDeleteModal(this.href);"><img src="template/core/images/delete.png" alt="delete" title="حذف" /></a> ';
				break;
			case 'trash':
				$actions .= '<a href="'.$base_url_for_links.'trash/'.$extra_param_prefix.'[id]'.$extra_param_suffix.'" onclick="return ShowDeleteModal(this.href);"><img src="template/core/images/delete.png" alt="delete" title="حذف" /></a> ';
				break;
			case 'edit':
				$actions .= '<a href="'.$base_url_for_links.'edit/[id]"><img src="template/core/images/edit.png" alt="edit" title="ویرایش" /></a> ';
				break;
			case 'report':
				$actions .= '<a href="'.$base_url_for_links.'report/[id]"><img src="template/core/images/report.png" alt="report" title="گزارشات" /></a> ';
				break;
			case 'view':
				$actions .= '<a href="'.$base_url_for_links.'view/[id]" target="_blank"><img src="template/core/images/view.png" alt="view" title="نمایش" /></a> ';
				break;
		}
	}
}
?>
	<?=$colgroups;?>
	<thead>
		<tr>
			<?=$ths;?>
		</tr>
	</thead>
	<tbody>
		<?php
		$tr_class = 'even';
		foreach($data as $i => $row){
			$tr_class = ($tr_class == 'odd') ? 'even' : 'odd';
		?>
		<tr>
			<?php foreach($config['cols'] as $id => $c){
				$title = $class = '';
				if(isset($c['type']))
				{
					switch($c['type'])
					{
						case 'status':
							$class = ' class="'. (($row[$id] == true) ? 'no-hover active' : 'no-hover deactive') . '"';
							$title = ' title="'. (($row[$id] == true) ? 'فعال' : 'غیر فعال') . '"';
							$row[$id] = '';
							break;
						
						case 'custom':
							$row[$id] = eval($c['value']);
							break;
						case 'list':
							if($c['list'] == 'lists')
								$c['list'] = $GLOBALS['lists'][$c['list_key']];
							$row[$id] = @$c['list'][$row[$id]];
							break;
						case 'date':
							load('date');
							if(isset($c['format']))
								$row[$id] = Date::formatDate($row[$id],$c['format']);
							else
								$row[$id] = Date::formatDate($row[$id]);
							break;
						case 'currency':
							$row[$id] = number_format($row[$id]);
							break;
						case 'link':
							$row[$id] = '<a href="'.str_replace('[id]',urlencode(@$row[$c['id']]),$c['url']).'">'.$row[$id].'</a>';
							break;
					}
				}
				
				if($id == 'actions')
				{
					$row[$id] = str_replace('[id]',$row['id'],$actions);
					$class = ' class="actions"';
				}
			?>
			<td<?=$class.$title;?><?=$td[$id];?>><?=@$c['prefix'].@$row[$id].@$c['suffix'];?></td>
			<?php } ?>
		</tr>
		<?php } ?>
	</tbody>
</table>
<?php if(!empty($pagination)) { ?>
<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<?=@$pagination;?>
</div>
<?php } ?>
<?php }else{ ?>
<br />
<p align="center">موردی یافت نشد</p>
<?php } ?>