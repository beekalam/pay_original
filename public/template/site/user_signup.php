<?php
//$sublayout = 'forms';
if($registered)
{
	?>
<p class="text-success">ثبت نام شما با موفقیت انجام شد.</p>
<p class="text-info">حساب کاربری شما پس از بررسی توسط مدیریت، تایید خواهد شد و میتوانید از امکانات سایت استفاده کنید.<br />برای ورود به حساب کاربری خود <a href="user/signin">اینجا</a> کلیک کنید.</p>
	<?php
}
else
	echo $form;
?>