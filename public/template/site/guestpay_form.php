<?php $layout = 'layout_forms.php'; ?>
        <div class="row">
            <?php if(isset($payError)) {?>
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    متاسفانه خطایی در مراحل پرداخت آنلاین رخ داده و قادر به ارتباط با سرور بانک نیستیم، لطفا بعدا تلاش کنید.
                </div>
            </div><?php }?>
            <div class="col-xs-6">
                <article>
                    <h1 class="title">پرداخت وجه برای <?=$user['name'];?></h1>
                    <?=$form;?>
                    <?php if($user['karmozd_type'] == 1) {?>
                    <hr/>
                    به مبلغ بالا <?=number_format(get('karmozd_guestpay')/10);?> تومان به عنوان کارمزد اضافه میشود.
                    <?php }?>
                </article>
            </div>
            <div class="col-xs-6">
                <article class="mgb">
                    <b>توضیحات دریافت کننده وجه :</b><br>
                    <?=nl2br($user['guestpay_description']);?>
                </article>
                <article>
                    <p>پس از پر کردن فرم روی دکمه ثبت کلیک کنید.</p>
                    <p>سپس وارد درگاه پرداخت بانکی میشوید و میتوانید با استفاده از شماره 16 رقمی کارت، کد CVV2، رمز دوم و تاریخ انقضای کارت عملیات پرداخت را انجام دهید.</p>
                    <p>توجه داشته باشید که پرداخت با کلیه کارت های عضو شبکه شتاب قابل انجام است.</p>
                </article>
            </div>
        </div>
