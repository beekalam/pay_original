<?php $layout = 'layout_forms.php'; ?>
        <div class="row">
            <div class="col-xs-12">
            </div>
            <div class="col-xs-6">
                <article>
                    <h1 class="title">پرداخت وجه برای <?=$user['name'];?></h1>
                    <p>مبلغ : <?=number_format($guestpay['amount']/10);?> تومان</p>
                    <p>تاریخ : <?=Date::formatDate($invoice['date']);?></p><?php if($invoice['status'] == 1) {?>
                    <p>کد پیگیری بانک : <?=$invoice['bank_code'];?></p><?php }?>
                </article>
            </div>
            <div class="col-xs-6">
                <?php if($invoice['status'] == 1) {?>
                <div class="alert alert-success">
                    پرداخت با موفقیت انجام شد.
                </div>
                <?php } else {?>
                <div class="alert alert-danger">
                    پرداخت انجام نشده است.
                </div>
                <?php }?>
                <article class="mgb">
                    <b>توضیحات دریافت کننده وجه :</b><br>
                    <?=nl2br($user['guestpay_description']);?>
                </article><?php if($invoice['status'] != 1) {?>
                <article>
                    <p>در صورتی که مبلغ از حساب شما کسر شده است، حداکثر طی 72 ساعت به حساب تان بر میگردد.</p>
                </article><?php }?>
            </div>
        </div>
