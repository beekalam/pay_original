jQuery(document).ready(function($){
    if($.fn.layerSlider)
        $('#slider').layerSlider({
            pauseOnHover: true,
            responsive: false,
            firstSlide: 1,
            skin: 'noskin',
            skinsPath: 'template/site/plugins/layerslider/skins/',
        });
});