<!DOCTYPE html>
<html>
<head>
<base href="<?=SITE_URL;?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Lang" content="fa">
<meta name="generator" content="G8Way.ir">
<title><?=$title_for_layout;?></title>
<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="template/bootstrap/css/bootstrap-rtl.min.css" />
<link rel="stylesheet" type="text/css" href="template/site/css/forms.css">
</head>
<body class="min-space">
    <div class="wrapper">
        <?=$content_for_layout;?>
        <hr />
        <footer><?=show_cr(false);?></footer>
    </div>
</body>
</html>
