<?php $layout = 'layout_forms.php'; ?>
        <div class="row">
            <div class="col-xs-6">
                <article>
                    <?=$form;?>
                </article>
            </div>
            <div class="col-xs-6">
                <article class="mgb">
                    <h3 class="text-center"><?=$data['title'];?></h3>
                    <hr />
                    <h3>قیمت : <?=number_format($data['amount']);?> ریال</h3>
                </article>
                <article>
                    <p>پس از پر کردن فرم روی دکمه ثبت کلیک کنید.</p>
                    <p>سپس وارد درگاه پرداخت بانکی میشوید و میتوانید با استفاده از شماره 16 رقمی کارت، کد CVV2، رمز دوم و تاریخ انقضای کارت عملیات پرداخت را انجام دهید.</p>
                    <p>توجه داشته باشید که پرداخت با کلیه کارت های عضو شبکه شتاب قابل انجام است.</p>
                </article>
            </div>
        </div>
