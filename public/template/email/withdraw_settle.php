<p><?=$user['name'];?> عزیز،</p>
<p>موجودی حساب شما تسویه شد.</p>
<p>&nbsp;</p>
<p>اطلاعات مبلغ واریزی به شرح ذیل میباشد :</p>
<p>تاریخ : <?=Date::formatDate('now');?></p>
<p>مبلغ واریزی : <?=$settle['value'];?></p>
<p>شناسه واریز : <?=$settle['track_number'];?></p>
<p>توضیحات : <?=$settle['description'];?></p>
<p>&nbsp;</p>
<p>برای ورود به سایت میتوانید از لینک زیر استفاده نمایید :</p>
<p><a href="<?=USERCP_URL;?>"><?=USERCP_URL;?></a></p>
