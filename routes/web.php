<?php

//------------------------- FRONT ----------------------------------------------------
Route::get("/", "FrontController@index")->name("front.index");
Route::get("/register", "FrontController@register")->name('front.register');
Route::post('/register_store', "FrontController@register_store")->name('front.register_store');

Route::get("/faq", "FrontController@faq")->name('front.faq');
Route::get("/aboutus", "FrontController@aboutus")->name('front.aboutus');
Route::get("/contactus", "FrontController@contactus")->name('front.contactus');


Route::middleware(['auth'])->group(function () {
//-------------------------  ADMIN /////////////////////////////////////////////////////////
    Route::get("/admin", "DashboardController@index")->name("admin.dashboard");
    Route::get('/admin/logout', "DashboardController@logout");
///////////////////////////////////////////////////////////////////////////////////
    Route::get("/bank", "BankController@index");
//------------------------  GATEWAY //////////////////////////////////////////////////////
    Route::get("/gateway", "GatewayController@index")->name('gateway.index');
    Route::get('/gateway/add', "GatewayController@add")->name('gateway.add');
    Route::post('/gateway/add_gateway', "GatewayController@add_gateway")->name("gateway.add_gateway");
    Route::post('/gateway/delete_gateway', "GatewayController@delete_gateway")->name("gateway.delete");
    Route::get('/gateway/edit/{id}', "GatewayController@edit")->name("gateway.edit");
    Route::post('/gateway/edit_gateway', "GatewayController@editGateway")->name("gateway.edit_gateway");
    Route::get('/gateway/setting', 'GatewayController@setting')->name('gateway.setting');
    Route::post('/gateway/update_setting', 'GatewayController@update_setting')->name('gateway.update_setting');
//------------------------ SETTINGS --------------------------------------------------
    Route::get("/settings", "SettingsController@edit")->name("settings.edit");
    Route::post("/settings/update", "SettingsController@update")->name("settings.update_site_settings");
    Route::get("/settings/system", "SettingsController@system")->name("settings.system");
    Route::post("/settings/system_settings_update", "SettingsController@systemSettingsUpdate")->name("settings.system_settings_update");
    Route::get('/settings/withdraw', 'SettingsController@withdraw')->name('settings.withdraw');
    Route::post('/settings/withdraw_settings_update', 'SettingsController@withdrawSettingsUpdate')->name('settings.withdraw_settings_update');
    Route::get('/settings/karmozd', 'SettingsController@karmozd')->name('settings.karmozd');
    Route::post('/settings/karmozd_settings_update', 'SettingsController@karmozdSettingsUpdate')->name('settings.karmozd_settings_update');
//------------------------ Terminals -------------------------------------------------
    Route::get('/terminals/index', 'TerminalController@index')->name('terminals.index');
    Route::get("/terminals", "TerminalController@add")->name("terminals.add");
    Route::post("/terminals/add_terminal", "TerminalController@addTerminal")->name("terminals.add_terminal");
//------------------------ Forms ----------------------------------------------------------
    Route::get('/forms/index', 'FormsController@index')->name('forms.index');
    Route::get('/forms/forms', 'FormsController@forms')->name('forms.forms');
    Route::get('/forms/add', 'FormsController@add')->name('forms.add');
    Route::delete('/forms/{id}','FormsController@delete')->name('forms.delete');
    Route::post('/forms/store', 'FormsController@store')->name('forms.store');
    Route::get('/forms/paid', 'FormsController@paid')->name('forms.paid');
    Route::get('/forms/viewdata/{id}', 'FormsController@viewdata')->name('forms.viewdata');
   
//-----------------------  Links ------------------------------------------------------------
    Route::get('/links/index', 'LinksController@index')->name('links.index');
    Route::get('/links/add', 'LinksController@add')->name('links.add');
    Route::post('/links/store', 'LinksController@store')->name('links.store');
    Route::get('/links/pay/{pay_id}', 'LinksController@pay')->name('links.pay');
//-----------------------  Pages -----------------------------------------------------------
    Route::get('/page/index', 'PagesController@index')->name('page.index');
    Route::get('/page/add', 'PagesController@add')->name('page.add');
    Route::post('/page/store', 'PagesController@store')->name('page.store');
//------------------------ News -------------------------------------------------------------
    Route::get('/news/index', 'NewsController@index')->name('news.index');
    Route::get('/news/add', 'NewsController@add')->name('news.add');
    Route::post('/news/store', 'NewsController@store')->name('news.store');
//------------------------ Notifications -------------------------------------------------------
    Route::get('/notifications/index', 'NotificationsController@index')->name('notifications.index');
    Route::get('/notifications/add', 'NotificationsController@add')->name('notifications.add');
    Route::post('/notifications/store', 'NotificationsController@store')->name('notifications.store');
//------------------------ Ticket ----------------------------------------------------------
    Route::get('/ticket', 'TicketController@index')->name('tickets.index');
    Route::get('/ticket/create', 'TicketController@create')->name('tickets.create');
    Route::post('/ticket', 'TicketController@store')->name('tickets.store');
    Route::get('/ticket/show/{id}', 'TicketController@create')->name('tickets.show');
//------------------------ User ---------------------------------------------------------------------
    Route::get('/user/profile','UserController@profile')->name('user.profile');
    Route::post('/user/profile','UserController@updateProfile')->name('user.update_profile');
    Route::get('/user/change_password','UserController@showChangePasswordForm')->name('user.change_password');
    Route::post('/user/change_password','UserController@changePassword')->name('user.store_password');
});

//------------------------ Test ---------------------------------------------------------------------
// Route::get('/test', 'TestController@index');

//------------------------ MISC ---------------------------------------------------------------------
Route::get('/misc/state_cities/','MiscController@stateCities')->name('misc.state_cities');


Route::get('/forms/pay/{pay_id}', 'FormsController@pay')->name('forms.pay');
Route::post('/forms/pay/{pay_id}', 'FormsController@pay_final')->name('forms.pay_final');
Route::any('/invoice/process_transaction/{key}','InvoiceController@process_transaction')->name('invoice.process_transaction');
Auth::routes();
// Route::get('/', function () {
//     return redirect("/admin");
// return view('welcome');
// });


//Route::get('/home', 'HomeController@index')->name('home');
