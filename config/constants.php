<?php
return [
    'options' => [
        'footer_link' => '<div id="script_license">قدرت گرفته از <a href="http://sabanovin.com" target="_blank">Sabanovin.com</a></div>',
        'footer_link_center' => '<div align="center" id="script_license">قدرت گرفته از <a href="http://sabanovin.com" target="_blank">Sabanovin.com</a></div>',
        'password_validation_requirements' => 'required|string|min:6|confirmed',
        'user_document_folder' => 'user_docs',
    ],
    'INVOICE_PAY_TYPE' => [
        'API' => '1',
        'LINK' => '2',
        'FORM' => '3'
    ],
    'INVOICE_STATUS' =>[
        'PENDING' => '0',
        'PAIED' => '1',
        'ERROR' => '2'
    ],
];