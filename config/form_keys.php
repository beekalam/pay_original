<?php
return [
    'name' => array(
        'type' => 'text',
        'field' => 'textbox',
        'title' => 'نام و نام خانوادگی',
        'validation' => 'required|max:50',
    ),
    'domain' => array(
        'type' => 'domain',
        'field' => 'textbox',
        'title' => 'دامین',
        'validation' => 'required',
    ),
    'email' => array(
        'type' => 'email',
        'field' => 'textbox',
        'title' => 'ایمیل',
        'validation' => 'required',
    ),
    'mobile' => array(
        'type' => 'mobile',
        'field' => 'textbox',
        'title' => 'موبایل',
        'validation' => 'required',
    ),
    'phone' => array(
        'type' => 'text',
        'field' => 'phone',
        'title' => 'تلفن',
        'validation' => 'required',
    ),
    'address' => array(
        'type' => 'text',
        'field' => 'textarea',
        'title' => 'آدرس پستی',
        'validation' => 'required|max:200',
    ),
    'zipcode' => array(
        'type' => 'zipcode',
        'field' => 'textbox',
        'title' => 'کد پستی',
        'validation' => 'required',
    ),
    'prod_id' => array(
        'type' => 'text',
        'field' => 'textbox',
        'title' => 'شناسه محصول',
        'validation' => 'required|max:50',
    ),
    'code' => array(
        'type' => 'text',
        'field' => 'textbox',
        'title' => 'کد پیگیری',
        'validation' => 'required|max:50',
    ),
    'description' => array(
        'type' => 'text',
        'field' => 'textarea',
        'title' => 'توضیحات',
        'validaion' => 'nullable|max:200',
    ),
];
