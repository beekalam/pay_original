   <?php if(isset($system_message)){ ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>پیغام سیستم : </strong>
            <?=$system_message;?>
        </div>
        <?php } ?>
        <?php if(isset($error)) { ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>خطا : </strong>
            <?=$error;?>
        </div><?php } ?>
        <?php if(isset($no_layout)) {
            echo $content_for_layout;
        } else { ?>
        <div class="box">
            <div class="title"><?=$title_for_layout;?></div>
            <div class="in">
                <?=$content_for_layout;?>
            </div>
        </div>
        <?php } ?>
        <div id="inline_load" style="display: none;"></div>
        <?php if(isset($page_description)) { ?>
        <div class="box mgt">
            <div class="in">
                <p class="text-info">
                    <?=$page_description;?>
                </p>
            </div>
        </div><?php } ?>