@if (Session::has('message'))
    @php
        $message = $type = '';
        list($type,$message) = explode('|',Session::get('message'));
        if(isset($type) && $type=='error') 
            $type='danger';
    @endphp
    <div class="alert alert-dismissible alert-{{ $type }}">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <ul>
            <li>{{ $message }} </li>
        </ul>
    </div>
@endif