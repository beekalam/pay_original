<ul class="nav">
    @php
        $is_admin = Auth::user()->isAdmin();
    @endphp


    <li>
        <a href="{{ route('admin.dashboard') }}">
            <i class="glyphicon glyphicon-dashboard">
                <b></b>
            </i>
            <span>داشبورد</span>
        </a>
    </li>
 

    {{-- notifications --}}
    @if($is_admin)
    <li>
        <a href="#" class="menu">
            <i class="glyphicon glyphicon-bullhorn">
                <b></b>
            </i>
            <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
            <span>اطلاع رسانی</span>
        </a>
        <ul>
            <li>
                <a href="{{ route("notifications.index") }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    فهرست اطلاعیه های عمومی
                </a>
            </li>
            <li>
                <a href="{{ route('notifications.add') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    ارسال اطلاعیه عمومی جدید
                </a>
            </li>
        </ul>
    </li>
    @else
    <li>
        <a href="{{ route('notifications.index') }}">
                <i class="glyphicon glyphicon-bullhorn">
                    <b></b>
                </i>
                <span>آرشیو اطلاعیه ها</span>
        </a>
    </li>
    @endif

    @if(!$is_admin)
    <li>
            <a href="#" class="menu">
                <i class="glyphicon glyphicon-headphones">
                    <b></b>
                </i>
                <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
                <span>پشتیبانی</span>
            </a>
            <ul>
                <li>
                    <a href="{{ route('tickets.index') }}">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        فهرست تیکت ها
                    </a>
                </li>
                <li>
                    <a href="{{ route('tickets.create') }}">
                        <i class="glyphicon glyphicon-menu-left"></i>
                        ایجاد تیکت جدید
                    </a>
                </li>
            </ul>
        </li>
    @endif

    {{-- gateways --}}
    @if($is_admin)
    <li>
        <a href="#" class="menu">
            <i class="glyphicon glyphicon-road">
                <b></b>
            </i>
            <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
            <span>درگاه های بانکی</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('gateway.index') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    فهرست درگاه های بانکی
                </a>
            </li>
            <li>
                <a href="{{ route('gateway.add') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    افزودن درگاه بانکی
                </a>
            </li>
            <li>
                <a href="{{ route('gateway.setting') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    درگاه پیشفرض
                </a>
            </li>
        </ul>
    </li>
    @endif

    <li>
        <a href="#" class="menu">
            <i class="glyphicon glyphicon-sort">
                <b></b>
            </i>
            <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
            <span>مدیریت ترمینال ها</span>
        </a>
        <ul>
            @if($is_admin)
            <li>
                <a href="">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    ترمینال های در انتظار تایید
                </a>
            </li>
            @endif
            <li>
                <a href="{{ route('terminals.index') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    فهرست ترمینال ها
                </a>
            </li>
            <li>
                <a href="{{ route('terminals.add') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    افزودن ترمینال جدید
                </a>
            </li>
        </ul>
    </li>
    
    {{-- links --}}
    <li>
        <a href="#" class="menu">
            <i class="glyphicon glyphicon-link">
                <b></b>
            </i>
            <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
            <span>مدیریت لینک ها</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('links.index') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    فهرست لینک های پرداخت
                </a>
            </li>
            <li>
                <a href="{{ route('links.add') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    ایجاد لینک پرداخت جدید
                </a>
            </li>
        </ul>
    </li>
    

    {{-- content --}}
    @if($is_admin)
    <li>
        <a href="#" class="menu">
            <i class="glyphicon glyphicon-comment">
                <b></b>
            </i>
            <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
            <span>مدیریت محتوا</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('page.index') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    فهرست صفحات ثابت
                </a>
            </li>
            <li>
                <a href="{{ route('page.add') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    افزودن صفحه جدید
                </a>
            </li>
            <li>
                <a href="{{ route('news.index') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    فهرست اخبار
                </a>
            </li>
            <li>
                <a href="{{ route('news.add') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    افزودن خبر جدید
                </a>
            </li>
        </ul>
    </li>
    @endif

    {{-- forms--}}
    <li>
        <a href="#" class="menu">
            <i class="glyphicon glyphicon-check">
                <b></b>
            </i>
            <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
            <span>مدیریت فرم ها</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('forms.forms') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    فهرست فرم های پرداخت
                </a>
            </li>
            <li>
                <a href="{{ route('forms.add') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    ایجاد فرم پرداخت جدید
                </a>
            </li>
            <li>
                <a href="{{ route('forms.index') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    فرم های ثبت شده
                </a>
            </li>
        </ul>
    </li>

    {{--  settings  --}}
    @if($is_admin)
    <li>
        <a href="#" class="menu">
            <i class="glyphicon glyphicon-cog">
                <b></b>
            </i>
            <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
            <span>تنظیمات</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('settings.edit') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    تنظیمات اصلی سایت
                </a>
            </li>
            <li>
                <a href="{{ route('settings.system') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    تنظیمات مدیریتی
                </a>
            </li>
            <li>
                <a href="{{ route('settings.withdraw') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    تنظیمات تسویه حساب
                </a>
            </li>
            <li>
                <a href="{{ route('settings.karmozd') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    تنظیمات کارمزد
                </a>
            </li>
            <li>
                <a href="/setting/notifs">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    تنظیمات اطلاع رسانی
                </a>
            </li>
            <li>
                <a href="/setting/edit/sms">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    تنظیمات SMS
                </a>
            </li>
            <li>
                <a href="/admin/config">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    اطلاعات ورود
                </a>
            </li>
        </ul>
    </li>
    @else
     <li>
        <a href="#" class="menu">
            <i class="glyphicon glyphicon-cog">
                <b></b>
            </i>
            <span class="pull-left">
                    <i class="glyphicon glyphicon-menu-down arrow"></i>
                    <i class="glyphicon glyphicon-menu-up arrow-active"></i>
                </span>
            <span>تنظیمات</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('user.profile') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    ویرایش اطلاعات کاربر
                </a>
            </li>
            <li>
                <a href="{{ route('user.change_password') }}">
                    <i class="glyphicon glyphicon-menu-left"></i>
                    تغییر کلمه عبور
                </a>
            </li>
        </ul>
    </li>
    @endif
</ul>