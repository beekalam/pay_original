@extends('layouts.app')
@section('content')
<div class="box">
    <div class="title">ویرایش و تکمیل اطلاعات پروفایل</div>
    <div class="in">
        <form class="form-horizontal" action="{{ route('user.update_profile') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row-fluid help">
                <div class="span12">
                    <p class="text-info">
                        این اطلاعات نزد سایت محفوظ بوده و به هیچ وجه در اختیار شخص یا گروهی قرار نخواهد گرفت. </p>
                </div>
            </div>

            <div class="form-group">
                <label for="field_name" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">نام و نام خانوادگی *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="name" id="field_name" maxlength="50" value="{{ $user->name }}"
                                class="validate[required,maxSize[50]] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="form-group">
                <label for="field_father" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">نام پدر *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="father" id="field_father" maxlength="30" value="{{ $user->father }}"
                                class="validate[required,maxSize[30]] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="form-group">
                <label for="field_melli_scan" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تصویر کارت ملی
                    *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="file" name="melli_scan" id="field_melli_scan" class="" style="display:none">
                            <div class="input-group">
                                <input id="alt_field_melli_scan" class="form-control" type="text" onclick="$('input[id=field_melli_scan]').click();">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" onclick="$('input[id=field_melli_scan]').click();">انتخاب
                                        تصویر</button>
                                </span>
                            </div>

                            <script type="text/javascript">
                                $('input[id=field_melli_scan]').change(function () {
                                    $('#alt_field_melli_scan').val($(this).val());
                                });
                            </script>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <p class="help-block">فقط با فرمت jpg</p>
                    <div class="clearfix"></div>

                </div>
            </div>


            <div class="form-group">
                <label for="field_shenasname_scan" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تصویر
                    شناسنامه *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="file" name="shenasname_scan" id="field_shenasname_scan" class="" style="display:none">
                            <div class="input-group">
                                <input id="alt_field_shenasname_scan" class="form-control" type="text" onclick="$('input[id=field_shenasname_scan]').click();">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" onclick="$('input[id=field_shenasname_scan]').click();">انتخاب
                                        تصویر</button>
                                </span>
                            </div>

                            <script type="text/javascript">
                                $('input[id=field_shenasname_scan]').change(function () {
                                    $('#alt_field_shenasname_scan').val($(this).val());
                                });
                            </script>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <p class="help-block">فقط با فرمت jpg</p>
                    <div class="clearfix"></div>

                </div>
            </div>


            <div class="form-group">
                <label for="field_birthday" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تاریخ تولد *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <div style="width: 30%; float: right;">
                                <select id="f_year" style="width: 100%; padding: 0 6px; border-left: 0px; border-radius: 0 3px 3px 0;"
                                    class="form-control">
                                    <option>سال</option>
                                    <option value="1394">1394</option>
                                    <option value="1393">1393</option>
                                    <option value="1392">1392</option>
                                    <option value="1391">1391</option>
                                    <option value="1390">1390</option>
                                    <option value="1389">1389</option>
                                    <option value="1388">1388</option>
                                    <option value="1387">1387</option>
                                    <option value="1386">1386</option>
                                    <option value="1385">1385</option>
                                    <option value="1384">1384</option>
                                    <option value="1383">1383</option>
                                    <option value="1382">1382</option>
                                    <option value="1381">1381</option>
                                    <option value="1380">1380</option>
                                    <option value="1379">1379</option>
                                    <option value="1378">1378</option>
                                    <option value="1377">1377</option>
                                    <option value="1376">1376</option>
                                    <option value="1375">1375</option>
                                    <option value="1374">1374</option>
                                    <option value="1373">1373</option>
                                    <option value="1372">1372</option>
                                    <option value="1371">1371</option>
                                    <option value="1370">1370</option>
                                    <option value="1369">1369</option>
                                    <option value="1368">1368</option>
                                    <option value="1367">1367</option>
                                    <option value="1366">1366</option>
                                    <option value="1365">1365</option>
                                    <option value="1364">1364</option>
                                    <option value="1363">1363</option>
                                    <option value="1362">1362</option>
                                    <option value="1361">1361</option>
                                    <option value="1360">1360</option>
                                    <option value="1359">1359</option>
                                    <option value="1358">1358</option>
                                    <option value="1357">1357</option>
                                    <option value="1356">1356</option>
                                    <option value="1355">1355</option>
                                    <option value="1354">1354</option>
                                    <option value="1353">1353</option>
                                    <option value="1352">1352</option>
                                    <option value="1351">1351</option>
                                    <option value="1350">1350</option>
                                    <option value="1349">1349</option>
                                    <option value="1348">1348</option>
                                    <option value="1347">1347</option>
                                    <option value="1346">1346</option>
                                    <option value="1345">1345</option>
                                    <option value="1344">1344</option>
                                    <option value="1343">1343</option>
                                    <option value="1342">1342</option>
                                    <option value="1341">1341</option>
                                    <option value="1340">1340</option>
                                    <option value="1339">1339</option>
                                    <option value="1338">1338</option>
                                    <option value="1337">1337</option>
                                    <option value="1336">1336</option>
                                    <option value="1335">1335</option>
                                    <option value="1334">1334</option>
                                    <option value="1333">1333</option>
                                    <option value="1332">1332</option>
                                    <option value="1331">1331</option>
                                    <option value="1330">1330</option>
                                    <option value="1329">1329</option>
                                    <option value="1328">1328</option>
                                    <option value="1327">1327</option>
                                    <option value="1326">1326</option>
                                    <option value="1325">1325</option>
                                    <option value="1324">1324</option>
                                    <option value="1323">1323</option>
                                    <option value="1322">1322</option>
                                    <option value="1321">1321</option>
                                    <option value="1320">1320</option>
                                    <option value="1319">1319</option>
                                    <option value="1318">1318</option>
                                    <option value="1317">1317</option>
                                    <option value="1316">1316</option>
                                    <option value="1315">1315</option>
                                    <option value="1314">1314</option>
                                    <option value="1313">1313</option>
                                    <option value="1312">1312</option>
                                    <option value="1311">1311</option>
                                    <option value="1310">1310</option>
                                    <option value="1309">1309</option>
                                    <option value="1308">1308</option>
                                    <option value="1307">1307</option>
                                    <option value="1306">1306</option>
                                    <option value="1305">1305</option>
                                    <option value="1304">1304</option>
                                    <option value="1303">1303</option>
                                    <option value="1302">1302</option>
                                    <option value="1301">1301</option>
                                    <option value="1300">1300</option>
                                </select>
                            </div>
                            <div style="width: 45%;  float: right;">
                                <select id="f_month" style="width: 100%; padding: 0 6px; border-left: 0px; border-radius: 0"
                                    class="form-control">
                                    <option value="1">فروردین</option>
                                    <option value="2">اردیبهشت</option>
                                    <option value="3">خرداد</option>
                                    <option value="4">تیر</option>
                                    <option value="5">مرداد</option>
                                    <option value="6">شهریور</option>
                                    <option value="7">مهر</option>
                                    <option value="8">آبان</option>
                                    <option value="9">آذر</option>
                                    <option value="10">دی</option>
                                    <option value="11">بهمن</option>
                                    <option value="12">اسفند</option>
                                </select>
                            </div>
                            <div style="width: 25%;  float: right;">
                                <select id="f_day" style="width: 100%; padding: 0 6px; border-radius: 3px 0 0 3px"
                                    class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <input type="hidden" name="birthday" id="field_birthday" value="{{ $user->birthday }}">
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var v = $("#field_birthday").val();
                                    console.log("v: " + v);
                                    console.log($("#field_birthday").attr("value"));
                                    if (v != "") {
                                        v = v.split('-');
                                        v[0] && $("#f_year").val(v[0]);
                                        v[1] && $("#f_month").val(v[1]);
                                        v[2] && $("#f_day").val(v[2]);
                                    }
                                    $('#f_year,#f_month,#f_day').change(function () {
                                        $('#field_birthday').val($('#f_year').val() + '-' + $(
                                            '#f_month').val() + '-' + $('#f_day').val());
                                    });
                                    // $('#f_year').change();

                                });
                            </script>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="form-group">
                <label for="field_sh_melli" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">شماره ملی *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="sh_melli" id="field_sh_melli" dir="ltr" value="{{ $user->sh_melli }}"
                                class="validate[required] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="form-group">
                <label for="field_state_id" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">استان *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <select name="state_id" id="field_state_id"  class="form-control">
                                <option value="">لطفا استان خود را انتخاب کنید</option>
                                @foreach($states as $s)
                                <option value="{{ $s->id }}" @if($s->id == $user->state_id) selected="selected" @endif>
                                    {{ $s->name }}
                                </option>
                                @endforeach
                            </select>
                            <div class="cb"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <script>
                        $(document).ready(function(){
                            $('#field_state_id').on('change', function() {
                                var stateID = $(this).val();
                                console.log("......");
                                console.log(stateID);
                                if(stateID) {
                                    $.ajax({
                                        url: "{{ route('misc.state_cities') }}?state_id="+stateID,
                                        type: "GET",
                                        dataType: "json",
                                        success:function(data) {
                                            $('select[name="city_id"]').empty();
                                            $('select[name="city_id"]').append('<option value="">لطفا شهر خود را انتخاب کنید</option>');
                                            $.each(data, function(key, value) {
                                                $('select[name="city_id"]').append('<option value="'+ value +'">'+ key +'</option>');
                                            });
                                        }
                                    });
                                }else{
                                    $('select[name="city_id"]').empty();
                                }
                            });
                        });
                    </script>
                </div>
            </div>


            <div class="form-group">
                <label for="field_city_id" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">شهر *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <select name="city_id" id="field_city_id" class="form-control">
                                <option value="">لطفا شهر خود را انتخاب کنید</option>
                                @foreach($cities as $id=>$name)
                                    <option value="{{ $id }}" @if($user->city_id == $id) selected='selected' @endif>
                                        {{ $name }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="cb"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="form-group">
                <label for="field_address" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">آدرس *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <textarea name="address" id="field_address" cols="40" rows="4" class="validate[required,maxSize[250]] form-control">{{ $user->address }}</textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <p class="help-block">آدرس کامل پستی</p>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="form-group">
                <label for="field_zipcode" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کد پستی *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="zipcode" id="field_zipcode" dir="ltr" value="{{ $user->zipcode }}"
                                class="validate[required] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <p class="help-block">کد پستی 10 رقمی</p>
                    <div class="clearfix"></div>
                </div>
            </div>

            <p class="form-submit" align="center">
                <button type="submit" class="btn btn-success ">
                    <span>ثبت</span>
                </button> &nbsp;&nbsp;
            </p>

        </form>
        {{-- <script type="text/javascript" src="/template/core/cities.js"></script> --}}
    </div>
</div>
@endsection