@extends('layouts.app')
@section('content')
<div class="box">
    <div class="title">تغییر کلمه عبور</div>
    <div class="in">
        <form class="form-horizontal" action="{{ route('user.change_password') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                <label for="field_current_password" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کلمه عبور
                    فعلی *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="password" name="current_password" id="field_current_password" dir="ltr" 
                                class="validate[required] form-control" required>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <p class="help-block">برای تغییر کلمه عبور وارد کردن کلمه عبور فعلی الزامیست</p>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="form-group">
                <label for="field_password" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کلمه عبور جدید *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="password" name="new_password" id="field_password" dir="ltr" value="" 
                                class="validate[required,minSize[3]] form-control" required>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="form-group">
                <label for="field_re_password" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تکرار کلمه
                    عبور جدید *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="password" name="new_password_confirmation" id="field_re_password" dir="ltr" value="" 
                            class="validate[required] form-control" required>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <p class="help-block">عینا کلمه عبوری که در بالا وارد کرده اید را وارد کنید</p>
                    <div class="clearfix"></div>
                </div>
            </div>

            <p class="form-submit" align="center">
                <button type="submit" class="btn btn-success ">
                    <span>ثبت تغییرات</span>
                </button> &nbsp;&nbsp;
            </p>

        </form>
    </div>
</div>
@endsection