<html>

<head>
    {{--
    <base href="http://localhost/pay_sabanovin_local/"> --}}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Lang" content="fa">
    <meta name="generator" content="sabanovin.com">
    <title>پرداخت تست</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('template/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/bootstrap/css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/site/css/forms.css') }}">
    <script type="text/javascript" src="{{ asset('template/bootstrap/js/bootstrap.min.js') }}"></script>
</head>

<body class="min-space">
    <div class="wrapper">
        <div class="row">
            <div class="col-xs-6">
                <article>
                    <form class="form-horizontal" action="{{ route("forms.pay_final",['id' => $form_key])}}" method="post">
                        {{ csrf_field() }}
                        @foreach($form->fields_array() as $f)
                        <div class="form-group {{ $errors->has($f) ? 'has-error':'' }}">
                            <div class="col-xs-12 isReq">
                                <input type="text" name="{{ $f }}" id="field_{{ $f }}" placeholder="{{ $pay_fields[$f]['title'] }}"
                                    maxlength="50" value='{{ old("$f") }}' class="form-control">

                                @if($errors->has($f))
                                <span class="invalid-feedback has-error" role="alert">
                                    <strong>{{ $errors->first($f) }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @endforeach

                        <p class="form-submit" align="center">
                            <button type="submit" class="btn btn-success ">
                                <span>ثبت و پرداخت</span>
                            </button>
                        </p>
                    </form>
                </article>
            </div>
            <div class="col-xs-6">
                <article class="mgb">
                    <h3 class="text-center">
                        {{$form->title}}
                    </h3>
                    <hr>
                    <h3>قیمت :
                        {{number_format($form->amount)}}
                        تومان</h3>
                </article>
                <article>
                    <p>پس از پر کردن فرم روی دکمه ثبت کلیک کنید.</p>
                    <p>سپس وارد درگاه پرداخت بانکی میشوید و میتوانید با استفاده از شماره 16 رقمی کارت، کد CVV2، رمز دوم
                        و
                        تاریخ انقضای کارت عملیات پرداخت را انجام دهید.</p>
                    <p>توجه داشته باشید که پرداخت با کلیه کارت های عضو شبکه شتاب قابل انجام است.</p>
                </article>
            </div>
        </div>
        <hr>
        <footer>
            {!! Config::get('constants.options.footer_link') !!}
        </footer>
    </div>


</body>

</html>