<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title>{{ $settings['site_page_title'] }}</title>
<link rel="stylesheet" type="text/css" href="{{ asset('template/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/bootstrap/css/bootstrap-rtl.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/site/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/site/plugins/layerslider/css/layerslider.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('template/site/css/style.css') }}">
<link rel='shortcut icon' href="{{ asset('template/site/images/fav.png')}}" type='image/x-icon')  />
<link rel='icon' href="{{ asset('template/site/images/fav.png') }} type='image/x-icon' />
<link rel='apple-touch-icon' href="{{ asset('template/site/images/fav.png') }}" />
<script type="text/javascript" src="{{ asset('template/site/js/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/site/plugins/layerslider/js/greensock.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/site/plugins/layerslider/js/layerslider.transitions.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/site/plugins/layerslider/js/layerslider.kreaturamedia.jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/site/js/script.js') }}"></script>

</head>

<body>
<div id="wrapper">
    <header>
        <div class="cont">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="logo">
                        <a href="./">Sabapay</a>
                        <h1 class="tagline">ارائه دهنده درگاه واسط بانکی</h1>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <ul class="social-icons">
                        <li>
                            <a href="#" class="telegram">
                                <i class="fa fa-telegram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="instagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="facebook">
                                <i class="fa fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div id="contact-details">
                        <ul>
                            <li>
                                <i class="fa fa-envelope"></i>
                                {{ $settings['site_contact_email'] }}
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                +71 32922
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <nav>
                <ul>
                    <li>
                        <a href="{{ route('front.index') }}">صفحه اصلی</a>
                    </li>
                    <li>
                        @guest
                        <a href="{{ route('login') }}">ورود</a>
                        @else
                        <a href="{{ route('admin.dashboard') }}">ورود</a>
                        @endguest
                    </li>
                    <li>
                        @guest
                        <a href="{{ route('register') }}">ثبت نام</a>
                        @else
                        <a href="{{ route('admin.dashboard') }}">ثبت نام</a>
                        @endguest
                    </li>
                    <li>
                        <a href="{{ route('front.faq') }}">پرسش های متداول</a>
                    </li>
                    <li>
                        <a href="{{ route('front.aboutus') }}">درباره ما</a>
                    </li>
                    <li>
                        <a href="{{ route('front.contactus') }}">تماس با ما</a>
                    </li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </div>
    </header>
    
    <div id="content">
        @yield('content')
    </div>
    <div id="footer">
        <div class="nav cont">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <h3><span>دسترسی سریع</span></h3>
                    <ul>
                        <li><a href="/">صفحه اصلی</a></li>
                        <li><a href="/login">ورود</a></li>
                        <li><a href="/register">عضویت</a></li>
                        <li><a href="/contactus">تماس با ما</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <h3><span>بیشتر بدانید</span></h3>
                    <ul>
                        <li><a href="/aboutus">درباره ما</a></li>
                        <li><a href="/tos">قوانین</a></li>
                        <li><a href="/faq">سوالات متداول</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <h3><span>اخبار</span></h3>
                    <ul>
                    <?php $news = [];
                    foreach($news as $_news) {?> 
                    <li><a href="news/view/<?=$_news['slug'];?>"><?=$_news['title'];?></a></li><?php }?> 
                    <li><a href="news/index">آرشیو اخبار</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <h3><span>ارتباط با ما</span></h3>
                    <p>تلفن تماس : <span class="important"></span></p>
                    <p>ایمیل : <span class="important">{{ $settings['site_contact_email'] }}</span></p>
                    <p>آدرس : فارس - شیراز - مشیر فاطمی - بن بستالف4 - پلاک71 -ط2 - واحد 4</p>
                </div>
            </div>
            <div class="split"></div>
            <div class="copy">
                <div class="row">
                    <div class="col-xs-6">
                        کلیه حقوق محفوظ است
                    </div>
                    <div class="col-xs-6 txt-l">
                        {!! Config::get('constants.options.footer_link') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
