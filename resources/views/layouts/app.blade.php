<!DOCTYPE html>
<html>

<head>
    <base href="<?php //SITE_URL; ?>" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="developer" content="sabanovin.com">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/cp/plugins/multiselect/css/ui.multiselect.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('template/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('template/bootstrap/css/bootstrap-rtl.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('template/cp/css/style.css') }}" />
    @yield('styles')
    <script type="text/javascript" src="{{ asset('template/cp/js/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/cp/js/ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/cp/plugins/datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/cp/plugins/datepicker/bootstrap-datepicker.fa.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/cp/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/core/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/core/ckeditor/config.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/cp/plugins/globalize/globalize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/cp/plugins/DevExpressChartJS/dx.chartjs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/cp/plugins/multiselect/js/ui.multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/core/core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/cp/js/script.js') }}"></script>
    <?php //{{ asset('template_files(); ?>
    <link rel="stylesheet" type="text/css" href="{{ asset('template/cp/plugins/jqueryui/all/themes/base/jquery.ui.all.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('template/cp/plugins/datepicker/bootstrap-datepicker.min.css') }}" />

    <title>
        <?php echo isset($title) ? $title : "";?>
    </title>
</head>

<body class="preload min-space">
    <div class="loading"><span>در حال بارگذاری ...</span></div>

    <div class="sidebar-container">
        <div class="head">
            <img src="{{asset('/img/logo.png') }}" alt="sabanovin" class='img' />
        </div>
        <div id="navC">
            @include("_partials.menu")
        </div>
        <div class="foot">
            پنل مدیریت
        </div>
    </div>
    <div class="content-container">
        <div class="head">
            <div class="profile">
                <span>{{ Auth::user()->name }}</span>
            </div>
            <div class="signout">
                <a href="{{ url('/admin/logout') }}">
                    <i class="glyphicon glyphicon-remove-sign"></i>
                    <span>&nbsp; خروج</span>
                </a>
            </div>
            <div class="navBtn">
                <a href="#">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="content">
            @include('_partials.error')
            @include ('_partials.message')
            @yield('content')
            <div id="inline_load" style="display: none;"></div>
            <div class="box footer mgt">
                <div class="in">
                    <div class="txt-r">
                        <?php //show_cr(false);?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" tabindex="-1" role="dialog" id="DeleteModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">پیغام سیستم</h4>
                </div>
                <div class="modal-body">
                    <p>آیا از حذف این رکورد اطمینان دارید؟</p>
                </div>
                <div class="modal-footer">
                    <form action="" method="POST" id="delete_modal_form">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function show_delete_modal(action){
            console.log('route is');
            var html = '{{ csrf_field() }}';
            html += '{{ method_field('DELETE')}}';
            html += '<button type="button" class="btn" data-dismiss="modal"> انصراف</button>';
            html += '<button type="submit" class="btn btn-warning" > &nbsp; بلی &nbsp;</button>';
            $("#delete_modal_form").html(html);
            $('#delete_modal_form').attr('action',  action);
            $("#DeleteModal").modal();  
            return false;
        }
    </script>
</body>

</html>