@extends('layouts.app')
@section('content')
    <script type="text/javascript">
        function do_filter() {
            var path        = 'sysadmin/terminal/index/';
            path += (($('#user_id').val() != '') ? $('#user_id').val() : '0') + '/';
            path += (($('#title').val() != '') ? $('#title').val() : 'null') + '/';
            path += (($('#domain').val() != '') ? $('#domain').val() : 'null') + '/';
            path += $('#status').val() + '/';
            path += $('#ipp').val() + '/';
            window.location = path;
        }
    </script>
    @if(Auth::user()->isAdmin())
    <div class="box mgb">
        <div class="in">
            <div class="row">
                <div class="col-xs-3">
                    <label for="user_id">شناسه کاربر</label>
                    <input id="user_id" name="user_id" class="form-control" value="">
                </div>
                <div class="col-xs-3">
                    <label for="title">عنوان</label>
                    <input id="title" name="title" class="form-control" value="">
                </div>
                <div class="col-xs-3">
                    <label for="domain">دامین</label>
                    <input id="domain" name="domain" class="form-control" value="">
                </div>
                <div class="col-xs-3">
                    <label for="status">وضعیت ترمینال</label>
                    <select id="status" class="form-control">
                        <option value="null">همه موارد</option>
                        <option value="0">در انتظار تایید</option>
                        <option value="1">تایید شده</option>
                        <option value="3">ترمینال مسدود</option>
                        <option value="3">رد شده</option>
                    </select>
                </div>
                <div class="col-xs-3">
                    <label for="ipp">تعداد در هر صفحه</label>
                    <select id="ipp" class="form-control">
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
                <div class="col-xs-3">
                    <div>&nbsp;</div>
                    <button class="btn btn-success" type="button" onclick="do_filter()">
                        <span>اعمال فیلتر/جستجو</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="box table">
        <div class="title">فهرست ترمینال ها</div>
        <div class="in">
            <div class="buttons">
                <a href="{{ route('terminals.add') }}" class="btn btn-success">افزودن ترمینال جدید</a> &nbsp;
            </div>
            <table class="table table-bordered">
                <colgroup width="10%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="27%"></colgroup>
                <colgroup width="12%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="10%"></colgroup>
                <thead>
                <tr>
                    <th>عنوان</th>
                    <th>دامین</th>
                    <th>کاربر</th>
                    <th>API Key</th>
                    <th>تاریخ ثبت</th>
                    <th>کل درآمد</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($terminals as $t)
                    <tr>
                        <td>{{ $t['title'] }}</td>
                        <td>{{ $t['domain'] }}</td>
                        <td>{{ $t['user_id'] }}</td>
                        <td><input type="text" readonly="readonly" onfocus="this.select()" onclick="this.select()"
                                   dir="ltr"
                                   value="{{ $t['api_key'] }}"></td>
                        <td>{{ $t['fa_created_at'] }}</td>
                        <td>{{  $t['total_revenue'] }}</td>
                        <td>
                            @if($t['status'] == 1)
                                <span class="text-success">تایید شده</span>
                            @else
                                <span class="text-danger">تایید نشده</span>
                            @endif
                        </td>
                        <td class="actions" align="center">
                            {{-- todo : fix the links --}}
                            <a href="sysadmin/terminal/view/1007" target="_blank">
                                <img src="{{ asset('template/core/images/view.png') }}" alt="view" title="نمایش"></a>
                            <a href="sysadmin/terminal/edit/1007">
                                <img src="{{ asset('template/core/images/edit.png') }}" alt="edit" title="ویرایش"></a>
                            <a href="sysadmin/terminal/trash/1007" onclick="return ShowDeleteModal(this.href);">
                                <img src="{{ asset('template/core/images/delete.png') }}" alt="delete" title="حذف"></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div id="inline_load" style="display: none;"></div>
    <div class="box footer mgt">
        <div class="in">
                {!! Config::get('constants.options.footer_link') !!}
            </div>
        </div>
    </div>
@endsection
