@extends("layouts.app")
@section('content')
    <div class="box">
        <div class="title">افزودن ترمینال جدید</div>
        <div class="in">
            <form class="form-horizontal" action="{{ route('terminals.add_terminal') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_domain" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">دامین *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class=" input-group"><input type="text" name="domain" id="field_domain" dir="ltr"
                                                                 value="" class="validate[required] form-control">
                                    <span class="input-group-addon">http://www.</span></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">بدون www و http وارد کنید (مثال : mysite.com)</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_title" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">عنوان *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="title" id="field_title" dir="ltr" value=""
                                       class="validate[required] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_karmozd_type" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کسر کارمزد
                        *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <select name="karmozd_type" id="field_karmozd_type" style="max-width:350px;"
                                        class="validate[required] form-control">
                                    <option value="1">کسر کارمزد از پرداخت کننده</option>
                                    <option value="2" selected="selected">کسر کارمزد از شما</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block"><b>از پرداخت کننده :</b> مبلغ کارمزد به مبلغی که پرداخت کننده مشخص میکند
                            اضافه میشود.<br>
                            <b>از شما :</b> کاربر همان مبلغی که مشخص میکند را پرداخت میکند و مابقی پس از کسر کارمزد، به
                            حساب شما افزوده میشود.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_karmozd" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کارمزد
                        *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="karmozd" id="field_karmozd" value="0.00"
                                       class="validate[required] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">در صورتی که میخواهید کارمزدی متفاوت از کارمزد پیشفرض سیستم برای این
                            ترمینال تعیین کنید، مقدار آن را وارد کنید.<br>
                            در صورتی که صفر وارد کنید کارمزد پیشفرض سیستم مد نظر خواهد بود<br>
                            مثال : 2.65</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_gateway_id" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">درگاه
                        بانکی</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <select name="gateway_id" id="field_gateway_id" style="max-width:350px;"
                                        class="form-control">
                                    <option value="0" selected="selected">درگاه پیشفرض سیستم</option>
                                    <option value="1">پرداخت الکترونیک سامان کیش</option>
                                    <option value="8">qere</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_no_domain_lock" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">بدون قفل
                        دامین</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="checkbox" name="no_domain_lock" id="field_no_domain_lock" value="1"
                                       class="validate[custom[integer]]">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">در صورتی که قفل دامین از تنظیمات فعال باشد، میتوانید با تیک زدن این گزینه
                            امکان قفل دامین را برای این ترمینال بردارید.<br>در صورتی که قفل دامین از تنظیمات غیرفعال
                            باشد تیک زدن یا نزدن این گزینه هیچ تاثیری نخواهد داشت.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>
@endsection