@extends('layouts.app')
@section('content')
    <div class="box table">
        <div class="title">لیست اخبار</div>
        <div class="in">
            <table class="table table-bordered">
                <colgroup width="6%"></colgroup>
                <colgroup width="51%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="18%"></colgroup>
                <colgroup width="15%"></colgroup>
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>عنوان خبر</th>
                    <th>وضعیت</th>
                    <th>تاریخ درج خبر</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    @foreach($news as $n)
                        <td align="center">{{ $n->id }}</td>
                        <td>{{ $n->title }}</td>
                        <td>
                            @if($n->active == '1')
                                فعال
                            @else
                                غیر  فعال
                            @endif
                        </td>
                        <td>{{ \App\ThirdParty\convert_gregorian_iso_to_jalali_iso($n->date) }}</td>
                        <td class="actions" align="center">
                            <a href="sysadmin/news/view/2" target="_blank">
                                <img src="{{ asset('template/core/images/view.png') }}" alt="view" title="نمایش">
                            </a>
                            <a href="sysadmin/news/delete/2" onclick="return ShowDeleteModal(this.href);">
                                <img src="{{asset('template/core/images/delete.png')}}" alt="delete" title="حذف"></a>
                            <a href="sysadmin/news/edit/2">
                                <img src="{{asset('template/core/images/edit.png')}}" alt="edit" title="ویرایش">
                            </a>
                        </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection