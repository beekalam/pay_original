@extends('layouts.app')
@section('content')
    <div class="box table">
        <div class="title">لیست صفحات</div>
        <div class="in">
            <table class="table table-bordered">
                <colgroup width="6%"></colgroup>
                <colgroup width="51%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="18%"></colgroup>
                <colgroup width="15%"></colgroup>
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>عنوان صفحه</th>
                    <th>وضعیت</th>
                    <th>تاریخ ایجاد صفحه</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $p)
                    <tr>
                        <td align="center">{{ $p->id }}</td>
                        <td>{{ $p->title }}</td>
                        <td>
                            @if($p->active == '1')
                                فعال
                            @else
                                غیر  فعال
                            @endif
                        </td>
                        <td>{{ \App\ThirdParty\convert_gregorian_iso_to_jalali_iso($p->date) }}</td>
                        <td class="actions" align="center">
                            <a href="sysadmin/page/view/5" target="_blank">
                                <img src="{{ asset('template/core/images/view.png') }}" alt="view" title="نمایش">
                            </a>
                            <a href="sysadmin/page/delete/5" onclick="return ShowDeleteModal(this.href);">
                                <img src="{{ asset('template/core/images/delete.png') }}" alt="delete" title="حذف">
                            </a>
                            <a href="sysadmin/page/edit/5">
                                <img src="{{ asset('template/core/images/edit.png') }}" alt="edit" title="ویرایش">
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection