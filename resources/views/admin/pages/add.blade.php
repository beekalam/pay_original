@extends('layouts.app')
@section('content')
    <div class="box">
        <div class="title">افزودن صفحه جدید</div>
        <div class="in">
            <form class="form-horizontal" action="{{ route('page.store') }}" method="post">
                  {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_title" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">عنوان صفحه *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="title" id="field_title" maxlength="250" size="60"
                                       value="{{ old('title') }}"
                                       class="validate[required,maxSize[250]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field_text" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">متن صفحه *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <textarea name="text" id="field_text" class="validate[required] ckeditor" style="">{{ old('text') }}</textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="field_slug" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">عنوان انگلیسی صفحه
                        *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="slug" id="field_slug" dir="rtl"
                                       value="{{ old('slug') }}"
                                       class="validate[required] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">از این کلمه در آدرس صفحه استفاده میشود<br>مثال : www.domain.com/page/view/<b>about_us</b>
                        </p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_active" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">فعال ؟</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="checkbox" name="active" id="field_active" value="1" checked="checked"
                                       class="validate[custom[integer]]">
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>
@endsection