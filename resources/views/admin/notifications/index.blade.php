@extends('layouts.app')
@section('content')
    <div class="box table">
        <div class="title">فهرست اطلاعیه های عمومی</div>
        <div class="in">
            <table class="table table-bordered">
                <colgroup width="6%"></colgroup>
                <colgroup width="51%"></colgroup>
                <colgroup width="18%"></colgroup>
                <colgroup width="15%"></colgroup>
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>عنوان اطلاعیه</th>
                    <th>تاریخ ثبت اطلاعیه</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($notifications as $n)
                    <tr>
                        <td align="center">{{ $n->id }}</td>
                        <td>{{ $n->title }}</td>
                        <td>{{\App\ThirdParty\convert_gregorian_iso_to_jalali_iso($n->created_at)}}</td>
                        <td class="actions" align="center">
                            <a href="sysadmin/notification/delete/10" onclick="return ShowDeleteModal(this.href);">
                                <img src="{{ asset('template/core/images/delete.png')}}" alt="delete" title="حذف">
                            </a>
                            <a href="sysadmin/notification/edit/10">
                                <img src="{{ asset('template/core/images/edit.png') }}" alt="edit" title="ویرایش">
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection