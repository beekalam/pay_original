@extends('layouts.app');
@section('content')
    <div class="box">
        <div class="title">تنظیمات سیستم</div>
        <div class="in">
            <form class="form-horizontal" action="{{ route('settings.update_site_settings') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_site_page_title" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">عنوان
                        صفحه
                        اصلی</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="site_page_title" id="field_site_page_title"
                                       value="{{ $site_page_title }}" class="validate[required] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block"></p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_is_public" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">برای عموم
                        فعال؟</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="checkbox" name="is_public" id="field_is_public" value="1"
                                       @if ($is_public ==  '1')
                                       checked="checked"
                                       @endif
                                       class="validate[custom[integer]]">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">با تیک زدن این گزینه، امکان عضویت فعال میشود و سایت دارای صفحه اصلی خواهد
                            بود<br>
                            در صورت غیرفعال کردن این ویژگی، با بازدید از صفحه اصلی سایت، یک صفحه سفید نمایش داده خواهد
                            شد.
                        </p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_site_contact_email" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">ایمیل
                        فرم تماس با ما</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="site_contact_email" id="field_site_contact_email"
                                       value="{{ $site_contact_email }}"
                                       class="validate[required,custom[email]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">آدرسی که فرم های تماس با ما به آن ارسال میشود</p>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>
@endsection