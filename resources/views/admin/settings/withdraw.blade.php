@extends('layouts.app')
@section('content')
    <div class="box">
        <div class="title">تنظیمات سیستم</div>
        <div class="in">
            <form class="form-horizontal" action="{{ route("settings.withdraw_settings_update") }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_withdraw_active" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تسویه
                        حساب
                        فعال؟</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="checkbox" name="withdraw_active" id="field_withdraw_active" value="1"
                                       @if($withdraw_active == 1)
                                       checked="checked"
                                       @endif
                                       class="validate[custom[integer]]">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">آیا امکان درخواست تسویه حساب در پنل کاربری فعال باشد؟</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_withdraw_min" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">حد نصاب
                        پرداخت</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="withdraw_min" id="field_withdraw_min"
                                       value="{{ $withdraw_min }}"
                                       class="validate[required,custom[integer]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">حد نصاب پرداخت موجودی کاربران برای تسویه حساب (به ریال)</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_tasvie_limit_hours" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">محدودیت
                        ساعتی درخواست</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="tasvie_limit_hours" id="field_tasvie_limit_hours"
                                       value="{{  $tasvie_limit_hours }}"
                                       class="validate[required,custom[integer]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block"></p>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>
@endsection