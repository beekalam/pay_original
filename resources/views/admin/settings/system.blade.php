@extends('layouts.app')
@section('content')
    <div class="box">
        <div class="title">تنظیمات سیستم</div>
        <div class="in">
            <form class="form-horizontal" action="{{ route('settings.system_settings_update') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_terminals_auto_approve" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تایید
                        اتوماتیک ترمینال ها</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="checkbox" name="terminals_auto_approve" id="field_terminals_auto_approve"
                                       @if($terminals_auto_approve == '1')
                                       checked="checked"
                                       @endif
                                       value="1" class="validate[custom[integer]]">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">در صورت تیک زدن این گزینه، ترمینال هایی که کاربران ثبت میکنند بصورت خودکار
                            تایید میشود.<br>
                            در غیر اینصورت ترمینال های ثبت شده در صف انتظار قرار میگیرند و مدیر میتواند پس از بررسی آنها
                            را
                            تایید یا رد کند.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_max_upload_size" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">حجم
                        مجاز
                        مدارک</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="max_upload_size" id="field_max_upload_size" value="{{ $max_upload_size }}"
                                       class="validate[required,custom[integer]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">حداکثر حجم مجازی که هر فایل مدرک میتواند داشته باشد<br>به کیلوبایت</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_photo_shenasname" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">آپلود
                        عکس
                        شناسنامه؟</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="checkbox" name="photo_shenasname" id="field_photo_shenasname" value="1"
                                       @if($photo_shenasname == '1')
                                       checked="checked"
                                       @endif
                                       class="validate[custom[integer]]">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">در صورت تیک زدن این گزینه، کاربران میبایست تصویر شناسنامه را نیز علاوه بر
                            کارت
                            ملی آپلود کنند.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_domain_lock" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">قفل روی
                        دامین؟</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="checkbox" name="domain_lock" id="field_domain_lock" value="1"
                                       @if($domain_lock == "1")
                                               checked="checked"
                                       @endif
                                       class="validate[custom[integer]]">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">در صورت تیک زدن این گزینه، دامین ارسال کننده تراکنش با دامین ترمینال
                            مربوطه
                            مقایسه خواهد شد و در صورت مچ نبودن اجازه پرداخت داده نخواهد شد.<br>در واقع از این گزینه
                            میتوانید
                            برای ایجاد یک امنیتی در مقابل کاربرانی که قصد سوء استفاده دارند استفاده کنید.<br>ولی توجه
                            داشته
                            باشید که امکان دور زدن این ویژگی برای افراد حرفه ای وجود دارد و امینیت آن ۱۰۰٪‌ نیست!</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_show_invoice_errors_usercp"
                           class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">نمایش
                        خطا تراکنش ها</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="checkbox" name="show_invoice_errors_usercp"
                                       id="field_show_invoice_errors_usercp" value="1"
                                       @if($show_invoice_errors_usercp)
                                       checked="checked"
                                       @endif
                                       class="validate[custom[integer]]">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">در صورت تیک زدن این گزینه توضیحات خطاهای تراکنش ها به کاربران نمایش داده
                            میشود
                            و با برداشتن تیک آن توضیحات خطاها نمایش داده نخواهد شد و تراکنش هایی که به دلیل مشکل اتصال
                            به
                            بانک و موارد مرتبط ناموفق میشوند نیز نمایش داده نخواهد شد.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>
@endsection