@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="title">تنظیمات سیستم</div>
        <div class="in">
            <form class="form-horizontal" action="{{ route("settings.karmozd_settings_update") }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_karmozd" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کارمزد پرداخت
                        آنلاین</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="karmozd" id="field_karmozd" value="{{ $karmozd }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">درصدی که به ازای هر تراکنش به عنوان کارمزد سایت، از مبلغ پرداختی کسر و
                            مابقی
                            به موجودی کاربر افزوده میشود
                            مثال : 1.25</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_karmozd_max" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">سقف
                        کارمزد</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="karmozd_max" id="field_karmozd_max" value="{{ $karmozd_max }}"
                                       class="validate[custom[integer]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">سقف مبلغی که به عنوان کارمزد دریافت میشود.<br>
                            اگر درصد کارمزد از این رقم بیشتر باشد، از این رقم به عنوان کارمزد استفاده میشود.<br>
                            در صورتی که نمیخواهید سقف معینی تعیین کنید این رقم را 0 قرار دهید تا همیشه درصد مد نظر باشد.<br>
                            به ریال وارد کنید</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_karmozd_guestpay" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کارمزد
                        پرداخت مهمان</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="karmozd_guestpay" id="field_karmozd_guestpay"
                                       value="{{ $karmozd_guestpay }}"
                                       class="validate[custom[integer]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">مبلغ ثابتی که به عنوان کارمزد پرداخت مهمان گرفته میشود.<br>
                            به ریال وارد کنید</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_karmozd_withdraw_shetab"
                           class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کارمزد
                        تسویه شتابی</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="karmozd_withdraw_shetab" id="field_karmozd_withdraw_shetab"
                                       value="{{ $karmozd_withdraw_shetab }}"
                                       class="validate[custom[integer]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">مبلغ ثابتی که به عنوان کارمزد تسویه شتابی (کارت به کارت) از درخواست کاربر
                            کسر
                            میشود.<br> به ریال وارد کنید.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_karmozd_withdraw_paya" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کارمزد
                        تسویه پایا</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="karmozd_withdraw_paya" id="field_karmozd_withdraw_paya"
                                       value="{{ $karmozd_withdraw_paya }}"
                                       class="validate[custom[integer]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">مبلغ ثابتی که به عنوان کارمزد تسویه پایا از درخواست کاربر کسر میشود.<br>به
                            ریال وارد کنید.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>
@endsection