<?php
$no_layout = true;
$modal_template = true;
?>
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اطلاعات ثبت شده</div>
            <div class="in">
                <?php foreach($data['data'] as $k => $v):?>
                <dl class="dl-horizontal">
                    <dt><?=$fields[$k];?></dt>
                    <dd><?=nl2br($v);?></dd>
                </dl>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="box">
            <div class="title">اطلاعات فرم و پرداخت</div>
            <div class="in">
                <dl class="dl-horizontal">
                    <dt>مبلغ</dt>
                    <dd><?=number_format($data['amount']);?> ریال</dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>وضعیت</dt>
                    <dd>
                        مشاهده تراکنش
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt>تاریخ ثبت</dt>
                    <dd><?php    //jdate($data['date'], 'j M Y ساعت H:i');?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>