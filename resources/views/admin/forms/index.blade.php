@extends('layouts.app')
@section('content')
    <div class="box table">
        <div class="title">لیست فرم های ثبت شده</div>
        <div class="in">
            <table class="table table-bordered">
                <colgroup width="30%"></colgroup>
                <colgroup width="15%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="15%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="10%"></colgroup>
                <thead>
                <tr>
                    <th>فرم</th>
                    <th>کاربر</th>
                    <th>ترمینال</th>
                    <th>مبلغ</th>
                    <th>تاریخ ثبت</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($forms_data as $f)
                    <tr>
                        <td>{{ $f->form->title }}</td>
                        <td>
                            {{ $f->form->user->name }}
                        </td>
                        <td>
                            {{ $f->form->terminal->title }}
                        </td>
                        <td>
                            {{ $f->amount }}
                        </td>
                        <td>{{ \App\ThirdParty\convert_gregorian_iso_to_jalali_iso($f->date) }}</td>
                        <td>
                            @if($f->status == 0)
                                معلق
                            @elseif($f->status == 1)
                                <span class="text-success">
                                پرداخت موفق
                                </span>
                            @elseif($f->status == 2)
                                <span class="text-danger">
                               پرداخت ناموفق
                                </span>
                            @endif
                        </td>
                        <td class="actions" align="center">
                            <a href="{{ route('forms.viewdata',['id' => $f->id]) }}"
                               onclick="inline_load(this,event);"><img
                                        src="{{ asset('template/cp/images/document_inspector.png') }}" alt="اطلاعات فرم"
                                        title="اطلاعات فرم">
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection