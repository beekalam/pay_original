@extends('layouts.app')
@section('content')
    <div class="box">
        <div class="title">افزودن فرم پرداخت جدید</div>
        <div class="in">
            <form class="form-horizontal" action="{{ route('forms.store') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_title" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">عنوان کالا
                        *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="title" id="field_title" maxlength="50"
                                       value="{{ old('title') }}"
                                       class="validate[required,maxSize[50]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_amount" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">مبلغ *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class=" input-group">
                                    <input type="text" name="amount" id="field_amount" dir="ltr"
                                           value="{{ old('amount') }}"
                                           class="validate[required,custom[integer]] form-control">
                                    <span class="input-group-addon">ریال</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_callback_url_success" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">انتقال
                        به آدرس (پرداخت موفق)</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="callback_url_success" id="field_callback_url_success" dir="ltr"
                                       value="{{ old('callback_url_success') }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">آدرس کامل همراه با <b class="en" style="display:inline-block;">http://</b><br>
                            در صورت خالی گذاشتن نتیجه تراکنش در صفحه پیشفرض سایت نمایش داده خواهد شد.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_callback_url_fail" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">انتقال
                        به آدرس (پرداخت ناموفق)</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="callback_url_fail" id="field_callback_url_fail" dir="ltr"
                                       value="{{ old('calback_url_fail') }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">آدرس کامل همراه با <b class="en" style="display:inline-block;">http://</b><br>
                            در صورت خالی گذاشتن نتیجه تراکنش در صفحه پیشفرض سایت نمایش داده خواهد شد.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_terminal_id" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">ترمینال
                        *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <select name="terminal_id" id="field_terminal_id" style="max-width:350px;"
                                        class="validate[required] form-control">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($terminals as $terminal)
                                        <option value="{{ $terminal['id'] }}">{{ $terminal['title'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_fields" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">فیلدها *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-12">
                                <select name="fields[]" id="field_fields_" class="validate[required] multiselect"
                                        multiple="multiple" style="width: 440px; height: 200px; display: none;"
                                        data-placeholder="فیلدها">
                                    <option value="name">نام و نام خانوادگی</option>
                                    <option value="domain">دامین</option>
                                    <option value="email">ایمیل</option>
                                    <option value="mobile">موبایل</option>
                                    <option value="phone">تلفن</option>
                                    <option value="address">آدرس پستی</option>
                                    <option value="zipcode">کد پستی</option>
                                    <option value="prod_id">شناسه محصول</option>
                                    <option value="code">کد پیگیری</option>
                                    <option value="description">توضیحات</option>
                                </select>
                                <div class="cb"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">حداقل یک مورد را انتخاب کنید.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>
@endsection