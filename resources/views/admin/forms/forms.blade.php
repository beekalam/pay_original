@extends('layouts.app')
@section('content')
<div class="box table">
    <div class="title">لیست فرم های پرداخت</div>
    <div class="in">
        <table class="table table-bordered">
            <colgroup width="20%"></colgroup>
            <colgroup width="10%"></colgroup>
            <colgroup width="10%"></colgroup>
            <colgroup width="10%"></colgroup>
            <colgroup width="10%"></colgroup>
            <colgroup width="30%"></colgroup>
            <colgroup width="10%"></colgroup>
            <thead>
                <tr>
                    <th>عنوان</th>
                    <th>کاربر</th>
                    <th>ترمینال</th>
                    <th>مبلغ</th>
                    <th>تاریخ ایجاد</th>
                    <th>آدرس فرم</th>
                    <th>عملیات</th>
                </tr>
            </thead>
            <tbody>
                @foreach($forms as $f)
                <tr>
                    <td>{{ $f->title }}</td>
                    <td>{{ $f->user->name }}</td>
                    <td>{{ $f->terminal->title }}</td>
                    <td>{{ number_format($f->amount) }}</td>
                    <td>{{ \App\ThirdParty\convert_gregorian_iso_to_jalali_iso($f->created_at) }}</td>
                    <td>
                        <input type="text" readonly="readonly" onfocus="this.select()" onclick="this.select()" dir="ltr"
                            value="{{ route('forms.pay',['pay_id' => $f->id ."_". $f->hash]) }}">
                    </td>
                    <td class="actions" align="center">
                        <a href="javascript:void" onclick="show_delete_modal('{{ route('forms.delete',['id' => $f->id]) }}');">
                            <img src='/template/core/images/delete.png' alt="delete" title="حذف">
                        </a>
                        <a href="javascript:void">
                            <img src="/template/core/images/edit.png" alt="edit" title="ویرایش">
                        </a>
                        <a href="sysadmin/form/code/1001" onclick="inline_load(this,event); return false;">
                            <img src="/template/cp/images/script_code_red.png" alt="دریافت کد" title="دریافت کد">
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection