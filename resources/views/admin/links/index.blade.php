@extends('layouts.app')
@section('content')
    <div class="box table">
        <div class="title">لیست لینک های پرداخت</div>
        <div class="in">
            <div class="buttons">
                <a href="{{ route('links.add') }}" class="btn btn-success">افزودن لینک جدید</a> &nbsp;
            </div>
            <table class="table table-bordered">
                <colgroup width="20%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="30%"></colgroup>
                <colgroup width="10%"></colgroup>
                <thead>
                <tr>
                    <th>عنوان</th>
                    <th>کاربر</th>
                    <th>ترمینال</th>
                    <th>مبلغ</th>
                    <th>لینک</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($links as $l)
                    <tr>
                        <td>{{ $l->user->name }}</td>
                        <td>{{ $l->title }}</td>
                        <td>{{ $l->terminal->title }}</td>
                        <td>{{ number_format($l->amount) }} ریال</td>
                        <td><input type="text" readonly="readonly" onfocus="this.select()" onclick="this.select()"
                                   dir="ltr"
                                   value="{{ route('links.pay',['pay_id' => $l->id . "_" .  $l->hash]) }}">
                        </td>
                        <td class="actions" align="center">
                            <a href="sysadmin/link/trash/1001" onclick="return ShowDeleteModal(this.href);">
                                <img src="{{ asset('template/core/images/delete.png') }}" alt="delete" title="حذف">
                            </a>
                            <a href="sysadmin/link/edit/1001">
                                <img src="{{ asset('template/core/images/edit.png') }}" alt="edit" title="ویرایش">
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection