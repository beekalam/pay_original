@extends('layouts.app')
@section('content')
    <div class="box">
        <div class="title">{{ $title }}</div>

        @if($step == 1)
            <div class="in">
                <form class="form-horizontal" action="{{ route("gateway.add_gateway") }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="field_title" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">عنوان درگاه
                            *</label>
                        <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                            <div class="row">
                                <div class="col-xs-5">
                                    <input type="text" name="title" id="field_title" maxlength="50" value=""
                                           class="validate[required,maxSize[50]] form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <p class="help-block">لطفا عنوانی در اینجا وارد کنید که در آینده بتوانید این درگاه را از
                                دیگر
                                درگاه های با بانک یکسان تشخیص دهید.</p>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field_module_id" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">ماژول
                            بانک
                            *</label>
                        <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                            <div class="row">
                                <div class="col-xs-5">
                                    <select name="module_id" id="field_module_id" style="max-width:350px;"
                                            class="validate[required,custom[integer]] form-control">
                                        <option value="" selected="selected">انتخاب کنید</option>
                                        @foreach($bank_modules as $m)
                                            <option value="{{ $m['id'] }}">{{ $m['title'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <input type="hidden" name="step" value="{{  $step }}"/>
                    <p class="form-submit" align="center">
                        <button type="submit" class="btn btn-success ">
                            <span>مرحله بعد</span>
                        </button> &nbsp;&nbsp;
                    </p>
                </form>
                @endif
                @if($step == 2)
                    <form class="form-horizontal" action="{{ route("gateway.add_gateway") }}" method="post">
                        {{ csrf_field() }}
                        @foreach($requirements as $req)
                            <div class="form-group">
                                <label for="field_title"
                                       class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">{{$req}}</label>
                                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <input type="text" name="{{  $req }}" id="field_title" maxlength="50"
                                                   value=""
                                                   class="validate[required,maxSize[50]] form-control">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        @endforeach

                        <input type="hidden" name="step" value="{{  $step }}"/>
                        <input type="hidden" name="module_id" value="{{ $module_id }}"/>
                        <input type="hidden" name="name" value="{{ $name }}"/>
                        <p class="form-submit" align="center">
                            <button type="submit" class="btn btn-success ">
                                <span>مرحله بعد</span>
                            </button> &nbsp;&nbsp;
                        </p>
                    </form>
                @endif
            </div>
    </div>
@endsection