@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="title">تنظیمات درگاه پرداخت</div>
        <div class="in">
            <form class="form-horizontal" action="{{  route('gateway.update_setting') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_gateway_id" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">درگاه پرداخت
                        پیشفرض *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <select name="gateway_id" id="field_gateway_id" style="max-width:350px;"
                                        class="validate[required] form-control">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($gateways as $g)
                                        @if($g['id'] == $gateway1['id'])
                                            <option value="{{ $g['id'] }}"
                                                    selected="selected">{{ $g['title'] }}</option>
                                        @else
                                            <option value="{{ $g['id'] }}">{{ $g['title'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="field_gateway_id_2" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">درگاه
                        پرداخت زاپاس *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <select name="gateway_id_2" id="field_gateway_id_2" style="max-width:350px;"
                                        class="validate[required] form-control">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($gateways as $g)
                                        @if($g['id'] == $gateway2['id'])
                                            <option value="{{ $g['id'] }}"
                                                    selected="selected">{{ $g['title'] }}</option>
                                        @else
                                            <option value="{{ $g['id'] }}">{{ $g['title'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">لطفا بانک متفاوتی را انتخاب کنید.<br>
                            در صورت انتخاب بانک یکسان با درگاه پیشفرض، تداخل ایجاد خواهد شد.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>
@endsection