@extends("layouts.app")
@section('content')
    <?php
    // dump($gateway);
    ?>
    <div class="box">
        <div class="title">ویرایش درگاه بانکی</div>
        <div class="in">
            <form class="form-horizontal" action="{{ route('gateway.edit_gateway') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="field_title" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">عنوان درگاه
                        *</label>
                    <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                        <div class="row">
                            <div class="col-xs-5">
                                <input type="text" name="title" id="field_title" maxlength="50"
                                       value="{{ $gateway['title'] }}"
                                       class="validate[required,maxSize[50]] form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <p class="help-block">لطفا عنوانی در اینجا وارد کنید که در آینده بتوانید این درگاه را از دیگر
                            درگاه
                            های با بانک یکسان تشخیص دهید.</p>
                        <div class="clearfix"></div>

                    </div>
                </div>

                @foreach($gateway['data'] as $k=>$v)
                    <div class="form-group">
                        <label for="field_MELLAT_TERMINAL_CODE"
                               class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">{{ $k }}
                            *</label>
                        <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                            <div class="row">
                                <div class="col-xs-5">
                                    <input type="text" name="{{ $k }}" id="{{ $k }}"
                                           value="{{ $v }}"
                                           class="validate[required] form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <p class="help-block"></p>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                @endforeach
                <input type="hidden" name="id" value="{{ $gateway['id'] }}" />
                <p class="form-submit" align="center">
                    <button type="submit" class="btn btn-success ">
                        <span>ثبت</span>
                    </button> &nbsp;&nbsp;
                </p>

            </form>
        </div>
    </div>

@endsection