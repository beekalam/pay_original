@extends('layouts.app')

@section('content')
    <div class="box table">
        <div class="title">{{ $title }}</div>
        <div class="in">
            <table class="table table-bordered">
                <colgroup width="20%"></colgroup>
                <colgroup width="35%"></colgroup>
                <colgroup width="10%"></colgroup>
                <colgroup width="15%"></colgroup>
                <thead>
                <tr>
                    <th>ماژول</th>
                    <th>عنوان درگاه</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                @foreach($gateways as $g)
                    <tr>
                        <td>
                            {{ $g['module_title'] }}
                        </td>
                        <td>
                            {{ $g['title'] }}
                        </td>
                        <td>
                            <span class="text-success">فعال</span>
                        </td>

                        <td class="actions" align="center">
                            <a href="{{ route("gateway.edit",["id" => $g['id']]) }}">
                                <img src="template/core/images/edit.png" alt="edit" title="ویرایش">
                            </a>

                            {{--<a href="{{ route('gateway.delete') }}">--}}
                            {{--<img src="template/core/images/delete.png" alt="delete" title="حذف">--}}
                            {{--</a>--}}
                            <img src="template/core/images/delete.png" alt="delete" title="حذف"
                                 data-toggle="modal" data-target="#delete_gateway_modal_{{ $g['id'] }}"
                                 style="cursor: pointer;">

                            <a href="sysadmin/gateway/test/1" target="_blank">
                                <img src="template/cp/images/money_add.png" alt="تست پرداخت" title="تست پرداخت">
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    @foreach($gateways as  $g)
        <div class="modal fade" tabindex="-1" role="dialog" id="delete_gateway_modal_{{ $g['id'] }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">پیغام سیستم</h4>
                    </div>
                    <div class="modal-body">
                        <p>آیا از حذف این رکورد اطمینان دارید؟</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal"> انصراف</button>
                        <form method="POST" action="{{ route('gateway.delete') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $g['id'] }}">
                            <button type="submit" class="btn btn-warning"> &nbsp; بلی</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@endsection