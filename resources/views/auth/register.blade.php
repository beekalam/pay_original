@extends('layouts.front')

@section('content')
    <div class="cont">
        <h1 class="title">
            <span>ثبت نام</span>
        </h1>
        <form class="form-horizontal" action="{{ route('register') }}" method="post">
            {{ csrf_field() }}
            <div class="row help">
                <div class="col-xs-12">
                    <p class="text-info">
                        اطلاعاتی که در فرم زیر وارد میکنید به هیچ وجه در اختیار شخص یا گروهی قرار نخواهد گرفت.<br>
                        مواردی مانند ایمیل و شماره موبایل نیز فقط برای اطلاع رسانی استفاده خواهد شد. </p>
                </div>
            </div>
            @include('layouts.error')
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="field_name" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">نام و نام خانوادگی
                    *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="name" id="field_name"
                                   placeholder="نام و نام خانوادگی" maxlength="50"
                                   value="{{ old('name') }}" class="form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="form-group {{$errors->has('email') ? 'has-error':'' }}">
                <label for="field_email" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">ایمیل *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="email" id="field_email" dir="ltr" placeholder="ایمیل" size="50"
                                   value="{{ old('email') }}"
                                   class="validate[required,custom[email]] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="form-group {{ $errors->has('mobile') ? 'has-error':'' }}">
                <label for="field_mobile" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تلفن همراه *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="mobile" id="field_mobile" dir="ltr"
                                   placeholder="تلفن همراه"
                                   value="{{ old('mobile') }}"
                                   class="validate[required] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <span class="help-block">مثال : 09123456789</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="form-group {{ $errors->has('phone') ? 'has-error':'' }}">
                <label for="field_phone" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تلفن ثابت (با کد شهر)
                    *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="phone" id="field_phone" dir="ltr"
                                   placeholder="تلفن ثابت (با کد شهر)"
                                   value="{{ old('phone') }}"
                                   class="validate[required] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <span class="help-block">مثال : 02144422333</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="form-group {{ $errors->has('password') ? 'has-error':'' }}">
                <label for="field_password" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کلمه عبور
                    *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="password" name="password" id="field_password" dir="ltr" placeholder="کلمه عبور"
                                   maxlength="100"
                                   value=""
                                   class="validate[required,minSize[8],maxSize[100]] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="form-group">
                <label for="field_repassword" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تکرار کلمه عبور
                    *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="password" name="password_confirmation" id="field_repassword" dir="ltr"
                                   placeholder="تکرار کلمه عبور"
                                   value="" class="validate[required] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <span class="help-block">عینا کلمه عبوری که در بالا وارد کرده اید را وارد کنید</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <div class="form-group {{ $errors->has('tos') ? 'has-error':'' }}">
                <label for="field_tos" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تایید قوانین *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <label> <input type="checkbox" name="tos" id="field_tos" value="1"
                                           class="validate[required]">
                                تایید قوانین</label></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                <span class="help-block">بعد از مطالعه دقیق <a href="page/view/tos" target="_blank">قوانین وبسایت</a>،
                    در صورت تایید، این گزینه را تیک بزنید.</span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <p class="form-submit" align="center">
                <button type="submit" class="btn btn-success ">
                    <span>ثبت نام</span>
                </button>
            </p>
        </form>
        <div class="bottomPad"></div>
    </div>
    <?php if(false): ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
@endsection
