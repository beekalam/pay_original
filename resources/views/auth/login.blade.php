<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <base href="http://localhost/pay_sabanovin_local/">
    <link rel="stylesheet" type="text/css" href="{{asset('template/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/bootstrap/css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/admin/login.css') }}">
    <title>ورود کاربر</title>
</head>

<body>
    <div class="form-signin">
        <div class="inner">
             @include('layouts.error')
            <h2 class="form-signin-heading">ورود کاربر</h2>
            <form method="post" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                    <div class="input-group">
                        <input type="text" name="email" 
                            value="{{ old('email') }}"
                            dir="ltr" class="form-control" placeholder="email"
                            autofocus="">
                        <div class="input-group-addon">
                            <i class="glyphicon glyphicon-user"></i>
                        </div>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('password') ? 'has-error':'' }}">
                    <div class="input-group">
                        <input type="password" name="password" value="" dir="ltr" class="form-control" placeholder="Password">
                        <div class="input-group-addon">
                            <i class="glyphicon glyphicon-lock"></i>
                        </div>
                    </div>
                </div>

                @if(false)
                <div class="form-group">
                    <div class="input-group" id="captchaWrapper">
                        <input type="text" name="captcha" dir="ltr" class="form-control" placeholder="Captcha"
                            autocomplete="off">
                        <span class="input-group-btn">
                            <button onclick="refreshCaptcha()" class="btn btn-info" type="button">
                                <i class="glyphicon glyphicon-refresh"></i>
                            </button>
                        </span>
                        <img src="captcha" alt="captcha" id="captcha">
                    </div>
                </div>
                @endif

                <div class="form-group">
                    <button class="btn btn-large btn-danger btn-block" type="submit">ورود</button>
                </div>
                <p><a href="{{ route('password.request') }}" class="forgot">کلمه عبور خود را فراموش کرده اید؟</a></p>
            </form>


            <script type="text/javascript">
                function refreshCaptcha() {
                    document.getElementById('captcha').src = 'captcha/?' + Math.floor(Math.random() * 1000000)
                    return false;
                }
            </script>

        </div>
        {!! Config::get('constants.options.footer_link_center') !!}
    </div>

</body>

</html>
@if(false)
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                    required autofocus>

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        @if(false)
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif