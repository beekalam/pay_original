@extends('layouts.app')

@section('content')
<div class="box">
    <div class="title">ایجاد تیکت جدید</div>
    <div class="in">
        <form class="form-horizontal" action="{{ route('tickets.store') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="field_department" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">بخش *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <select name="department" id="field_department" style="max-width:350px;" 
                                class="validate[required] form-control">
                                @foreach ($departments as $dep)
                                    <option value="{{ $dep->id }}">{{ $dep->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>


            <div class="form-group">
                <label for="field_title" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">عنوان تیکت *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" name="title" id="field_title" maxlength="250" size="60" 
                                value="{{ old('title') }}" 
                                class="validate[required,minSize[4],maxSize[250]] form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="form-group">
                <label for="field_text" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">متن تیکت *</label>
                <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                    <div class="row">
                        <div class="col-xs-5">
                            <textarea name="text" id="field_text" cols="80" rows="7" 
                                class="validate[required,minSize[10],maxSize[1000]] span9 form-control">{{ old('text') }}</textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>

            <p class="form-submit" align="center">
                <button type="submit" class="btn btn-success ">
                    <span>ارسال تیکت</span>
                </button> &nbsp;&nbsp;
            </p>

        </form>
    </div>
</div>
@endsection