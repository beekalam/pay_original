@extends('layouts.app')
@section('content')
<div class="box table">
    <div class="title">فهرست تیکت های پشتیبانی شما</div>
    <div class="in">
        <div class="buttons">
            <a href="{{ route('tickets.create') }}" class="btn btn-success">ایجاد تیکت جدید</a> &nbsp;
        </div>
        <table class="table table-bordered">
            <colgroup width="8%"></colgroup>
            <colgroup width="12%"></colgroup>
            <colgroup width="32%"></colgroup>
            <colgroup width="12%"></colgroup>
            <colgroup width="17%"></colgroup>
            <colgroup width="10%"></colgroup>
            <colgroup width="9%"></colgroup>
            <thead>
                <tr>
                    <th>شناسه</th>
                    <th>بخش</th>
                    <th>عنوان تیکت</th>
                    <th>وضعیت</th>
                    <th>آخرین ارسال</th>
                    <th>توسط</th>
                    <th>عملیات</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tickets as $t)
                <tr>
                    <td>{{ $t['id'] }}</td>
                    <td></td>
                    <td>{{ $t['title'] }}</td>
                    <td align="center">
                        {{ $t['status'] }}
                        <!-- <span style="color:#795237">در انتظار بررسی</span> -->
                    </td>
                    <td>{{ \App\ThirdParty\convert_gregorian_iso_to_jalali_iso($t['created_at']) }} </td>
                    <td align="center">شما</td>
                    <td class="actions" align="center">
                        <a href="#">
                            <img src="/template/core/images/view.png" alt="مشاهده تیکت" title="مشاهده تیکت">
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection