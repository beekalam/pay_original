@extends('layouts.front')
@section('content')

<div class="cont">
    <h1 class="title">
        <span>ثبت نام</span>
    </h1>
    <form class="form-horizontal" action="{{ route('front.register_store') }}" method="post">
        <div class="row help">
            <div class="col-xs-12">
                <p class="text-info">
                    اطلاعاتی که در فرم زیر وارد میکنید به هیچ وجه در اختیار شخص یا گروهی قرار نخواهد گرفت.<br>
                    مواردی مانند ایمیل و شماره موبایل نیز فقط برای اطلاع رسانی استفاده خواهد شد. </p>
            </div>
        </div>

        <div class="form-group">
            <label for="field_name" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">نام و نام خانوادگی *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="text" name="name" id="field_name" placeholder="نام و نام خانوادگی" maxlength="50"
                            value="" class="validate[required,maxSize[50]] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_email" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">ایمیل *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="text" name="email" id="field_email" dir="ltr" placeholder="ایمیل" size="50" value=""
                            class="validate[required,custom[email]] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_mobile" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تلفن همراه *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="text" name="mobile" id="field_mobile" dir="ltr" placeholder="تلفن همراه" value=""
                            class="validate[required] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <span class="help-block">مثال : 09123456789</span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_phone" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تلفن ثابت (با کد شهر) *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="text" name="phone" id="field_phone" dir="ltr" placeholder="تلفن ثابت (با کد شهر)"
                            value="" class="validate[required] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <span class="help-block">مثال : 02144422333</span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_password" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">کلمه عبور *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="password" name="password" id="field_password" dir="ltr" placeholder="کلمه عبور"
                            maxlength="100" value="" class="validate[required,minSize[8],maxSize[100]] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_repassword" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تکرار کلمه عبور *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="password" name="repassword" id="field_repassword" dir="ltr" placeholder="تکرار کلمه عبور"
                            value="" class="validate[required] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <span class="help-block">عینا کلمه عبوری که در بالا وارد کرده اید را وارد کنید</span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_password2" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">رمز دوم *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="password" name="password2" id="field_password2" dir="ltr" placeholder="رمز دوم"
                            maxlength="100" value="" class="validate[required,minSize[8],maxSize[100]] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <span class="help-block">برای انجام امور تسویه حساب و دیگر موارد مهم، از این رمز استفاده خواهد شد.</span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_repassword2" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تکرار رمز دوم *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="password" name="repassword2" id="field_repassword2" dir="ltr" placeholder="تکرار رمز دوم"
                            value="" class="validate[required] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <span class="help-block">عینا کلمه عبوری که در بالا وارد کرده اید را وارد کنید</span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_tos" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تایید قوانین *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <label> <input type="checkbox" name="tos" id="field_tos" value="1" class="validate[required]">
                            تایید قوانین</label> </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <span class="help-block">بعد از مطالعه دقیق <a href="page/view/tos" target="_blank">قوانین وبسایت</a>،
                        در صورت تایید، این گزینه را تیک بزنید.</span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <p class="form-submit" align="center">
            <button type="submit" class="btn btn-success ">
                <span>ثبت نام</span>
            </button>
        </p>
    </form>
    <div class="bottomPad"></div>
</div>
@endsection