@extends('layouts.front')
@section('content')
        <div class="cont">
            <div id="slider">
                <div class="ls-slide" data-ls="slidedelay: 9500; transition2d: 5,52,75,107,113">
                    <img src="template/site/images/home-img.jpg" class="ls-bg" alt="slide background" />
                    <h2 data-ls="offsetxin:0; offsetxout:0; delayin:2500; durationin:2000; easingin:easeOutElastic; transformoriginin:50% bottom 0; rotatexin:90; transformoriginout:50% bottom 0; rotatexout:90; durationout:2000;" class="ls-l" style="color: #fff; left: 54%; top: 70px; background: #fff; border-radius:5px 5px 0 0; color: #333; padding: 0 15px; line-height: 60px; white-space: nowrap;">در سریعترین زمان ممکن</h2>
                    <h1 data-ls="offsetxin:right; delayin:300; durationin:3000; offsetxout:right; durationout:2000;" class="ls-l" style="color: #fff; left: 52%; top: 130px; background: rgba(0,0,0,.7); line-height: 100px; padding: 0 100px; border-radius: 5px;">درگاه واسط بانکی</h1>
                    <h2 data-ls="offsetxin:0; offsetxout:0; delayin:3500; durationin:2000; easingin:easeOutElastic; transformoriginin:50% top 0; rotatexin:90; transformoriginout:50% top 0; rotatexout:90; durationout:2000;" class="ls-l" style="color: #fff; left: 48%; top: 230px; background: #ECD63C; border-radius:0 0 5px 5px; padding: 0 10px; line-height: 60px;">و ایمن ترین بستر</h2>
                </div>
                <div class="ls-slide" data-ls="slidedelay: 8000; transition2d: 5,52,75,107,113">
                    <img src="template/site/images/slide2-bg.jpg" class="ls-bg" alt="slide background" />
                    <h1 data-ls="offsetxin:right; offsetxout:right; delayin:500; durationin:3000; easingin:easeOutExpo; durationout:2000;" class="ls-l" style="color: #fff; left: 75%; top: 50%; background: #BEEE8A; border-radius:5px; color: #333; padding: 0 15px; line-height: 60px; white-space: nowrap;">دریافت و پرداخت وجه</h1>
                    <h1 data-ls="offsetxin:left; offsetxout:left; delayin:1500; durationin:3000; easingin:easeOutExpo; durationout:1500;" class="ls-l" style="color: #fff; left: 25%; top: 50%; background: #87CAF1; border-radius:5px; color: #333; padding: 0 15px; line-height: 60px; white-space: nowrap;">به آسان ترین روش</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-xs-12 featured-box">
                    <div class="circle">
                        <i class="fa fa-shield"></i>
                        <span></span>
                    </div>
                    <div class="featured-desc">
                        <h3>امنیت بالا</h3>
                        <p>انجام تراکنش ها در امن ترین بسترها صورت میگیرد، تمرکز خود را روی فروش بیشتر بگذارید و تامین امنیت را به ما بسپارید.</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 featured-box">
                    <div class="circle">
                        <i class="fa fa-bar-chart"></i>
                        <span></span>
                    </div>
                    <div class="featured-desc">
                        <h3>گزارشات دقیق</h3>
                        <p>تمامی تراکنش ها موفق و ناموفق در سیستم ثبت و ضبط میشوند و میتوانید گزارشات مختلف آنی را در سیستم مشاهده کنید.</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 featured-box">
                    <div class="circle">
                        <i class="fa fa-money"></i>
                        <span></span>
                    </div>
                    <div class="featured-desc">
                        <h3>تسویه حساب روزانه</h3>
                        <p>تسویه حساب ها در اسرع وقت و بصورت روزانه صورت میگیرد. حتی در روزهای تعطیل ...</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="pricing info">
            <div class="backdrop">
                <div class="cont">
                    <h3>تعرفه ها به چه صورت است؟</h3>
                    <p>فقط یه کارمزد حین انجام تراکنش گرفته میشه و از خروجی پول شما هیچ کارمزدی کسر نمیشه. برای اینکه کارمزد کمتری بدید، سقف کارمزد تو سیستم گذاشتیم. تو هیچ حالتی کارمزد پرداختی شما از 2500 تومان بیشتر نیست !</p>
                </div>
            </div>
        </div>
        <div class="info">
            <div class="cont">
                <h3>صفحه پرداخت شخصی</h3>
                <p>ساده ترین راه واسه گرفتن پول بدون نیاز به کارت بانکی یا داشتن وبلاگ و وب سایت. ارسال و دریافت پول تنها با یه لینک ساده</p>
            </div>
        </div>
        <div class="join-us">
            <div class="cont">
                <div class="row">
                    <div class="col-xs-9">
                        حساب تون رو بسازید و از امکانات رایگان لذت ببرید. ساخت حساب کمتر از یک دقیقه زمان میبره
                    </div>
                    <div class="col-xs-3 text-left">
                        <a href="register" class="btn btn-default">ایجاد حساب کاربری</a>
                    </div>
                </div>
            </div>
        </div>
@endsection