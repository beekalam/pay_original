@extends('layouts.front')
@section('content')
<div class="cont">
    <h1 class="title">
        <span>تماس با ما</span>
    </h1>
    <form class="form-horizontal" action="contact" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="field_name" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">نام و نام خانوادگی *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="text" name="name" id="field_name" placeholder="نام و نام خانوادگی" maxlength="30"
                            value="" class="validate[required,maxSize[30]] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_phone" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">تلفن تماس با کد شهر</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="text" name="phone" id="field_phone" dir="ltr" placeholder="تلفن تماس با کد شهر"
                            value="" class="numeric form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_email" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">ایمیل *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <input type="text" name="email" id="field_email" dir="ltr" placeholder="ایمیل" size="50" value=""
                            class="validate[required,custom[email]] form-control">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="field_text" class="col-xs-5 col-sm-4 col-md-3 col-lg-2 control-label">متن پیغام *</label>
            <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 isReq">
                <div class="row">
                    <div class="col-xs-5">
                        <textarea name="text" id="field_text" placeholder="متن پیغام" cols="60" rows="5" class="validate[required,minSize[10],maxSize[400]] span9 form-control"></textarea>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <p class="form-submit" align="center">
            <button type="submit" class="btn btn-success button green">
                <span>ارسال پیام</span>
            </button>
        </p>
    </form>
    <div class="bottomPad"></div>
</div>
@endsection